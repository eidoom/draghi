import db from "$lib/server/database.js";

db.ability = [
  { id: 1, name: "STR", score: 10 },
  { id: 2, name: "DEX", score: 10 },
  { id: 3, name: "CON", score: 10 },
  { id: 4, name: "INT", score: 10 },
  { id: 5, name: "WIS", score: 10 },
  { id: 6, name: "CHA", score: 10 },
];

db.skills = [
  "Acrobatics",
  "Animal Handling",
  "Arcana",
  "Athletics",
  "Deception",
  "History",
  "Insight",
  "Intimidation",
  "Investigation",
  "Medicine",
  "Nature",
  "Perception",
  "Performance",
  "Persuasion",
  "Religion",
  "Sleight of Hand",
  "Stealth",
  "Survival",
];

db.feats = [{ name: "Grappler", desc: "Core" }];
