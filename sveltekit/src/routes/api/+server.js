import { json } from "@sveltejs/kit";

import db from "$lib/server/database.js";

export async function PUT({ request }) {
  const { tab, col, id, val } = await request.json();

  const i = db[tab].findIndex((o) => o.id === id);

  db[tab][i][col] = val;

  return new Response(val);
}

export async function POST({ request }) {
  const data = await request.json();

  db.feats.push(data);

  return json({ type: "success" });
}
