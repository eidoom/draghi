import db from "$lib/server/database.js";

export function load() {
  return { name: "Joe", ...db };
}
