# Modular UI

Gets data from API.

Build on Svelte. Used [template](https://github.com/sveltejs/template).

## Development

```bash
npm install
npm run dev
```

Navigate to <http://localhost:8080>.

## Production

```bash
npm run build
npm run start
```
or to access from other machines,
```bash
npm run open
```
