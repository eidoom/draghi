#!/usr/bin/env python3

import json

if __name__ == "__main__":
    with open("static/magic-item.json", "r") as f:
        items = json.load(f)

    categories = set(item["category"] for item in items)

    # TODO
    # with open("static/....json", "r") as f:
    #     items = json.load(f)

    # categories |= set(item["category"] for item in items)

    print(categories)
