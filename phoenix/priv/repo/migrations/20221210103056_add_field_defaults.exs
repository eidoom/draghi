defmodule Draghi.Repo.Migrations.AddFieldDefaults do
  use Ecto.Migration

  def change do
    alter table(:characters) do
      modify :hp, :integer, default: 0
      modify :xp, :integer, default: 0
    end
  end
end
