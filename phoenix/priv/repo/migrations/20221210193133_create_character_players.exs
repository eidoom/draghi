defmodule Draghi.Repo.Migrations.CreateCharacterPlayers do
  use Ecto.Migration

  def change do
    create table(:character_players, primary_key: false) do
      add :character_id, references(:characters, on_delete: :delete_all)
      add :player_id, references(:players, on_delete: :delete_all)

      timestamps()
    end

    create index(:character_players, [:character_id])
    create index(:character_players, [:player_id])
    create unique_index(:character_players, [:character_id, :player_id])
  end
end
