defmodule Draghi.Repo.Migrations.MakeFieldsNotNull do
  alias Draghi.Repo
  alias Draghi.Characters.Character
  import Ecto.Query
  use Ecto.Migration

  def change do
    Character
    |> where([c], is_nil(c.xp))
    |> update(set: [xp: 0])
    |> Repo.update_all([])

    Character
    |> where([c], is_nil(c.hp))
    |> update(set: [hp: 0])
    |> Repo.update_all([])

    flush()

    alter table(:characters) do
      modify :name, :string, null: false
      modify :hp, :integer, default: 0, null: false
      modify :xp, :integer, default: 0, null: false
    end
  end
end
