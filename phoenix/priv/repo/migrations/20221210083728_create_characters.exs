defmodule Draghi.Repo.Migrations.CreateCharacters do
  use Ecto.Migration

  def change do
    create table(:characters) do
      add :name, :string
      add :hp, :integer

      timestamps()
    end
  end
end
