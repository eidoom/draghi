defmodule Draghi.Repo.Migrations.AddXpToCharacters do
  use Ecto.Migration

  def change do
    alter table(:characters) do
      add :xp, :integer
    end
  end
end
