defmodule Draghi.CharactersFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Draghi.Characters` context.
  """

  @doc """
  Generate a character.
  """
  def character_fixture(attrs \\ %{}) do
    {:ok, character} =
      attrs
      |> Enum.into(%{
        hp: 42,
        name: "some name"
      })
      |> Draghi.Characters.create_character()

    character
  end

  @doc """
  Generate a character_player.
  """
  def character_player_fixture(attrs \\ %{}) do
    {:ok, character_player} =
      attrs
      |> Enum.into(%{

      })
      |> Draghi.Characters.create_character_player()

    character_player
  end
end
