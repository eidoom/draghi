defmodule DraghiWeb.PageControllerTest do
  use DraghiWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    IO.puts(html_response(conn, 200))
    assert html_response(conn, 200) =~ "Welcome"
  end
end
