defmodule Draghi.CharactersTest do
  use Draghi.DataCase

  alias Draghi.Characters

  describe "characters" do
    alias Draghi.Characters.Character

    import Draghi.CharactersFixtures

    @invalid_attrs %{hp: nil, name: nil}

    test "list_characters/0 returns all characters" do
      character = character_fixture()
      assert Characters.list_characters() == [character]
    end

    test "get_character!/1 returns the character with given id" do
      character = character_fixture()
      assert Characters.get_character!(character.id) == character
    end

    test "create_character/1 with valid data creates a character" do
      valid_attrs = %{hp: 42, name: "some name"}

      assert {:ok, %Character{} = character} = Characters.create_character(valid_attrs)
      assert character.hp == 42
      assert character.name == "some name"
    end

    test "create_character/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Characters.create_character(@invalid_attrs)
    end

    test "update_character/2 with valid data updates the character" do
      character = character_fixture()
      update_attrs = %{hp: 43, name: "some updated name"}

      assert {:ok, %Character{} = character} = Characters.update_character(character, update_attrs)
      assert character.hp == 43
      assert character.name == "some updated name"
    end

    test "update_character/2 with invalid data returns error changeset" do
      character = character_fixture()
      assert {:error, %Ecto.Changeset{}} = Characters.update_character(character, @invalid_attrs)
      assert character == Characters.get_character!(character.id)
    end

    test "delete_character/1 deletes the character" do
      character = character_fixture()
      assert {:ok, %Character{}} = Characters.delete_character(character)
      assert_raise Ecto.NoResultsError, fn -> Characters.get_character!(character.id) end
    end

    test "change_character/1 returns a character changeset" do
      character = character_fixture()
      assert %Ecto.Changeset{} = Characters.change_character(character)
    end
  end

  describe "character_players" do
    alias Draghi.Characters.CharacterPlayer

    import Draghi.CharactersFixtures

    @invalid_attrs %{}

    test "list_character_players/0 returns all character_players" do
      character_player = character_player_fixture()
      assert Characters.list_character_players() == [character_player]
    end

    test "get_character_player!/1 returns the character_player with given id" do
      character_player = character_player_fixture()
      assert Characters.get_character_player!(character_player.id) == character_player
    end

    test "create_character_player/1 with valid data creates a character_player" do
      valid_attrs = %{}

      assert {:ok, %CharacterPlayer{} = character_player} = Characters.create_character_player(valid_attrs)
    end

    test "create_character_player/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Characters.create_character_player(@invalid_attrs)
    end

    test "update_character_player/2 with valid data updates the character_player" do
      character_player = character_player_fixture()
      update_attrs = %{}

      assert {:ok, %CharacterPlayer{} = character_player} = Characters.update_character_player(character_player, update_attrs)
    end

    test "update_character_player/2 with invalid data returns error changeset" do
      character_player = character_player_fixture()
      assert {:error, %Ecto.Changeset{}} = Characters.update_character_player(character_player, @invalid_attrs)
      assert character_player == Characters.get_character_player!(character_player.id)
    end

    test "delete_character_player/1 deletes the character_player" do
      character_player = character_player_fixture()
      assert {:ok, %CharacterPlayer{}} = Characters.delete_character_player(character_player)
      assert_raise Ecto.NoResultsError, fn -> Characters.get_character_player!(character_player.id) end
    end

    test "change_character_player/1 returns a character_player changeset" do
      character_player = character_player_fixture()
      assert %Ecto.Changeset{} = Characters.change_character_player(character_player)
    end
  end
end
