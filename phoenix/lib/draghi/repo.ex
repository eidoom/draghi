defmodule Draghi.Repo do
  use Ecto.Repo,
    otp_app: :draghi,
    adapter: Ecto.Adapters.Postgres
end
