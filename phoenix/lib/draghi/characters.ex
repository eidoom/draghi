defmodule Draghi.Characters do
  @moduledoc """
  The Characters context.
  """

  import Ecto.Query, warn: false
  alias Draghi.Repo

  alias Draghi.Characters.Character

  @doc """
  Returns the list of characters.

  ## Examples

      iex> list_characters()
      [%Character{}, ...]

  """
  def list_characters do
    Repo.all(Character)
  end

  @doc """
  Gets a single character.

  Raises `Ecto.NoResultsError` if the Character does not exist.

  ## Examples

      iex> get_character!(123)
      %Character{}

      iex> get_character!(456)
      ** (Ecto.NoResultsError)

  """
  def get_character!(id), do: Repo.get!(Character, id)

  @doc """
  Creates a character.

  ## Examples

      iex> create_character(%{field: value})
      {:ok, %Character{}}

      iex> create_character(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_character(attrs \\ %{}) do
    %Character{}
    |> Character.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a character.

  ## Examples

      iex> update_character(character, %{field: new_value})
      {:ok, %Character{}}

      iex> update_character(character, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_character(%Character{} = character, attrs) do
    character
    |> Character.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a character.

  ## Examples

      iex> delete_character(character)
      {:ok, %Character{}}

      iex> delete_character(character)
      {:error, %Ecto.Changeset{}}

  """
  def delete_character(%Character{} = character) do
    Repo.delete(character)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking character changes.

  ## Examples

      iex> change_character(character)
      %Ecto.Changeset{data: %Character{}}

  """
  def change_character(%Character{} = character, attrs \\ %{}) do
    Character.changeset(character, attrs)
  end

  alias Draghi.Characters.CharacterPlayer

  @doc """
  Returns the list of character_players.

  ## Examples

      iex> list_character_players()
      [%CharacterPlayer{}, ...]

  """
  def list_character_players do
    Repo.all(CharacterPlayer)
  end

  @doc """
  Gets a single character_player.

  Raises `Ecto.NoResultsError` if the Character player does not exist.

  ## Examples

      iex> get_character_player!(123)
      %CharacterPlayer{}

      iex> get_character_player!(456)
      ** (Ecto.NoResultsError)

  """
  def get_character_player!(id), do: Repo.get!(CharacterPlayer, id)

  @doc """
  Creates a character_player.

  ## Examples

      iex> create_character_player(%{field: value})
      {:ok, %CharacterPlayer{}}

      iex> create_character_player(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_character_player(attrs \\ %{}) do
    %CharacterPlayer{}
    |> CharacterPlayer.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a character_player.

  ## Examples

      iex> update_character_player(character_player, %{field: new_value})
      {:ok, %CharacterPlayer{}}

      iex> update_character_player(character_player, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_character_player(%CharacterPlayer{} = character_player, attrs) do
    character_player
    |> CharacterPlayer.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a character_player.

  ## Examples

      iex> delete_character_player(character_player)
      {:ok, %CharacterPlayer{}}

      iex> delete_character_player(character_player)
      {:error, %Ecto.Changeset{}}

  """
  def delete_character_player(%CharacterPlayer{} = character_player) do
    Repo.delete(character_player)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking character_player changes.

  ## Examples

      iex> change_character_player(character_player)
      %Ecto.Changeset{data: %CharacterPlayer{}}

  """
  def change_character_player(%CharacterPlayer{} = character_player, attrs \\ %{}) do
    CharacterPlayer.changeset(character_player, attrs)
  end
end
