defmodule Draghi.Characters.CharacterPlayer do
  use Ecto.Schema
  import Ecto.Changeset

  schema "character_players" do
    timestamps()
  end

  @doc false
  def changeset(character_player, attrs) do
    character_player
    |> cast(attrs, [])
    |> validate_required([])
  end
end
