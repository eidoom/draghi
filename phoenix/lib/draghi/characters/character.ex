defmodule Draghi.Characters.Character do
  use Ecto.Schema
  import Ecto.Changeset
  alias Draghi.Players.Player

  schema "characters" do
    field :name, :string
    field :hp, :integer, default: 0
    field :xp, :integer, default: 0

    many_to_many :players, Player, join_through: "character_players", on_replace: :delete

    timestamps()
  end

  @doc false
  def changeset(character, attrs) do
    character
    |> cast(attrs, [:name, :hp, :xp])
    |> validate_required([:name, :hp, :xp])
    |> validate_number(:hp, greater_than_or_equal_to: 0)
    |> validate_number(:xp, greater_than_or_equal_to: 0)
  end
end
