defmodule DraghiWeb.PageController do
  use DraghiWeb, :controller

  def index(conn, _params) do
    render(conn, :index)
  end

end
