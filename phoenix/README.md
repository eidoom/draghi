# Phoenix

## Installation

### Fedora

#### PostgreSQL

* [PosergreSQL](https://www.postgresql.org/) [docs](https://www.postgresql.org/docs/current/) [WP](https://en.wikipedia.org/wiki/PostgreSQL) [AW](https://wiki.archlinux.org/title/PostgreSQL) [SC](https://git.postgresql.org/gitweb/?p=postgresql.git) [fedora-docs](https://docs.fedoraproject.org/en-US/quick-docs/postgresql/)

```shell
sudo dnf install postgresql-server postgresql-contrib oidentd
sudo postgresql-setup --initdb --unit postgresql
sudo systemctl start oidentd
sudo systemctl start postgresql
```
Authentication is by [`ident`](https://en.wikipedia.org/wiki/Ident_protocol) by default (see `/var/lib/pgsql/data/pg_hba.conf`), so we install and start the [`oidentd`](https://en.wikipedia.org/wiki/Oidentd) daemon.
We make a database user with the same name as our Linux user, who can login and create databases,
```shell
sudo -u postgres createuser <username> --login --createdb --pwprompt
```
Settings in `/var/lib/pgsql/data`.
Logs in `/var/lib/pgsql/initdb_postgresql.log`.

#### Phoenix

* [Pheonix](https://www.phoenixframework.org/) [docs](https://hexdocs.pm/phoenix/overview.html) [WP](https://en.wikipedia.org/wiki/Phoenix_(web_framework)) [SC](https://github.com/phoenixframework/phoenix)
* [Elixir](https://elixir-lang.org/) [docs](https://elixir-lang.org/docs.html) [WP](https://en.wikipedia.org/wiki/Elixir_(programming_language)) [AW](https://wiki.archlinux.org/title/Elixir) [SC](https://github.com/elixir-lang/elixir)

```shell
sudo dnf install elixir inotify-tools erlang-xmerl
mix local.hex
mix local.rebar
mix archive.install hex phx_new
mix phx.new <name>
cd <name>
```
Configure `config/dev.exs` for the database: we don't need a password since we're using `ident` but we'll need to set the username.
```shell
mix ecto.create
mix phx.server
```
Open <http://localhost:4000>.

## Usage

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix

  * https://www.phoenix-tutorial.com

## Reminders

### Creating a table

```shell
mix phx.gen.html Characters Character characters name:string hp:integer
mix ecto.migrate
```

### Adding a field

```shell
mix ecto.gen.migration add_xp_to_characters
mix ecto.migrate
```

### DB <-> MVC <-> RESTful

```
lib
├── draghi
│   ├── characters
│   │   └── character.ex
└── draghi_web
    ├── controllers
    │   └── character_controller.ex
    └── templates
        └── character
            └── *.html.heex
```
