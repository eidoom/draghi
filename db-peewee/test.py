#!/usr/bin/env python3


from peewee import (
    Case,
    fn,
    SQL,
    JOIN,
)

from models import (
    Ability,
    Armour,
    ArmourCategory,
    ArmourProficiency,
    ArmourStealthDisadvantage,
    AttackType,
    CClass,
    CClassSpell,
    Character,
    CharacterAbility,
    SaveProficiency,
    CharacterArmour,
    CharacterArmourEquipped,
    CharacterCClass,
    CharacterSubCClass,
    CharacterWeapon,
    CharacterWeaponEquipped,
    Component,
    DamageType,
    MagicSchool,
    Race,
    Skill,
    SkillProficiency,
    Spell,
    SpellComponent,
    SpellConcentration,
    SpellMaterial,
    SpellRitual,
    SubCClass,
    Weapon,
    WeaponProficiency,
    WeaponProperty,
    WeaponPropertyDamage,
    WeaponPropertyDescription,
    WeaponPropertyRange,
    WeaponPropertySpecial,
    XP,
)
import query
from database import db
from init_db import init_db


def make_test_characters():
    test_characters = ("adnarim", "nala", "nayr")

    characters = [c.name for c in Character.select(Character.name)]

    if not any(t in characters for t in test_characters):
        # Character
        query = Ability.select(Ability.id)
        for name in test_characters:
            character = Character.create(
                name=name, race=Race.get(Race.name == "Human"), hp=6
            )

            with db.atomic():
                CharacterAbility.insert_many(
                    (
                        {"character": character, "ability": ability, "value": 6}
                        for ability in query
                    )
                ).execute()

            CharacterCClass.insert(
                character=character,
                cclass=CClass.get(CClass.name == "Barbarian"),
            ).execute()

        # multiclassing
        adnarim_fighter = CharacterCClass.create(
            character=Character.get(Character.name == "adnarim"),
            cclass=CClass.get(CClass.name == "Fighter"),
            level=2,
        )

        # subclassing
        CharacterSubCClass.insert(
            character_cclass=adnarim_fighter,
            subcclass=SubCClass.get(SubCClass.name == "Champion"),
        ).execute()

        # CharacterArmour
        padded = CharacterArmour.create(
            character=Character.get(Character.name == "adnarim"),
            armour=Armour.get(Armour.name == "Padded"),
        )

        CharacterArmourEquipped.insert(character_armour=padded).execute()

        CharacterArmour.insert(
            character=Character.get(Character.name == "adnarim"),
            armour=Armour.get(Armour.name == "Leather"),
        ).execute()

        studded = CharacterArmour.create(
            character=Character.get(Character.name == "nala"),
            armour=Armour.get(Armour.name == "Studded Leather"),
        )

        CharacterArmourEquipped.insert(character_armour=studded).execute()

        CharacterArmour.insert(
            character=Character.get(Character.name == "nala"),
            armour=Armour.get(Armour.name == "Hide"),
        ).execute()

        # CharacterWeapon
        staff = CharacterWeapon.create(
            character=Character.get(Character.name == "adnarim"),
            weapon=Weapon.get(Weapon.name == "Quarterstaff"),
        )

        CharacterWeaponEquipped.insert(character_weapon=staff).execute()

        mace = CharacterWeapon.create(
            character=Character.get(Character.name == "nala"),
            weapon=Weapon.get(Weapon.name == "Mace"),
        )

        CharacterWeaponEquipped.insert(character_weapon=mace).execute()

        CharacterWeapon.insert(
            character=Character.get(Character.name == "nala"),
            weapon=Weapon.get(Weapon.name == "Greatclub"),
        ).execute()

        CharacterWeapon.insert(
            character=Character.get(Character.name == "nayr"),
            weapon=Weapon.get(Weapon.name == "Handaxe"),
        ).execute()


def test_queries():
    print("Equipped armour:")

    query = (
        CharacterArmour.select(Character.name, Armour.name)
        .join(Armour)
        .join_from(CharacterArmour, Character)
        .join_from(
            CharacterArmour, CharacterArmourEquipped
        )  # select only equipped armours
        .where(Armour.category == ArmourCategory.get(ArmourCategory.name == "Light"))
    )

    for characterarmour in query:
        print(f"{characterarmour.character.name:>10}: {characterarmour.armour.name}")

    print("Weapons in inventories:")

    query = (
        CharacterWeapon.select(
            Character.name,
            fn.GROUP_CONCAT(Weapon.name)
            .python_value(lambda x: x.split(","))
            .alias("items"),
        )
        .join(Weapon)
        .switch(CharacterWeapon)
        .join(Character)
        .group_by(Character.name)
    )

    for cw in query:
        print(f"{cw.character.name:>10}:")
        for w in cw.items:
            print(f"{'':12}{w}")

    print("Inventory weights:")

    inventory_weight = (
        Case(None, ((Armour.weight, fn.SUM(Armour.weight)),), 0)
        + Case(None, ((Weapon.weight, fn.SUM(Weapon.weight)),), 0)
    ).alias("total")

    query = (
        Character.select(
            Character.name,
            inventory_weight,
        )
        .join(CharacterArmour, join_type=JOIN.LEFT_OUTER)
        .join(Armour, join_type=JOIN.LEFT_OUTER)
        .switch(Character)
        .join(CharacterWeapon, join_type=JOIN.LEFT_OUTER)
        .join(Weapon, join_type=JOIN.LEFT_OUTER)
        .group_by(Character.name)
        .order_by(SQL("total").desc())
    )

    for r in query:
        print(f"{r.name:>10}: {r.total}")

    w = "Spear"
    print(f"{w} properties:")

    query = (
        WeaponProperty.select(
            WeaponPropertyDescription.name,
            WeaponPropertyDescription.description,
            WeaponPropertyRange.min,
            WeaponPropertyRange.max,
            WeaponPropertyDamage.damage,
        )
        .join(WeaponPropertyDescription)
        .join_from(WeaponProperty, Weapon)
        .join_from(WeaponProperty, WeaponPropertyRange, join_type=JOIN.LEFT_OUTER)
        .join_from(WeaponProperty, WeaponPropertyDamage, join_type=JOIN.LEFT_OUTER)
        .where(Weapon.name == w)
    )

    for r in query.objects():
        out = f"{r.name:>10}: {r.description}"
        if r.min is not None:
            out += f" ({r.min}--{r.max})"
        if r.damage is not None:
            out += f" ({r.damage})"
        print(out)

    c = "nala"
    race = Race.select(Race.name).join(Character).where(Character.name == c).scalar()
    cclass = (
        CClass.select(CClass.name)
        .join(CharacterCClass)
        .join(Character)
        .where(Character.name == c)
        .scalar()
    )
    print(f"Abilities of {c} ({race} {cclass})")

    query = (
        CharacterAbility.select(
            Ability.name,
            CharacterAbility.value,
            Ability.measure,
        )
        .join(Character)
        .join_from(CharacterAbility, Ability)
        .where(Character.name == c)
    )

    for a in query:
        print(f"{a.ability.name:>13} {a.value:>2} (measuring {a.ability.measure})")

    print("HP:")
    max_hp = fn.SUM(CharacterCClass.level * CClass.hit_die).alias("max_hp")
    query = (
        CharacterCClass.select(Character.name, Character.hp, max_hp)
        .join(Character)
        .join_from(CharacterCClass, CClass)
        .group_by(Character)
        .order_by(max_hp)
    )
    for a in query:
        print(f"{a.character.name:>8} {a.character.hp:>3} / {a.max_hp}")


if __name__ == "__main__":
    with db.connection_context():
        init_db()

        make_test_characters()

        test_queries()

        print("adnarim max hp:", query.max_hp("adnarim"))
        print("nala inventory weight:", query.inventory_weight("nala"))
