from peewee import SqliteDatabase

db = SqliteDatabase(
    ":memory:",
    pragmas={
        "foreign_keys": 1,
        "ignore_check_constraints": 0,
    },
)
