#!/usr/bin/env python3


from peewee import (
    Case,
    fn,
    SQL,
    JOIN,
)

from models import (
    Ability,
    Armour,
    ArmourProficiency,
    CClass,
    Character,
    CharacterAbility,
    CharacterArmour,
    CharacterCClass,
    CharacterWeapon,
    Race,
    Skill,
    SkillProficiency,
    Weapon,
    WeaponProficiency,
    WeaponProperty,
    WeaponPropertyDamage,
    WeaponPropertyDescription,
    WeaponPropertyRange,
    XP,
)


def max_hp(character_name):
    return (
        CharacterCClass.select(fn.SUM(CharacterCClass.level * CClass.hit_die))
        .join(CClass)
        .join_from(CharacterCClass, Character)
        .where(Character.name == character_name)
        .scalar()
    )


def inventory_weight(character_name):
    return (
        Character.select(
            Case(None, ((Armour.weight, fn.SUM(Armour.weight)),), 0)
            + Case(None, ((Weapon.weight, fn.SUM(Weapon.weight)),), 0)
        )
        .join(CharacterArmour, join_type=JOIN.LEFT_OUTER)
        .join(Armour, join_type=JOIN.LEFT_OUTER)
        .switch(Character)
        .join(CharacterWeapon, join_type=JOIN.LEFT_OUTER)
        .join(Weapon, join_type=JOIN.LEFT_OUTER)
        .where(Character.name == character_name)
        .scalar()
    )
