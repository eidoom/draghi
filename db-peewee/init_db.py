#!/usr/bin/env python3

import json

from database import db
from models import (
    Ability,
    Armour,
    ArmourCategory,
    ArmourProficiency,
    ArmourStealthDisadvantage,
    AttackType,
    Attunement,
    CClass,
    CClassSpell,
    Character,
    CharacterAbility,
    CharacterArmour,
    CharacterArmourEquipped,
    CharacterCClass,
    CharacterCondition,
    CharacterSubCClass,
    CharacterWeapon,
    CharacterWeaponEquipped,
    Component,
    Condition,
    DamageType,
    Immunity,
    MagicItem,
    MagicSchool,
    Monster,
    Race,
    Rarity,
    Resistance,
    SaveProficiency,
    Skill,
    SkillProficiency,
    Spell,
    SpellAoE,
    SpellShape,
    SpellAttackType,
    SpellComponent,
    SpellConcentration,
    SpellDamageType,
    SpellMaterial,
    SpellRitual,
    SpellSave,
    StartingCClass,
    SubCClass,
    Vulnerability,
    Weapon,
    WeaponProficiency,
    WeaponProperty,
    WeaponPropertyDamage,
    WeaponPropertyDescription,
    WeaponPropertyRange,
    WeaponPropertySpecial,
    XP,
)


def proc_weapon(weapon):
    out = {}
    for k, v in weapon.items():
        if k == "damage_type":
            out[k] = (
                DamageType.select(DamageType.id).where(DamageType.name == v).scalar()
            )
        elif k == "attack_type":
            out[k] = (
                AttackType.select(AttackType.id).where(AttackType.name == v).scalar()
            )
        elif k == "rarity":
            out[k] = Rarity.select(Rarity.id).where(Rarity.name == v).scalar()
        elif k != "properties":
            out[k] = v
    return out


def proc_armour(armour):
    out = {}
    for k, v in armour.items():
        if k == "rarity":
            out[k] = Rarity.select(Rarity.id).where(Rarity.name == v).scalar()
        elif k == "category":
            out[k] = (
                ArmourCategory.select(ArmourCategory.id)
                .where(ArmourCategory.name == v)
                .scalar()
            )
        elif k != "stealth_disadvantage":
            out[k] = v
    return out


def proc_magic_item(magic_item):
    out = {}
    for k, v in magic_item.items():
        if k == "rarity":
            out[k] = Rarity.select(Rarity.id).where(Rarity.name == v).scalar()
        else:
            out[k] = v
    return out


def init_db():
    if not db.get_tables():
        tables = (
            Ability,
            Armour,
            ArmourCategory,
            ArmourProficiency,
            ArmourStealthDisadvantage,
            AttackType,
            Attunement,
            CClass,
            CClassSpell,
            Character,
            CharacterAbility,
            CharacterArmour,
            CharacterArmourEquipped,
            CharacterCClass,
            CharacterCondition,
            CharacterSubCClass,
            CharacterWeapon,
            CharacterWeaponEquipped,
            Component,
            Condition,
            DamageType,
            Immunity,
            MagicItem,
            MagicSchool,
            Monster,
            Race,
            Rarity,
            Resistance,
            SaveProficiency,
            Skill,
            SkillProficiency,
            Spell,
            SpellAoE,
            SpellShape,
            SpellAttackType,
            SpellComponent,
            SpellConcentration,
            SpellDamageType,
            SpellMaterial,
            SpellRitual,
            SpellSave,
            StartingCClass,
            SubCClass,
            Vulnerability,
            Weapon,
            WeaponProficiency,
            WeaponProperty,
            WeaponPropertyDamage,
            WeaponPropertyDescription,
            WeaponPropertyRange,
            WeaponPropertySpecial,
            XP,
        )

        db.create_tables(tables)

        # XP
        with open("../static/xp.json", "r") as f:
            xp = json.load(f)

        with db.atomic():
            XP.insert_many(
                enumerate(xp, start=1), fields=(XP.level, XP.threshold)
            ).execute()

        # Race
        with open("../static/race.json", "r") as f:
            races = json.load(f)

        with db.atomic():
            Race.insert_many(
                ({k: v for k, v in race.items() if k in ("name",)} for race in races)
            ).execute()

        # CClass
        with open("../static/cclass.json", "r") as f:
            cclasses = json.load(f)["classes"]

        with db.atomic():
            CClass.insert_many(
                ({k: cclass[k] for k in ("name", "hit_die")} for cclass in cclasses)
            ).execute()

        # SubCClass
        with open("../static/subclass.json", "r") as f:
            subcclasses = json.load(f)

        with db.atomic():
            SubCClass.insert_many(
                (
                    {
                        k: CClass.select(CClass.id)
                        .where(CClass.name == subcclass[k])
                        .scalar()
                        if k == "cclass"
                        else subcclass[k]
                        for k in ("name", "cclass", "description")
                    }
                    for subcclass in subcclasses
                )
            ).execute()

        # Ability
        with open("../static/ability.json", "r") as f:
            abilities = json.load(f)

        with db.atomic():
            Ability.insert_many(abilities).execute()

        # Skill
        with open("../static/skill.json", "r") as f:
            skills = json.load(f)

        with db.atomic():
            Skill.insert_many(
                (
                    {
                        key: Ability.select(Ability.id)
                        .where(Ability.name == val)
                        .scalar()
                        if key == "ability"
                        else val
                        for key, val in skill.items()
                    }
                    for skill in skills
                )
            ).execute()

        # Rarity
        with open("../static/rarity.json", "r") as f:
            rarities = json.load(f)

        with db.atomic():
            Rarity.insert_many(rarities).execute()

        # Armour
        with open("../static/armour.json", "r") as f:
            armours = json.load(f)

        with db.atomic():
            ArmourCategory.insert_many(armours["categories"]).execute()

        with db.atomic():
            Armour.insert_many(
                (proc_armour(armour) for armour in armours["armours"])
            ).execute()

        with db.atomic():
            ArmourStealthDisadvantage.insert_many(
                (
                    (
                        Armour.select(Armour.id)
                        .where(Armour.name == armour["name"])
                        .scalar(),
                    )
                    for armour in armours["armours"]
                    if armour["stealth_disadvantage"]
                ),
                fields=(ArmourStealthDisadvantage.armour,),
            ).execute()

        # DamageType, AttackType
        with open("../static/attack-damage.json", "r") as f:
            attack_damages = json.load(f)

        with db.atomic():
            DamageType.insert_many(attack_damages["damage_types"]).execute()

        with db.atomic():
            AttackType.insert_many(attack_damages["attack_types"]).execute()

        # Weapon
        with open("../static/weapon.json", "r") as f:
            weapons = json.load(f)

        with db.atomic():
            WeaponPropertyDescription.insert_many(weapons["properties"]).execute()

        with db.atomic():
            Weapon.insert_many(
                (proc_weapon(weapon) for weapon in weapons["weapons"])
            ).execute()

        with db.atomic():
            WeaponProperty.insert_many(
                (
                    (
                        weapon,
                        WeaponPropertyDescription.select(WeaponPropertyDescription.id)
                        .where(
                            WeaponPropertyDescription.name == prop["name"],
                        )
                        .scalar(),
                    )
                    for weapon, props in (
                        (
                            Weapon.select(Weapon.id)
                            .where(Weapon.name == w["name"])
                            .scalar(),
                            w["properties"],
                        )
                        for w in weapons["weapons"]
                        if "properties" in w
                    )
                    for prop in props
                ),
                fields=(
                    WeaponProperty.weapon,
                    WeaponProperty.property,
                ),
            ).execute()

        with db.atomic():
            WeaponPropertyDamage.insert_many(
                (
                    (
                        WeaponProperty.select(WeaponProperty.id)
                        .join(WeaponPropertyDescription)
                        .join_from(WeaponProperty, Weapon)
                        .where(WeaponPropertyDescription.name == prop["name"])
                        .where(Weapon.name == weapon["name"])
                        .scalar(),
                        prop["damage"],
                    )
                    for weapon in weapons["weapons"]
                    if "properties" in weapon
                    for prop in weapon["properties"]
                    if prop["name"] == "Versatile"
                ),
                fields=(
                    WeaponPropertyDamage.property,
                    WeaponPropertyDamage.damage,
                ),
            ).execute()

        with db.atomic():
            WeaponPropertyRange.insert_many(
                (
                    (
                        WeaponProperty.select(WeaponProperty.id)
                        .join(WeaponPropertyDescription)
                        .join_from(WeaponProperty, Weapon)
                        .where(WeaponPropertyDescription.name == prop["name"])
                        .where(Weapon.name == weapon["name"])
                        .scalar(),
                        prop["min"],
                        prop["max"],
                    )
                    for weapon in weapons["weapons"]
                    if "properties" in weapon
                    for prop in weapon["properties"]
                    if prop["name"] == "Thrown"
                ),
                fields=(
                    WeaponPropertyRange.property,
                    WeaponPropertyRange.min,
                    WeaponPropertyRange.max,
                ),
            ).execute()

        with db.atomic():
            WeaponPropertySpecial.insert_many(
                (
                    (
                        WeaponProperty.select(WeaponProperty.id)
                        .join(WeaponPropertyDescription)
                        .join_from(WeaponProperty, Weapon)
                        .where(WeaponPropertyDescription.name == prop["name"])
                        .where(Weapon.name == weapon["name"])
                        .scalar(),
                        prop["description"],
                    )
                    for weapon in weapons["weapons"]
                    if "properties" in weapon
                    for prop in weapon["properties"]
                    if prop["name"] == "Special"
                ),
                fields=(
                    WeaponPropertySpecial.property,
                    WeaponPropertySpecial.description,
                ),
            ).execute()

        # MagicSchool
        with open("../static/magic-school.json", "r") as f:
            magic_schools = json.load(f)

        with db.atomic():
            MagicSchool.insert_many(magic_schools).execute()

        # Component
        with open("../static/component.json", "r") as f:
            components = json.load(f)

        with db.atomic():
            Component.insert_many(components).execute()

        # Spell
        with open("../static/spell.json", "r") as f:
            spells_raw = json.load(f)

        with db.atomic():
            SpellShape.insert_many(spells_raw["area_of_effect"]).execute()

        spells = spells_raw["spells"]

        with db.atomic():
            Spell.insert_many(
                (
                    {
                        k: MagicSchool.select(MagicSchool.id)
                        .where(MagicSchool.name == spell[k])
                        .scalar()
                        if k == "school"
                        else spell[k]
                        for k in (
                            "name",
                            "description",
                            "level",
                            "school",
                            "duration",
                            "range",
                            "casting_time",
                        )
                    }
                    for spell in spells
                )
            ).execute()

        with db.atomic():
            SpellComponent.insert_many(
                (
                    (
                        Spell.select(Spell.id)
                        .where(Spell.name == spell["name"])
                        .scalar(),
                        Component.select(Component.id)
                        .where(Component.short == component)
                        .scalar(),
                    )
                    for spell in spells
                    for component in spell["components"]
                ),
                fields=(
                    SpellComponent.spell,
                    SpellComponent.component,
                ),
            ).execute()

        with db.atomic():
            SpellMaterial.insert_many(
                (
                    (
                        SpellComponent.select(SpellComponent.id)
                        .join(Spell)
                        .where(Spell.name == spell["name"])
                        .join_from(SpellComponent, Component)
                        .where(Component.short == "M")
                        .scalar(),
                        spell["material"],
                    )
                    for spell in spells
                    if "material" in spell
                ),
                fields=(
                    SpellMaterial.spell_component,
                    SpellMaterial.material,
                ),
            ).execute()

        with db.atomic():
            CClassSpell.insert_many(
                (
                    (
                        Spell.select(Spell.id)
                        .where(Spell.name == spell["name"])
                        .scalar(),
                        CClass.select(CClass.id)
                        .where(CClass.name == cclass["name"])
                        .scalar(),
                    )
                    for spell in spells
                    for cclass in spell["classes"]
                ),
                fields=(
                    CClassSpell.spell,
                    CClassSpell.cclass,
                ),
            ).execute()

        with db.atomic():
            SpellRitual.insert_many(
                (
                    (
                        Spell.select(Spell.id)
                        .where(Spell.name == spell["name"])
                        .scalar(),
                    )
                    for spell in spells
                    if spell["ritual"]
                ),
                fields=(SpellRitual.spell,),
            ).execute()

        with db.atomic():
            SpellConcentration.insert_many(
                (
                    (
                        Spell.select(Spell.id)
                        .where(Spell.name == spell["name"])
                        .scalar(),
                    )
                    for spell in spells
                    if spell["concentration"]
                ),
                fields=(SpellConcentration.spell,),
            ).execute()

        with db.atomic():
            SpellAttackType.insert_many(
                (
                    (
                        Spell.select(Spell.id)
                        .where(Spell.name == spell["name"])
                        .scalar(),
                        AttackType.select(AttackType.id)
                        .where(AttackType.name == spell["attack_type"])
                        .scalar(),
                    )
                    for spell in spells
                    if "attack_type" in spell
                ),
                fields=(
                    SpellAttackType.spell,
                    SpellAttackType.attack_type,
                ),
            ).execute()

        with db.atomic():
            SpellDamageType.insert_many(
                (
                    (
                        Spell.select(Spell.id)
                        .where(Spell.name == spell["name"])
                        .scalar(),
                        DamageType.select(DamageType.id)
                        .where(DamageType.name == spell["damage"]["damage_type"])
                        .scalar(),
                    )
                    for spell in spells
                    if "damage" in spell and "damage_type" in spell["damage"]
                ),
                fields=(
                    SpellDamageType.spell,
                    SpellDamageType.damage_type,
                ),
            ).execute()

        with db.atomic():
            SpellSave.insert_many(
                (
                    (
                        Spell.select(Spell.id)
                        .where(Spell.name == spell["name"])
                        .scalar(),
                        Ability.select(Ability.id)
                        .where(Ability.short == spell["dc"]["dc_type"]["name"])
                        .scalar(),
                    )
                    for spell in spells
                    if "dc" in spell
                ),
                fields=(
                    SpellSave.spell,
                    SpellSave.save,
                ),
            ).execute()

        with db.atomic():
            SpellAoE.insert_many(
                (
                    (
                        Spell.select(Spell.id)
                        .where(Spell.name == spell["name"])
                        .scalar(),
                        SpellShape.select(SpellShape.id)
                        .where(
                            SpellShape.name == spell["area_of_effect"]["damage_type"]
                        )
                        .scalar(),
                        spell["area_of_effect"]["size"],
                    )
                    for spell in spells
                    if "area_of_effect" in spell
                ),
                fields=(
                    SpellAoE.spell,
                    SpellAoE.shape,
                    SpellAoE.size,
                ),
            ).execute()

        # MagicItem
        with open("../static/magic-item.json", "r") as f:
            magic_items = json.load(f)

        with db.atomic():
            MagicItem.insert_many(
                (proc_magic_item(magic_item) for magic_item in magic_items)
            ).execute()

        with db.atomic():
            Attunement.insert_many(
                (
                    (
                        MagicItem.select(MagicItem.id)
                        .where(MagicItem.name == item["name"])
                        .scalar(),
                    )
                    for item in magic_items
                    if "attunement" in item
                ),
                fields=(Attunement.magic_item,),
            ).execute()

        # Condition
        with open("../static/condition.json", "r") as f:
            conditions = json.load(f)

        with db.atomic():
            Condition.insert_many(conditions).execute()

        # ======================================================================
        # a test character
        ability_ids = Ability.select(Ability.id)
        with db.atomic():
            character = Character.create(
                name="Aleph",
                race=Race.select(Race.id).where(Race.name == "Human").scalar(),
                xp=1000,
                max_hp=18,
                hp=8,
            )

            CharacterAbility.insert_many(
                (
                    {"character": character, "ability": ability, "score": 8 + i}
                    for i, ability in enumerate(ability_ids)
                )
            ).execute()

            CharacterCClass.insert_many(
                (
                    {
                        "character": character,
                        "cclass": CClass.select(CClass.id)
                        .where(CClass.name == "Wizard")
                        .scalar(),
                        "level": 2,
                    },
                    {
                        "character": character,
                        "cclass": CClass.select(CClass.id)
                        .where(CClass.name == "Barbarian")
                        .scalar(),
                    },
                )
            ).execute()

            StartingCClass.insert({"character_cclass": 1}).execute()

            SaveProficiency.insert_many(
                (
                    {
                        "character_ability": (
                            CharacterAbility.select(CharacterAbility.id)
                            .join(Ability)
                            .where(Ability.short == ability)
                            .where(CharacterAbility.character == character)
                            .scalar()
                        )
                    }
                    for ability in ("DEX", "WIS")
                )
            ).execute()

            SkillProficiency.insert_many(
                (
                    {
                        "skill": (
                            Skill.select(Skill.id).where(Skill.name == skill).scalar()
                        ),
                        "character": character,
                    }
                    for skill in ("Insight", "History")
                )
            ).execute()

            CharacterCondition.insert_many(
                (
                    {
                        "condition": (
                            Condition.select(Condition.id)
                            .where(Condition.name == condition)
                            .scalar()
                        ),
                        "character": character,
                    }
                    for condition in ("Charmed", "Prone")
                )
            ).execute()
