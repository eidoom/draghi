#!/usr/bin/env python3


from peewee import (
    CharField,
    ForeignKeyField,
    IntegerField,
    Model,
    TextField,
    Check,
)

from database import db


class BaseModel(Model):
    class Meta:
        database = db


class XP(BaseModel):
    level = IntegerField(primary_key=True)
    threshold = IntegerField()


class Race(BaseModel):
    name = CharField(unique=True)


class Creature(BaseModel):
    # Base for Character and Monster
    name = CharField(unique=True)
    hp = IntegerField(default=0)


class Monster(Creature):
    ...


class Character(Creature):
    race = ForeignKeyField(Race, backref="characters")
    max_hp = IntegerField(default=0)
    xp = IntegerField(default=0)
    notes = TextField(default="")
    # [CharacterAbility]
    # [SkillProficiency]
    # [CharacterArmour]
    # [CharacterCClass]
    # [CharacterWeapon]
    # [ArmourProficiency]
    # [CharacterCondition]


class CClass(BaseModel):
    name = CharField(unique=True)
    hit_die = IntegerField()
    # [SubCClass]
    # [CharacterCClass]
    # [CClassSpell]


class SubCClass(BaseModel):
    name = CharField(unique=True)
    cclass = ForeignKeyField(CClass, backref="subcclasses")
    description = TextField()
    # [CharacterSubCClass]


class CharacterCClass(BaseModel):
    cclass = ForeignKeyField(CClass, backref="characters")
    character = ForeignKeyField(Character, backref="cclasses")
    level = IntegerField(
        default=1, constraints=[Check("level > 0"), Check("level <= 20")]
    )
    # ?CharacterSubCClass
    # ?StartingCClass
    class Meta:
        indexes = ((("cclass", "character"), True),)


class StartingCClass(BaseModel):
    character_cclass = ForeignKeyField(CharacterCClass, backref="subcclasses")


class CharacterSubCClass(BaseModel):
    character_cclass = ForeignKeyField(CharacterCClass, backref="subcclasses")
    subcclass = ForeignKeyField(SubCClass, backref="character_cclasses")


class Ability(BaseModel):
    name = CharField(unique=True)
    short = CharField(unique=True, max_length=3)
    measure = TextField()
    description = TextField()
    # [CharacterAbility]
    # [Skill]


class CharacterAbility(BaseModel):
    ability = ForeignKeyField(Ability, backref="characters")
    character = ForeignKeyField(Character, backref="abilities")
    score = IntegerField(
        default=0, constraints=[Check("score >= 0"), Check("score <= 30")]
    )
    # ?SaveProficiency


class SaveProficiency(BaseModel):
    character_ability = ForeignKeyField(
        CharacterAbility, unique=True, backref="proficient"
    )


class Skill(BaseModel):
    name = CharField(unique=True)
    description = TextField()
    ability = ForeignKeyField(Ability, backref="skills")
    # [SkillProficiency]


class SkillProficiency(BaseModel):
    skill = ForeignKeyField(Skill, backref="characters")
    character = ForeignKeyField(Character, backref="skills")


class ArmourCategory(BaseModel):
    name = CharField(unique=True)
    description = TextField()
    degree = IntegerField()
    # [Armour]


class Rarity(BaseModel):
    name = CharField(unique=True)
    degree = IntegerField()


class Armour(BaseModel):
    name = CharField(unique=True)
    category = ForeignKeyField(ArmourCategory, backref="armours")
    rarity = ForeignKeyField(Rarity, backref="armours")
    ac = IntegerField()  # armour class
    cost = IntegerField()  # cp (copper pieces)
    weight = IntegerField()
    description = TextField()
    # ?ArmourStealthDisadvantage
    # [CharacterArmour]
    # [ArmourProficiency]


class ArmourStealthDisadvantage(BaseModel):
    armour = ForeignKeyField(Armour, backref="stealth_disadvantage")


class ArmourProficiency(BaseModel):
    armour = ForeignKeyField(Armour, backref="proficient_characters")
    character = ForeignKeyField(Character, backref="armour_proficiencies")


class CharacterArmour(BaseModel):
    armour = ForeignKeyField(Armour, backref="owned_by")
    character = ForeignKeyField(Character, backref="armours")
    # ?CharacterArmourEquipped


class CharacterArmourEquipped(BaseModel):
    character_armour = ForeignKeyField(CharacterArmour, backref="equipped")


class DamageType(BaseModel):
    name = CharField(unique=True)
    description = TextField()


class AttackType(BaseModel):
    name = CharField(unique=True)


class Weapon(BaseModel):
    name = CharField(unique=True)
    rarity = ForeignKeyField(Rarity, backref="weapons")
    damage = CharField()
    damage_type = ForeignKeyField(DamageType, backref="weapons")
    level = CharField()
    attack_type = ForeignKeyField(AttackType, backref="weapons")
    cost = IntegerField()  # cp
    weight = IntegerField()
    # [WeaponProperty]
    # [CharacterWeapon]
    # [WeaponProficiency]


class WeaponProficiency(BaseModel):
    weapon = ForeignKeyField(Weapon, backref="proficient_characters")
    character = ForeignKeyField(Character, backref="weapon_proficiencies")


class CharacterWeapon(BaseModel):
    weapon = ForeignKeyField(Weapon, backref="characters")
    character = ForeignKeyField(Character, backref="weapons")
    # ?CharacterWeaponEquipped


class CharacterWeaponEquipped(BaseModel):
    character_weapon = ForeignKeyField(CharacterWeapon, backref="equipped")


class WeaponPropertyDescription(BaseModel):
    name = CharField(unique=True)
    description = TextField()
    # [WeaponProperty}


class WeaponProperty(BaseModel):
    weapon = ForeignKeyField(Weapon, backref="properties")
    property = ForeignKeyField(WeaponPropertyDescription, backref="weapons")
    # ?WeaponPropertyDamage
    # ?WeaponPropertyRange
    # ?WeaponPropertySpecial


class WeaponPropertyDamage(BaseModel):
    property = ForeignKeyField(WeaponProperty, backref="damage")
    damage = CharField()


class WeaponPropertyRange(BaseModel):
    property = ForeignKeyField(WeaponProperty, backref="range")
    min = IntegerField()
    max = IntegerField()


class WeaponPropertySpecial(BaseModel):
    property = ForeignKeyField(WeaponProperty, backref="special")
    description = TextField()


class MagicSchool(BaseModel):
    name = CharField(unique=True)
    description = TextField()


class Component(BaseModel):
    name = CharField(unique=True)
    short = CharField(unique=True, max_length=1)
    description = TextField()


class Spell(BaseModel):
    name = CharField(unique=True)
    description = TextField()
    level = IntegerField()
    duration = CharField()
    casting_time = CharField()
    range = CharField()
    school = ForeignKeyField(MagicSchool, backref="spells")
    # [SpellComponent]
    # [CClassSpell]
    # ?SpellConcentration
    # ?SpellRitual
    # ?SpellAttackType
    # ?SpellDamageType
    # ?SpellSave
    # ?SpellAoE


class SpellShape(BaseModel):
    name = CharField(unique=True)
    description = TextField()


class SpellAoE(BaseModel):
    shape = ForeignKeyField(SpellShape, backref="spells")
    spell = ForeignKeyField(Spell, backref="aoes")
    size = IntegerField()


class SpellDamageType(BaseModel):
    damage_type = ForeignKeyField(DamageType, backref="spells")
    spell = ForeignKeyField(Spell, backref="damage_types")


class SpellAttackType(BaseModel):
    attack_type = ForeignKeyField(AttackType, backref="spells")
    spell = ForeignKeyField(Spell, backref="attack_types")


class SpellSave(BaseModel):
    save = ForeignKeyField(Ability, backref="save_spells")
    spell = ForeignKeyField(Spell, backref="saves")


class SpellRitual(BaseModel):
    spell = ForeignKeyField(Spell, backref="ritual")


class SpellConcentration(BaseModel):
    spell = ForeignKeyField(Spell, backref="concentration")


class SpellComponent(BaseModel):
    spell = ForeignKeyField(Spell, backref="components")
    component = ForeignKeyField(Component, backref="spells")
    # ?SpellMaterial


class SpellMaterial(BaseModel):
    spell_component = ForeignKeyField(SpellComponent, backref="material")
    material = TextField()


class CClassSpell(BaseModel):
    spell = ForeignKeyField(Spell, backref="classes")
    cclass = ForeignKeyField(CClass, backref="spells")


class MagicItem(BaseModel):
    name = CharField(unique=True)
    category = CharField()
    rarity = ForeignKeyField(Rarity, backref="weapons")
    description = TextField()
    # ?Attunement


class Attunement(BaseModel):
    magic_item = ForeignKeyField(MagicItem, backref="magic_items")


class Condition(BaseModel):
    name = CharField(unique=True)
    description = TextField()
    # [CharacterCondition]


class CharacterCondition(BaseModel):
    character = ForeignKeyField(Character, backref="conditions")
    condition = ForeignKeyField(Condition, backref="characters")

    class Meta:
        indexes = ((("character", "condition"), True),)


class Vulnerability(BaseModel):
    damage_type = ForeignKeyField(DamageType, backref="vulnerable_characters")
    character = ForeignKeyField(Character, backref="vulnerabilities")


class Resistance(BaseModel):
    damage_type = ForeignKeyField(DamageType, backref="resistant_characters")
    character = ForeignKeyField(Character, backref="resistances")


class Immunity(BaseModel):
    damage_type = ForeignKeyField(DamageType, backref="immune_characters")
    character = ForeignKeyField(Character, backref="immunities")
