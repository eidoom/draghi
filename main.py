#!/usr/bin/env python3

import random, collections


ABILITIES = (
    "strength",
    "dexterity",
    "constitution",
    "intelligence",
    "wisdom",
    "charisma",
)

LEVEL_XPS = (
    0,
    300,
    900,
    2700,
    6500,
    14000,
    23000,
    34000,
    48000,
    64000,
    85000,
    100000,
    120000,
    140000,
    165000,
    195000,
    225000,
    265000,
    305000,
    355000,
)

TASK_DIFFICULTIES = (
    "very easy",
    "easy",
    "medium",
    "hard",
    "very hard",
    "nearly impossible",
)

DIFFICULTY_CLASSES = {
    w: 5 * i
    for i, w in enumerate(
        TASK_DIFFICULTIES,
        start=1,
    )
}

# Dice


def d(n, v=True):
    r = random.randint(1, n)
    if v:
        print(r)
    return r


def multiple(n, m):
    return [d(n) for _ in range(m)]


def advantage(n):
    return max(multiple(n, 2))


def disadvantage(n):
    return min(multiple(n, 2))


# ability check, saving throw, attack roll


def compare(character, ability, skill, score, target):
    return character.check(score, ability, skill) >= target


def ability_check(character, ability, skill, difficulty_class, dice=d):
    return compare(
        character,
        ability,
        skill,
        dice(20),
        DIFFICULTY_CLASSES[difficulty_class]
        if isinstance(difficulty_class, str)
        else difficulty_class,
    )


def attack_roll(character, ability, skill, armour_class, dice=d):
    r = dice(20)
    if r == 1:
        return False  # miss
    if r == 20:
        return True  # critical hit
    return compare(character, ability, skill, r, armour_class)


def contest(
    character,
    ability,
    skill,
    other_character,
    other_ability,
    other_skill,
    dice=d,
    other_dice=d,
):
    n = 20
    return compare(
        character,
        ability,
        skill,
        dice(n),
        other_character.check(other_dice(n), other_ability, other_skill),
    )


def passive_check(character, ability, skill, mod=None):
    if mod == "advantage":
        base = 15
    elif mod == "disadvantage":
        base = 5
    else:
        base = 10
    return character.check(base, ability, skill)


def damage_roll(weapon_dice, character, ability, skill, critical_hit=False):
    if critical_hit:
        weapon_dice *= 2
    return character.check(sum(d(n) for n in weapon_dice), ability, skill)


# class proficiencies
def saving_throw():
    ...


# Races


class Race:
    def __init__(self):
        self.ability_benefits = collections.defaultdict(int)
        self.base_walking_speed = 0  # feet


class Dwarf(Race):
    def __init__(self):
        super().__init__()
        self.ability_benefits["constitution"] = 2
        self.base_walking_speed = 25


class Elf(Race):
    def __init__(self):
        super().__init__()
        self.ability_benefits["dexterity"] = 2
        self.base_walking_speed = 30


class Halfling(Race):
    def __init__(self):
        super().__init__()
        self.ability_benefits["dexterity"] = 2
        self.base_walking_speed = 25


class Human(Race):
    def __init__(self):
        super().__init__()
        for ability in ABILITIES:
            self.ability_benefits[ability] = 1
        self.base_walking_speed = 30


# Subraces

...

# Classes


class CClass:
    def __init__(self):
        self.hit_dice = []
        self.base_hp = 0


class Cleric(CClass):
    def __init__(self):
        super().__init__()
        self.hit_dice.append(8)
        self.base_hp = 8


class Fighter(CClass):
    def __init__(self):
        super().__init__()
        self.hit_dice.append(10)
        self.base_hp = 10


class Rogue(CClass):
    def __init__(self):
        super().__init__()
        self.hit_dice.append(8)
        self.base_hp = 8


class Wizard(CClass):
    def __init__(self):
        super().__init__()
        self.hit_dice.append(6)
        self.base_hp = 6


# Abilities


class Ability:
    def __init__(self):
        self.score = sum(sorted([d(6, False) for _ in range(4)], reverse=True)[:3])
        self.max = 20  # adventurer (else 30)
        # self.skills = []

    def __lt__(self, other):
        return self.score < other.score

    def modifier(self):
        return min(10, self.score // 2 - 5)


# class Constitution(Ability):
#     def __init__(self):
#         super().__init__()


# class Strength(Ability):
#     def __init__(self):
#         super().__init__()
#         self.skills.append("Athletics")


# class Dexterity(Ability):
#     def __init__(self):
#         super().__init__()
#         self.skills.extend("Acrobatics", "Sleight of Hand", "Stealth")


# class Intelligence(Ability):
#     def __init__(self):
#         super().__init__()
#         self.skills.extend("Arcana", "History", "Investigation", "Nature", "Religion")


# class Wisdom(Ability):
#     def __init__(self):
#         super().__init__()
#         self.skills.extend(
#             "Animal Handling", "Insight", "Medicine", "Perception", "Survival"
#         )


# class Charisma(Ability):
#     def __init__(self):
#         super().__init__()
#         self.skills.extend("Deception", "Intimidation", "Performance", "Persuasion")


# Characters


class Character:
    def __init__(self, ability_order):
        self.xp = 0

        self.race = Human()
        self.cclass = Fighter()

        self.abilities = self._init_ability(ability_order)

        self.skills = []

        self.hp_max = self._init_hp()
        self.hp = self.hp_max

        self.equipment = []

        self.resistance = []
        self.vulnerability = []

        self.death_saving_throw_successes = 0
        self.death_saving_throw_failures = 0

        self.temporary_hit_points = 0

        self.conditions = []

    def _init_ability(self, order):
        ability_scores = sorted([Ability() for _ in range(6)], reverse=True)

        abilities = {a: ability_scores[order.index(a)] for a in ABILITIES}

        for a in ABILITIES:
            abilities[a].score += self.race.ability_benefits[a]

        return abilities

    def _init_hp(self):
        return self.cclass.base_hp + self.abilities["constitution"].modifier()

    def level(self):
        for i, threshold in enumerate(LEVEL_XPS[1:], start=1):
            if self.xp < threshold:
                return i
        return 20

    def proficiency_bonus(self):
        for i, p in enumerate(range(4, 17, 4), start=2):
            if self.xp < LEVEL_XPS[p]:
                return i
        return 6

    def check(self, base, ability, skill):
        score = base + self.abilities[ability].modifier()
        if skill in self.skills:
            score += self.proficiency_bonus()
        return score

    def armour_class(self):
        return 10 + self.abilities["dexterity"].modifier()  # no equipment

    def heal(self, amount):
        self.hp = min(self.hp_max, self.hp + amount)

    def death_saving_throw_failure(self, n=1):
        self.death_saving_throw_failures += n
        if self.death_saving_throw_failures >= 3:
            print("You die")

    def death_saving_throw_success(self, n=1):
        self.death_saving_throw_successes += n
        if self.death_saving_throw_successes >= 3:
            self.reset_death_saving_throws()
            print("You stablise")

    def reset_death_saving_throws():
        self.death_saving_throw_failures = 0
        self.death_saving_throw_successes = 0

    def damage(self, amount, critical=False):
        if self.temporary_hit_points > 0:
            ...

        if self.hp == 0:
            self.death_saving_throw_failure(2 if critical else 1)

        self.hp -= amount

        if self.hp < 0:
            if abs(self.hp) >= self.hp_max:
                print("Instant death")
            self.hp = 0
        else:
            print("Fall unconscious")

    def death_saving_throw(self):
        r = d(20)
        if r == 20:
            self.heal(1)
            self.reset_death_saving_throws()
        elif r >= 10:
            self.death_saving_throw_success()
        elif r == 1:
            self.death_saving_throw_failure(2)
        else:
            self.death_saving_throw_failure()

    # ?
    def initiative(self):
        return self.check(d(20), "dexterity", "")


if __name__ == "__main__":
    c = Character(
        ability_order=(
            "strength",
            "dexterity",
            "constitution",
            "intelligence",
            "wisdom",
            "charisma",
        ),
    )
    for a, d in c.abilities.items():
        print(a, d.score)
    # c.xp = 5000
    # print(c.proficiency_bonus())
    # print(c.hp)
    # c.xp = 0
    # c.abilities["wisdom"].score = 15
    # print(10 + c.abilities["wisdom"].modifier() + c.proficiency_bonus())
    # print(c.check(10, "wisdom", "perception"))
    # print(ability_check(c, "intelligence", "", "easy"))
