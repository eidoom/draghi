#!/usr/bin/env bash

VENV=.venv
STATIC=static/
HTMX=htmx.min.js
HTMX_VERSION=1.8.4
CSS=pico.min.css
SPINNER=ball-triangle.svg

if [ ! -d "${VENV}" ]; then
	python3 -m venv "${VENV}"
	source "${VENV}/bin/activate"
	if [ -f "requirements.txt" ]; then
		python3 -m pip install -r requirements.txt
	fi
fi

if [ ! -f "${STATIC}/${HTMX}" ]; then
	wget "https://github.com/bigskysoftware/htmx/releases/download/v${HTMX_VERSION}/${HTMX}.gz"
	gunzip ${HTMX}.gz
	mv ${HTMX} ${STATIC}
	rm ${HTMX}.gz
fi

if [ ! -f "${STATIC}/${CSS}" ]; then
	wget "https://unpkg.com/@picocss/pico@latest/css/${CSS}" --output-document="${STATIC}/${CSS}"
fi

if [ ! -f "${STATIC}/${SPINNER}" ]; then
	wget "https://raw.githubusercontent.com/SamHerbert/SVG-Loaders/master/svg-loaders/${SPINNER}" --output-document="${STATIC}/${SPINNER}"
fi

if [ ! -f "secret_key" ]; then
	head -c 32 /dev/urandom | base64 -w 0 > "secret_key"
fi

if [ ! -f "tmp.db" ]; then
	source "${VENV}/bin/activate"
	python3 init_db.py
fi
