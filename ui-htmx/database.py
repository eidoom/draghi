from peewee import SqliteDatabase

db = SqliteDatabase(
    "tmp.db",
    pragmas={
        "foreign_keys": 1,
        "ignore_check_constraints": 0,
    },
)
