const themeAuto = document.getElementById("theme-auto"),
  themeLight = document.getElementById("theme-light"),
  themeDark = document.getElementById("theme-dark");

if (localStorage.getItem("theme")) {
  const theme = localStorage.getItem("theme");
  if (theme === "light") {
    themeLight.checked = true;
  } else if (theme === "dark") {
    themeDark.checked = true;
  }
}

themeAuto.addEventListener("input", () => {
  document.documentElement.removeAttribute("data-theme");
  localStorage.removeItem("theme");
});

themeLight.addEventListener("input", () => {
  document.documentElement.setAttribute("data-theme", "light");
  localStorage.setItem("theme", "light");
});

themeDark.addEventListener("input", () => {
  document.documentElement.setAttribute("data-theme", "dark");
  localStorage.setItem("theme", "dark");
});
