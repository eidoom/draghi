# ui-htmx

## Usage

Install dependencies with `./setup.bash` (this is automatically called by the run scripts).

Run development server (`flask`) with `./dev.bash`.

Run production server (`gunicorn`) with `./prod.bash`.

Sample `nginx` entry for reverse proxy:

```nginx
server {
    listen 80;
    listen [::]:80;

    server_name DOMAIN;

    location / {
        return 301 https://$host$request_uri;
    }
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name DOMAIN;

    ssl_certificate /etc/letsencrypt/live/DOMAIN/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/DOMAIN/privkey.pem;

    location / {
        proxy_pass http://127.0.0.1:8000/;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Prefix /;
    }
}
```

## Dependencies

* [SQLite3](https://www.sqlite.org/index.html)
* [Peewee](https://docs.peewee-orm.com/en/latest/)
* [Flask](https://flask.palletsprojects.com/en/2.2.x/)
* [Gunicorn](https://gunicorn.org/)
* [htmx.js](https://htmx.org/)
* [pico.css](https://picocss.com/)
* [SVG-Loaders](https://github.com/SamHerbert/SVG-Loaders)
* [Flask-Login](https://flask-login.readthedocs.io/en/latest/)

## Consider

* [WTForms](https://wtforms.readthedocs.io/en/3.0.x/)
* [Blocking these passwords](https://github.com/danielmiessler/SecLists/tree/master/Passwords/Common-Credentials)

## TODO

* [tests](https://flask.palletsprojects.com/en/2.2.x/testing/)
* Monsters
* Character UI
