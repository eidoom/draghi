#!/usr/bin/env python3

import json

from database import db
from models import *


def proc_weapon(weapon):
    out = {}
    for k, v in weapon.items():
        if k == "damage_type":
            out[k] = (
                DamageType.select(DamageType.id).where(DamageType.name == v).scalar()
            )
        elif k == "attack_type":
            out[k] = (
                AttackType.select(AttackType.id).where(AttackType.name == v).scalar()
            )
        elif k == "rarity":
            out[k] = Rarity.select(Rarity.id).where(Rarity.name == v).scalar()
        elif k != "properties":
            out[k] = v
    return out


def proc_armour(armour):
    out = {}
    for k, v in armour.items():
        if k == "rarity":
            out[k] = Rarity.select(Rarity.id).where(Rarity.name == v).scalar()
        elif k == "category":
            out[k] = (
                ArmourCategory.select(ArmourCategory.id)
                .where(ArmourCategory.name == v)
                .scalar()
            )
        elif k != "stealth_disadvantage":
            out[k] = v
    return out


def proc_magic_item(magic_item):
    out = {}
    for k, v in magic_item.items():
        if k == "rarity":
            out[k] = Rarity.select(Rarity.id).where(Rarity.name == v).scalar()
        else:
            out[k] = v
    return out


def proc_spell(spell):
    out = {}
    for k, v in spell.items():
        if k == "school":
            out[k] = (
                MagicSchool.select(MagicSchool.id).where(MagicSchool.name == v).scalar()
            )
        elif k == "level":
            out[k] = (
                SpellLevel.select(SpellLevel.id).where(SpellLevel.level == v).scalar()
            )
        elif k in (
            "name",
            "description",
            "school",
            "duration",
            "range",
            "casting_time",
        ):
            out[k] = v
    return out


@db.connection_context()
@db.atomic()
def init_db():
    tables = (
        Ability,
        Alignment,
        Armour,
        ArmourCategory,
        ArmourProficiency,
        ArmourStealthDisadvantage,
        AttackType,
        Attunement,
        CClass,
        # CClassArmourProficiency,
        CClassFeature,
        CClassSaveProficiency,
        CClassSpell,
        CClassSubFeature,
        CClassToolProficiency,
        # CClassWeaponProficiency,
        Campaign,
        CampaignCharacter,
        CampaignGameMaster,
        CantripsKnown,
        Character,
        CharacterAbility,
        CharacterArmour,
        CharacterArmourEquipped,
        CharacterCClass,
        CharacterCondition,
        CharacterPlayer,
        CharacterSubCClass,
        CharacterSubRace,
        CharacterWeapon,
        CharacterWeaponEquipped,
        Component,
        Condition,
        DamageType,
        Feat,
        FeatPrereq,
        Feature,
        FeatureLevel,
        Immunity,
        KnownSpell,
        Level,
        MagicItem,
        MagicSchool,
        Monster,
        MonsterAbility,
        MonsterType,
        Morality,
        Player,
        PreparedSpell,
        Race,
        RaceTrait,
        Rarity,
        Resistance,
        SaveProficiency,
        Size,
        Skill,
        SkillExpertise,
        SkillProficiency,
        Society,
        Spell,
        SpellAoE,
        SpellAttackType,
        SpellComponent,
        SpellConcentration,
        SpellDamageType,
        SpellLevel,
        SpellMaterial,
        SpellRitual,
        SpellSave,
        SpellShape,
        SpellSlot,
        SpellSlots,
        SpellcastingAbility,
        SpellsKnown,
        StartingCClass,
        SubCClass,
        SubCClassFeature,
        SubCClassSubFeature,
        SubFeature,
        SubFeatureLevel,
        SubFeatureParent,
        SubRace,
        SubRaceTrait,
        Tool,
        ToolCategory,
        ToolProficiency,
        Trait,
        UsedSlot,
        Vulnerability,
        Weapon,
        WeaponProficiency,
        WeaponProperty,
        WeaponPropertyDamage,
        WeaponPropertyDescription,
        WeaponPropertyRange,
        WeaponPropertySpecial,
    )

    db.create_tables(tables, safe=True)

    with open("admin_password", "r") as f:
        password = f.read().rstrip("\n")

    Player.insert(name="admin", password=password).on_conflict_ignore().execute()

    # Level
    with open("../static/xp.json", "r") as f:
        xp = json.load(f)

    Level.insert_many(
        enumerate(xp, start=1), fields=(Level.level, Level.threshold)
    ).execute()

    # Size
    with open("../static/size.json", "r") as f:
        sizes = json.load(f)

    Size.insert_many(sizes).execute()

    # Trait
    with open("../static/trait.json", "r") as f:
        traits = json.load(f)

    # not including dragonic ancestry options yet
    Trait.insert_many(
        {k: v for k, v in trait.items() if k in ("name", "description")}
        for trait in traits
        if trait["name"][-1] != ")"
    ).execute()

    # Race
    with open("../static/race.json", "r") as f:
        races = json.load(f)

    Race.insert_many(
        (
            {
                k: Size.get(Size.name == v) if k == "size" else v
                for k, v in race.items()
                if k
                in (
                    "name",
                    "size",
                    "speed",
                    "alignment_desc",
                    "age_desc",
                    "size_desc",
                    "language_desc",
                )
            }
            for race in races
        )
    ).execute()

    RaceTrait.insert_many(
        (
            (
                Race.get(Race.name == race["name"]),
                Trait.get(Trait.name == trait),
            )
            for race in races
            if "traits" in race
            for trait in race["traits"]
        ),
        fields=(RaceTrait.race, RaceTrait.trait),
    ).execute()

    SubRace.insert_many(
        (
            {"race": Race.get(Race.name == race["name"])}
            | {k: v for k, v in subrace.items() if k in ("name", "description")}
            for race in races
            if "subraces" in race
            for subrace in race["subraces"]
        )
    ).execute()

    SubRaceTrait.insert_many(
        (
            (
                SubRace.get(SubRace.name == subrace["name"]),
                Trait.get(Trait.name == trait),
            )
            for race in races
            if "subraces" in race
            for subrace in race["subraces"]
            if "traits" in subrace
            for trait in subrace["traits"]
        ),
        fields=(SubRaceTrait.subrace, SubRaceTrait.trait),
    ).execute()

    # SpellLevel
    with open("../static/spell.json", "r") as f:
        spells_raw = json.load(f)

        SpellLevel.insert_many(spells_raw["levels"]).execute()

    # CClass
    with open("../static/cclass.json", "r") as f:
        cclasses = json.load(f)["classes"]

    CClass.insert_many(
        (
            {k: cclass[k] for k in ("name", "hit_die", "subclass_flavour")}
            for cclass in cclasses
        )
    ).execute()

    CantripsKnown.insert_many(
        (
            (
                CClass.get(CClass.name == cclass["name"]),
                Level.get(Level.level == cclass_level),
                number,
            )
            for cclass in cclasses
            if "cantrips_known" in cclass
            for cclass_level, number in enumerate(cclass["cantrips_known"], start=1)
            if number
        ),
        fields=(
            CantripsKnown.cclass,
            CantripsKnown.cclass_level,
            CantripsKnown.number,
        ),
    ).execute()

    SpellsKnown.insert_many(
        (
            (
                CClass.get(CClass.name == cclass["name"]),
                Level.get(Level.level == cclass_level),
                number,
            )
            for cclass in cclasses
            if "spells_known" in cclass
            for cclass_level, number in enumerate(cclass["spells_known"], start=1)
            if number
        ),
        fields=(
            SpellsKnown.cclass,
            SpellsKnown.cclass_level,
            SpellsKnown.number,
        ),
    ).execute()

    SpellSlots.insert_many(
        (
            (
                CClass.get(CClass.name == cclass["name"]),
                Level.get(Level.level == cclass_level),
                SpellLevel.get(SpellLevel.level == spell_level),
                number,
            )
            for cclass in cclasses
            if "spell_slots" in cclass
            for cclass_level, slots in enumerate(cclass["spell_slots"], start=1)
            for spell_level, number in enumerate(slots, start=1)
            if number
        ),
        fields=(
            SpellSlots.cclass,
            SpellSlots.cclass_level,
            SpellSlots.spell_level,
            SpellSlots.number,
        ),
    ).execute()

    # SubCClass
    with open("../static/subclass.json", "r") as f:
        subcclasses = json.load(f)

    SubCClass.insert_many(
        (
            {
                k: CClass.select(CClass.id).where(CClass.name == subcclass[k]).scalar()
                if k == "cclass"
                else subcclass[k]
                for k in ("name", "short", "cclass", "description")
            }
            for subcclass in subcclasses
        )
    ).execute()

    # Ability
    with open("../static/ability.json", "r") as f:
        abilities = json.load(f)

        Ability.insert_many(abilities).execute()

    # SpellcastingAbility

    SpellcastingAbility.insert_many(
        (
            (
                CClass.get(CClass.name == cclass["name"]),
                Ability.get(Ability.short == cclass["spellcasting_ability"]),
            )
            for cclass in cclasses
            if "spellcasting_ability" in cclass
        ),
        fields=(SpellcastingAbility.cclass, SpellcastingAbility.ability),
    ).execute()

    # Skill
    with open("../static/skill.json", "r") as f:
        skills = json.load(f)

    Skill.insert_many(
        (
            {
                key: Ability.select(Ability.id).where(Ability.name == val).scalar()
                if key == "ability"
                else val
                for key, val in skill.items()
            }
            for skill in skills
        )
    ).execute()

    # Rarity
    with open("../static/rarity.json", "r") as f:
        rarities = json.load(f)

    Rarity.insert_many(rarities).execute()

    # Armour
    with open("../static/armour.json", "r") as f:
        armours = json.load(f)

    ArmourCategory.insert_many(armours["categories"]).execute()

    Armour.insert_many((proc_armour(armour) for armour in armours["armours"])).execute()

    ArmourStealthDisadvantage.insert_many(
        (
            (Armour.select(Armour.id).where(Armour.name == armour["name"]).scalar(),)
            for armour in armours["armours"]
            if armour["stealth_disadvantage"]
        ),
        fields=(ArmourStealthDisadvantage.armour,),
    ).execute()

    # DamageType, AttackType
    with open("../static/attack-damage.json", "r") as f:
        attack_damages = json.load(f)

    DamageType.insert_many(attack_damages["damage_types"]).execute()

    AttackType.insert_many(attack_damages["attack_types"]).execute()

    # Weapon
    with open("../static/weapon.json", "r") as f:
        weapons = json.load(f)

    WeaponPropertyDescription.insert_many(weapons["properties"]).execute()

    Weapon.insert_many((proc_weapon(weapon) for weapon in weapons["weapons"])).execute()

    WeaponProperty.insert_many(
        (
            (
                weapon,
                WeaponPropertyDescription.select(WeaponPropertyDescription.id)
                .where(
                    WeaponPropertyDescription.name == prop["name"],
                )
                .scalar(),
            )
            for weapon, props in (
                (
                    Weapon.select(Weapon.id).where(Weapon.name == w["name"]).scalar(),
                    w["properties"],
                )
                for w in weapons["weapons"]
                if "properties" in w
            )
            for prop in props
        ),
        fields=(
            WeaponProperty.weapon,
            WeaponProperty.property,
        ),
    ).execute()

    WeaponPropertyDamage.insert_many(
        (
            (
                WeaponProperty.select(WeaponProperty.id)
                .join(WeaponPropertyDescription)
                .join_from(WeaponProperty, Weapon)
                .where(WeaponPropertyDescription.name == prop["name"])
                .where(Weapon.name == weapon["name"])
                .scalar(),
                prop["damage"],
            )
            for weapon in weapons["weapons"]
            if "properties" in weapon
            for prop in weapon["properties"]
            if prop["name"] == "Versatile"
        ),
        fields=(
            WeaponPropertyDamage.property,
            WeaponPropertyDamage.damage,
        ),
    ).execute()

    WeaponPropertyRange.insert_many(
        (
            (
                WeaponProperty.select(WeaponProperty.id)
                .join(WeaponPropertyDescription)
                .join_from(WeaponProperty, Weapon)
                .where(WeaponPropertyDescription.name == prop["name"])
                .where(Weapon.name == weapon["name"])
                .scalar(),
                prop["min"],
                prop["max"],
            )
            for weapon in weapons["weapons"]
            if "properties" in weapon
            for prop in weapon["properties"]
            if prop["name"] == "Thrown"
        ),
        fields=(
            WeaponPropertyRange.property,
            WeaponPropertyRange.min,
            WeaponPropertyRange.max,
        ),
    ).execute()

    WeaponPropertySpecial.insert_many(
        (
            (
                WeaponProperty.select(WeaponProperty.id)
                .join(WeaponPropertyDescription)
                .join_from(WeaponProperty, Weapon)
                .where(WeaponPropertyDescription.name == prop["name"])
                .where(Weapon.name == weapon["name"])
                .scalar(),
                prop["description"],
            )
            for weapon in weapons["weapons"]
            if "properties" in weapon
            for prop in weapon["properties"]
            if prop["name"] == "Special"
        ),
        fields=(
            WeaponPropertySpecial.property,
            WeaponPropertySpecial.description,
        ),
    ).execute()

    # MagicSchool
    with open("../static/magic-school.json", "r") as f:
        magic_schools = json.load(f)

    MagicSchool.insert_many(magic_schools).execute()

    # Component
    with open("../static/component.json", "r") as f:
        components = json.load(f)

    Component.insert_many(components).execute()

    # Spell

    SpellShape.insert_many(spells_raw["area_of_effect"]).execute()

    spells = spells_raw["spells"]

    Spell.insert_many((proc_spell(spell) for spell in spells)).execute()

    SpellComponent.insert_many(
        (
            (
                Spell.select(Spell.id).where(Spell.name == spell["name"]).scalar(),
                Component.select(Component.id)
                .where(Component.short == component)
                .scalar(),
            )
            for spell in spells
            for component in spell["components"]
        ),
        fields=(
            SpellComponent.spell,
            SpellComponent.component,
        ),
    ).execute()

    SpellMaterial.insert_many(
        (
            (
                SpellComponent.select(SpellComponent.id)
                .join(Spell)
                .where(Spell.name == spell["name"])
                .join_from(SpellComponent, Component)
                .where(Component.short == "M")
                .scalar(),
                spell["material"],
            )
            for spell in spells
            if "material" in spell
        ),
        fields=(
            SpellMaterial.spell_component,
            SpellMaterial.material,
        ),
    ).execute()

    CClassSpell.insert_many(
        (
            (
                Spell.select(Spell.id).where(Spell.name == spell["name"]).scalar(),
                CClass.select(CClass.id).where(CClass.name == cclass).scalar(),
            )
            for spell in spells
            for cclass in spell["classes"]
        ),
        fields=(
            CClassSpell.spell,
            CClassSpell.cclass,
        ),
    ).execute()

    SpellRitual.insert_many(
        (
            (Spell.select(Spell.id).where(Spell.name == spell["name"]).scalar(),)
            for spell in spells
            if spell["ritual"]
        ),
        fields=(SpellRitual.spell,),
    ).execute()

    SpellConcentration.insert_many(
        (
            (Spell.select(Spell.id).where(Spell.name == spell["name"]).scalar(),)
            for spell in spells
            if spell["concentration"]
        ),
        fields=(SpellConcentration.spell,),
    ).execute()

    SpellAttackType.insert_many(
        (
            (
                Spell.select(Spell.id).where(Spell.name == spell["name"]).scalar(),
                AttackType.select(AttackType.id)
                .where(AttackType.name == spell["attack_type"])
                .scalar(),
            )
            for spell in spells
            if "attack_type" in spell
        ),
        fields=(
            SpellAttackType.spell,
            SpellAttackType.attack_type,
        ),
    ).execute()

    SpellDamageType.insert_many(
        (
            (
                Spell.select(Spell.id).where(Spell.name == spell["name"]).scalar(),
                DamageType.select(DamageType.id)
                .where(DamageType.name == spell["damage"]["damage_type"])
                .scalar(),
            )
            for spell in spells
            if "damage" in spell and "damage_type" in spell["damage"]
        ),
        fields=(
            SpellDamageType.spell,
            SpellDamageType.damage_type,
        ),
    ).execute()

    SpellSave.insert_many(
        (
            (
                Spell.select(Spell.id).where(Spell.name == spell["name"]).scalar(),
                Ability.select(Ability.id)
                .where(Ability.short == spell["dc"]["dc_type"])
                .scalar(),
            )
            for spell in spells
            if "dc" in spell
        ),
        fields=(
            SpellSave.spell,
            SpellSave.save,
        ),
    ).execute()

    SpellAoE.insert_many(
        (
            (
                Spell.select(Spell.id).where(Spell.name == spell["name"]).scalar(),
                SpellShape.select(SpellShape.id)
                .where(SpellShape.name == spell["area_of_effect"]["damage_type"])
                .scalar(),
                spell["area_of_effect"]["size"],
            )
            for spell in spells
            if "area_of_effect" in spell
        ),
        fields=(
            SpellAoE.spell,
            SpellAoE.shape,
            SpellAoE.size,
        ),
    ).execute()

    # MagicItem
    with open("../static/magic-item.json", "r") as f:
        magic_items = json.load(f)

    MagicItem.insert_many(
        (proc_magic_item(magic_item) for magic_item in magic_items)
    ).execute()

    Attunement.insert_many(
        (
            (
                MagicItem.select(MagicItem.id)
                .where(MagicItem.name == item["name"])
                .scalar(),
            )
            for item in magic_items
            if "attunement" in item
        ),
        fields=(Attunement.magic_item,),
    ).execute()

    # Condition
    with open("../static/condition.json", "r") as f:
        conditions = json.load(f)

        Condition.insert_many(conditions).execute()

    with open("../static/alignment.json", "r") as f:
        alignment = json.load(f)

    # Alignment

    with open("../static/alignment.json", "r") as f:
        alignment = json.load(f)

    Morality.insert_many(
        ((m,) for m in alignment["morality"]), fields=(Morality.name,)
    ).execute()
    Society.insert_many(
        ((s,) for s in alignment["society"]), fields=(Society.name,)
    ).execute()
    Alignment.insert_many(
        (
            {
                **a,
                "morality": Morality.get(Morality.name == a["name"].split(" ")[-1]),
                "society": Society.get(Society.name == a["name"].split(" ")[0]),
            }
            for a in alignment["alignment"]
        ),
        fields=(),
    ).execute()

    # Feat
    with open("../static/feat.json", "r") as f:
        feats = json.load(f)

    Feat.insert_many(
        ([feat[k] for k in ("name", "description")] for feat in feats),
        fields=(Feat.name, Feat.description),
    ).execute()

    FeatPrereq.insert_many(
        (
            (
                Feat.get(Feat.name == feat["name"]),
                Ability.get(Ability.short == prereq["ability"]),
                prereq["minimum_score"],
            )
            for feat in feats
            for prereq in feat["prerequisites"]
        ),
        fields=(FeatPrereq.feat, FeatPrereq.ability, FeatPrereq.score),
    ).execute()

    # Feature
    with open("../static/feature.json", "r") as f:
        features = json.load(f)

    Feature.insert_many(
        (
            [feature[k] for k in ("name", "description")]
            for feature in features
            if "parents" not in feature
        ),
        fields=(Feature.name, Feature.description),
    ).execute()

    FeatureLevel.insert_many(
        (
            (
                Feature.get(Feature.name == feature["name"]),
                Level.get(Level.level == level),
            )
            for feature in features
            if "parents" not in feature
            for level in feature["levels"]
        ),
        fields=(FeatureLevel.feature, FeatureLevel.level),
    ).execute()

    CClassFeature.insert_many(
        (
            (
                Feature.get(Feature.name == feature["name"]),
                CClass.get(CClass.name == name),
            )
            for feature in features
            if "classes" in feature and "parents" not in feature
            for name in feature["classes"]
        ),
        fields=(CClassFeature.feature, CClassFeature.cclass),
    ).execute()

    SubCClassFeature.insert_many(
        (
            (
                Feature.get(Feature.name == feature["name"]),
                SubCClass.get(SubCClass.short == name),
            )
            for feature in features
            if "subclasses" in feature and "parents" not in feature
            for name in feature["subclasses"]
        ),
        fields=(SubCClassFeature.feature, SubCClassFeature.subcclass),
    ).execute()

    # SubFeature

    SubFeature.insert_many(
        (
            (
                feature["name"],
                feature["description"],
            )
            for feature in features
            if "parents" in feature
        ),
        fields=(SubFeature.name, SubFeature.description),
    ).execute()

    SubFeatureParent.insert_many(
        (
            (
                Feature.get(Feature.name == parent),
                SubFeature.get(SubFeature.name == feature["name"]),
            )
            for feature in features
            if "parents" in feature
            for parent in feature["parents"]
        ),
        fields=(SubFeatureParent.feature, SubFeatureParent.subfeature),
    ).execute()

    SubFeatureLevel.insert_many(
        (
            (
                SubFeature.get(SubFeature.name == feature["name"]),
                Level.get(Level.level == level),
            )
            for feature in features
            if "parents" in feature
            for level in feature["levels"]
        ),
        fields=(SubFeatureLevel.feature, SubFeatureLevel.level),
    ).execute()

    CClassSubFeature.insert_many(
        (
            (
                SubFeature.get(SubFeature.name == feature["name"]),
                CClass.get(CClass.name == name),
            )
            for feature in features
            if "classes" in feature and "parents" in feature
            for name in feature["classes"]
        ),
        fields=(CClassSubFeature.feature, CClassSubFeature.cclass),
    ).execute()

    SubCClassSubFeature.insert_many(
        (
            (
                SubFeature.get(SubFeature.name == feature["name"]),
                SubCClass.get(SubCClass.short == name),
            )
            for feature in features
            if "subclasses" in feature and "parents" in feature
            for name in feature["subclasses"]
        ),
        fields=(SubCClassSubFeature.feature, SubCClassSubFeature.subcclass),
    ).execute()

    # spellcasting

    SubFeature.insert_many(
        (
            (
                f"{feature['name']}: {cclass['name']}",
                feature["description"],
            )
            for cclass in cclasses
            if "spellcasting" in cclass
            for feature in cclass["spellcasting"]
        ),
        fields=(SubFeature.name, SubFeature.description),
    ).execute()

    SubFeatureParent.insert_many(
        (
            (
                Feature.get(Feature.name == cclass["spell_parent"]),
                SubFeature.get(
                    SubFeature.name == f"{feature['name']}: {cclass['name']}"
                ),
            )
            for cclass in cclasses
            if "spellcasting" in cclass
            for feature in cclass["spellcasting"]
        ),
        fields=(
            SubFeatureParent.feature,
            SubFeatureParent.subfeature,
        ),
    ).execute()

    CClassSubFeature.insert_many(
        (
            (
                SubFeature.get(
                    SubFeature.name == f"{feature['name']}: {cclass['name']}"
                ),
                CClass.get(CClass.name == cclass["name"]),
            )
            for cclass in cclasses
            if "spellcasting" in cclass
            for feature in cclass["spellcasting"]
        ),
        fields=(CClassSubFeature.feature, CClassSubFeature.cclass),
    ).execute()

    # Tool
    with open("../static/tool.json", "r") as f:
        tools = json.load(f)

    ToolCategory.insert_many(
        {"name": n} for n in set(tool["category"] for tool in tools)
    ).execute()

    Tool.insert_many(
        {
            k: ToolCategory.get(ToolCategory.name == v) if k == "category" else v
            for k, v in tool.items()
        }
        for tool in tools
    ).execute()

    # CClass Proficiencies

    # CClassWeaponProficiency.insert_many(
    #     (
    #         {
    #             "cclass": CClass.get(CClass.name == cclass["name"]),
    #             "weapon": Weapon.get(Weapon.name == weapon),
    #         }
    #         for cclass in cclasses
    #         if "proficiencies" in cclass and "weapons" in cclass["proficiencies"]
    #         for weapon in cclass["proficiencies"]["weapons"]
    #     )
    # ).execute()

    # CClassArmourProficiency.insert_many(
    #     (
    #         {
    #             "cclass": CClass.get(CClass.name == cclass["name"]),
    #             "armour": Armour.get(Armour.name == armour),
    #         }
    #         for cclass in cclasses
    #         if "proficiencies" in cclass and "armours" in cclass["proficiencies"]
    #         for armour in cclass["proficiencies"]["armours"]
    #     )
    # ).execute()

    CClassToolProficiency.insert_many(
        (
            {
                "cclass": CClass.get(CClass.name == cclass["name"]),
                "tool": Tool.get(Tool.name == tool),
            }
            for cclass in cclasses
            if "proficiencies" in cclass and "tools" in cclass["proficiencies"]
            for tool in cclass["proficiencies"]["tools"]
        )
    ).execute()

    CClassSaveProficiency.insert_many(
        (
            {
                "cclass": CClass.get(CClass.name == cclass["name"]),
                "ability": Ability.get(Ability.name == ability),
            }
            for cclass in cclasses
            if "proficiencies" in cclass and "saving_throws" in cclass["proficiencies"]
            for ability in cclass["proficiencies"]["saving_throws"]
        )
    ).execute()

    # MonsterType
    with open("../static/monster-type.json", "r") as f:
        monster_types = json.load(f)

    MonsterType.insert_many(monster_types).execute()

    # Monster
    with open("../static/monster.json", "r") as f:
        monsters = json.load(f)

    Monster.insert_many(
        {
            "name": monster["name"],
            "hp": monster["hit_points"],
            "type": MonsterType.get(
                MonsterType.name == monster["type"].title().replace(" Of ", " of ")
            ),
            "description": monster["desc"] if "desc" in monster else "",
            "size": Size.get(Size.name == monster["size"]),
            "ac": monster["armour_class"],
            "cr": monster["challenge_rating"] if "challenge_rating" in monster else 0,
            "xp": monster["xp"],
            # "alignment": Alignment.get(Alignment.name == monster["alignment"].title()),
        }
        for monster in monsters
    ).execute()

    MonsterAbility.insert_many(
        {
            "ability": ability,
            "monster": Monster.get(Monster.name == monster["name"]),
            "score": monster[ability.name.lower()],
        }
        for monster in monsters
        for ability in Ability.select()
    ).execute()


@db.connection_context()
@db.atomic()
def create_mock_player():
    campaign = Campaign.insert(name="Primis").on_conflict_ignore().execute()

    CampaignGameMaster.insert(
        game_master=Player.get(Player.name == "admin"), campaign=campaign
    ).execute()

    player = Player.insert(name="Echad", password="test").on_conflict_ignore().execute()

    character = (
        Character.insert(
            name="Aleph",
            race=Race.select(Race.id).where(Race.name == "Elf").scalar(),
            xp=1000,
            max_hp=18,
            hp=8,
            alignment=Alignment.get(Alignment.short == "N"),
        )
        .on_conflict_ignore()
        .execute()
    )

    CharacterSubRace.insert(
        character=character, subrace=SubRace.get(SubRace.name == "High Elf")
    ).execute()

    CharacterPlayer.insert(player=player, character=character).execute()

    CampaignCharacter.insert(character=character, campaign=campaign).execute()

    ability_ids = Ability.select(Ability.id)

    CharacterAbility.insert_many(
        (
            {"character": character, "ability": ability, "score": 8 + i}
            for i, ability in enumerate(ability_ids)
        )
    ).execute()

    wizard = CClass.select(CClass.id).where(CClass.name == "Wizard").scalar()

    ccclass = CharacterCClass.insert_many(
        (
            {
                "character": character,
                "cclass": wizard,
                "level": 2,
            },
            {
                "character": character,
                "cclass": CClass.select(CClass.id)
                .where(CClass.name == "Barbarian")
                .scalar(),
            },
        )
    ).execute()

    ccclass -= 1

    StartingCClass.insert({"character_cclass": ccclass}).execute()

    slot_levels = (
        SpellSlots.select()
        .join(CClass)
        .join_from(SpellSlots, SpellLevel)
        .where((CClass.name == "Wizard") & (SpellSlots.cclass_level == 2))
    )

    SpellSlot.insert_many(
        (
            (ccclass, slots.spell_level)
            for slots in slot_levels
            for _ in range(slots.number)
        ),
        fields=(SpellSlot.ccclass, SpellSlot.level),
    ).execute()

    UsedSlot.insert_many(((1,), (2,)), fields=(UsedSlot.slot,)).execute()

    known_spell = KnownSpell.insert_many(
        (
            {"ccclass": ccclass, "spell": Spell.get(Spell.name == spell)}
            for spell in ("Acid Splash", "Alarm")
        )
    ).execute()

    PreparedSpell.insert(ccclass=ccclass, spell=known_spell).execute()

    SaveProficiency.insert_many(
        (
            {
                "character_ability": (
                    CharacterAbility.select(CharacterAbility.id)
                    .join(Ability)
                    .where(Ability.short == ability)
                    .where(CharacterAbility.character == character)
                    .scalar()
                )
            }
            for ability in ("DEX", "WIS")
        )
    ).execute()

    prof = SkillProficiency.insert_many(
        (
            {
                "skill": (Skill.select(Skill.id).where(Skill.name == skill).scalar()),
                "character": character,
            }
            for skill in ("Insight", "History")
        )
    ).execute()

    SkillExpertise.insert(proficiency=prof).execute()

    CharacterCondition.insert_many(
        (
            {
                "condition": (
                    Condition.select(Condition.id)
                    .where(Condition.name == condition)
                    .scalar()
                ),
                "character": character,
            }
            for condition in ("Charmed", "Prone")
        )
    ).execute()


if __name__ == "__main__":
    if not db.get_tables():
        init_db()
        create_mock_player()
