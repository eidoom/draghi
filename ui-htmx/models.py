#!/usr/bin/env python3

import datetime

from peewee import (
    CharField,
    Check,
    DateTimeField,
    FloatField,
    ForeignKeyField,
    IntegerField,
    Model,
    TextField,
)

from database import db


class BaseModel(Model):
    class Meta:
        database = db


class Player(BaseModel):
    name = CharField(unique=True)
    password = CharField()

    join_datetime = DateTimeField(default=datetime.datetime.now(datetime.timezone.utc))
    bio = TextField(default="")

    is_authenticated = True
    is_active = True
    is_anonymous = False

    def get_id(self):
        return str(self.id)


class Campaign(BaseModel):
    name = CharField(unique=True)
    create_datetime = DateTimeField(
        default=datetime.datetime.now(datetime.timezone.utc)
    )


class CampaignGameMaster(BaseModel):
    campaign = ForeignKeyField(Campaign, backref="game_masters")
    game_master = ForeignKeyField(Player, backref="campaigns")

    class Meta:
        indexes = ((("campaign", "game_master"), True),)


class Level(BaseModel):
    level = IntegerField(
        primary_key=True,
        constraints=[Check("level > 0"), Check("level <= 20")],
        unique=True,
    )
    threshold = IntegerField()


class Trait(BaseModel):
    name = CharField(unique=True)
    description = TextField()
    # [RaceTrait]
    # [SubRaceTrait]


class Size(BaseModel):
    name = CharField(unique=True)
    reach = FloatField()


class Race(BaseModel):
    name = CharField(unique=True)
    size = ForeignKeyField(Size, backref="races")
    speed = IntegerField()
    alignment_desc = TextField()
    age_desc = TextField()
    size_desc = TextField()
    language_desc = TextField()
    # [SubRace]
    # [RaceTrait]


class RaceTrait(BaseModel):
    race = ForeignKeyField(Race, backref="traits")
    trait = ForeignKeyField(Trait, backref="races")

    class Meta:
        indexes = ((("race", "trait"), True),)


class SubRace(BaseModel):
    name = CharField(unique=True)
    race = ForeignKeyField(Race, backref="subraces")
    description = TextField()
    # [SubRaceTrait]


class SubRaceTrait(BaseModel):
    subrace = ForeignKeyField(SubRace, backref="traits")
    trait = ForeignKeyField(Trait, backref="subraces")

    class Meta:
        indexes = ((("subrace", "trait"), True),)


class Morality(BaseModel):
    name = CharField(unique=True)


class Society(BaseModel):
    name = CharField(unique=True)


class Alignment(BaseModel):
    morality = ForeignKeyField(Morality)
    society = ForeignKeyField(Society)
    name = CharField(unique=True)
    short = CharField(unique=True, max_length=2)
    description = TextField()

    class Meta:
        indexes = ((("morality", "society"), True),)


class Creature(BaseModel):
    # Base for Character and Monster
    name = CharField(unique=True)
    hp = IntegerField(default=0)


class MonsterType(BaseModel):
    name = CharField(unique=True)
    description = TextField()


class Monster(Creature):
    type = ForeignKeyField(MonsterType)
    description = TextField(default="")
    size = ForeignKeyField(Size)
    ac = IntegerField()  # armour class
    cr = FloatField(default=0)  # challenge rating
    xp = IntegerField()


class Character(Creature):
    race = ForeignKeyField(Race, backref="characters")
    max_hp = IntegerField(default=0)
    xp = IntegerField(default=0)
    notes = TextField(default="")
    tmp_hp = IntegerField(default=0)
    alignment = ForeignKeyField(Alignment)
    # [CharacterAbility]
    # [SkillProficiency]
    # [CharacterArmour]
    # [CharacterCClass]
    # [CharacterWeapon]
    # [ArmourProficiency]
    # [CharacterCondition]
    # [CharacterPlayer]


class CharacterSubRace(BaseModel):
    character = ForeignKeyField(Character)
    subrace = ForeignKeyField(SubRace)

    class Meta:
        indexes = ((("subrace", "character"), True),)


class CampaignCharacter(BaseModel):
    campaign = ForeignKeyField(Campaign, backref="characters")
    character = ForeignKeyField(Character, backref="campaigns")

    class Meta:
        indexes = ((("campaign", "character"), True),)


class CharacterPlayer(BaseModel):
    player = ForeignKeyField(Player)
    character = ForeignKeyField(Character)

    class Meta:
        indexes = ((("player", "character"), True),)


class CClass(BaseModel):
    name = CharField(unique=True)
    hit_die = IntegerField()
    subclass_flavour = CharField()
    # [SubCClass]
    # [CharacterCClass]
    # [CClassSpell]
    # [SpellsKnown]
    # [CantripsKnown]


class SpellLevel(BaseModel):
    level = IntegerField(unique=True)
    name = CharField(unique=True)


class CantripsKnown(BaseModel):
    cclass = ForeignKeyField(CClass)
    cclass_level = ForeignKeyField(Level)
    number = IntegerField()

    class Meta:
        indexes = ((("cclass", "cclass_level"), True),)


class SpellsKnown(BaseModel):
    cclass = ForeignKeyField(CClass)
    cclass_level = ForeignKeyField(Level)
    number = IntegerField()

    class Meta:
        indexes = ((("cclass", "cclass_level"), True),)


class SpellSlots(BaseModel):
    cclass = ForeignKeyField(CClass)
    cclass_level = ForeignKeyField(Level)
    spell_level = ForeignKeyField(SpellLevel)
    number = IntegerField()

    class Meta:
        indexes = ((("cclass", "cclass_level", "spell_level"), True),)


class SubCClass(BaseModel):
    name = CharField(unique=True)
    short = CharField(unique=True)
    cclass = ForeignKeyField(CClass, backref="subcclasses")
    description = TextField()
    # [CharacterSubCClass]


class CharacterCClass(BaseModel):
    cclass = ForeignKeyField(CClass, backref="characters")
    character = ForeignKeyField(Character, backref="cclasses")
    level = ForeignKeyField(Level, default=1)
    # ?CharacterSubCClass
    # ?StartingCClass

    class Meta:
        indexes = ((("cclass", "character"), True),)


class StartingCClass(BaseModel):
    character_cclass = ForeignKeyField(
        CharacterCClass, backref="subcclasses", unique=True
    )


class SpellSlot(BaseModel):
    ccclass = ForeignKeyField(CharacterCClass)
    level = ForeignKeyField(SpellLevel)


class UsedSlot(BaseModel):
    slot = ForeignKeyField(SpellSlot, unique=True)


class CharacterSubCClass(BaseModel):
    character_cclass = ForeignKeyField(CharacterCClass, backref="subcclasses")
    subcclass = ForeignKeyField(SubCClass, backref="character_cclasses")

    class Meta:
        indexes = ((("character_cclass", "subcclass"), True),)


class Ability(BaseModel):
    name = CharField(unique=True)
    short = CharField(unique=True, max_length=3)
    measure = TextField()
    description = TextField()
    # [CharacterAbility]
    # [Skill]


class MonsterAbility(BaseModel):
    ability = ForeignKeyField(Ability, backref="monsters")
    monster = ForeignKeyField(Monster, backref="abilities")
    score = IntegerField(
        default=0, constraints=[Check("score >= 0"), Check("score <= 30")]
    )
    # ?SaveProficiency

    class Meta:
        indexes = ((("ability", "monster"), True),)


class CharacterAbility(BaseModel):
    ability = ForeignKeyField(Ability, backref="characters")
    character = ForeignKeyField(Character, backref="abilities")
    score = IntegerField(
        default=0, constraints=[Check("score >= 0"), Check("score <= 30")]
    )
    # ?SaveProficiency

    class Meta:
        indexes = ((("ability", "character"), True),)


class SpellcastingAbility(BaseModel):
    ability = ForeignKeyField(Ability)
    cclass = ForeignKeyField(CClass)

    class Meta:
        indexes = ((("ability", "cclass"), True),)


class SaveProficiency(BaseModel):
    character_ability = ForeignKeyField(
        CharacterAbility, unique=True, backref="proficient"
    )


class Skill(BaseModel):
    name = CharField(unique=True)
    description = TextField()
    ability = ForeignKeyField(Ability, backref="skills")
    # [SkillProficiency]


class SkillProficiency(BaseModel):
    skill = ForeignKeyField(Skill, backref="characters")
    character = ForeignKeyField(Character, backref="skills")

    class Meta:
        indexes = ((("skill", "character"), True),)


class SkillExpertise(BaseModel):
    proficiency = ForeignKeyField(SkillProficiency, unique=True)


class ArmourCategory(BaseModel):
    name = CharField(unique=True)
    description = TextField()
    degree = IntegerField()
    # [Armour]


class Rarity(BaseModel):
    name = CharField(unique=True)
    degree = IntegerField()


class Armour(BaseModel):
    name = CharField(unique=True)
    category = ForeignKeyField(ArmourCategory, backref="armours")
    rarity = ForeignKeyField(Rarity, backref="armours")
    ac = IntegerField()  # armour class
    cost = IntegerField()  # cp (copper pieces)
    weight = IntegerField()
    description = TextField()
    # ?ArmourStealthDisadvantage
    # [CharacterArmour]
    # [ArmourProficiency]


class ArmourStealthDisadvantage(BaseModel):
    armour = ForeignKeyField(Armour, backref="stealth_disadvantage", unique=True)


class ArmourProficiency(BaseModel):
    armour = ForeignKeyField(Armour, backref="proficient_characters")
    character = ForeignKeyField(Character, backref="armour_proficiencies")

    class Meta:
        indexes = ((("armour", "character"), True),)


class CharacterArmour(BaseModel):
    armour = ForeignKeyField(Armour, backref="owned_by")
    character = ForeignKeyField(Character, backref="armours")
    # ?CharacterArmourEquipped


class CharacterArmourEquipped(BaseModel):
    character_armour = ForeignKeyField(CharacterArmour, backref="equipped", unique=True)


class DamageType(BaseModel):
    name = CharField(unique=True)
    description = TextField()


class AttackType(BaseModel):
    name = CharField(unique=True)


class Weapon(BaseModel):
    name = CharField(unique=True)
    rarity = ForeignKeyField(Rarity, backref="weapons")
    damage = CharField()
    damage_type = ForeignKeyField(DamageType, backref="weapons")
    level = CharField()
    attack_type = ForeignKeyField(AttackType, backref="weapons")
    cost = IntegerField()  # cp
    weight = IntegerField()
    # [WeaponProperty]
    # [CharacterWeapon]
    # [WeaponProficiency]


class WeaponProficiency(BaseModel):
    weapon = ForeignKeyField(Weapon, backref="proficient_characters")
    character = ForeignKeyField(Character, backref="weapon_proficiencies")

    class Meta:
        indexes = ((("weapon", "character"), True),)


class CharacterWeapon(BaseModel):
    weapon = ForeignKeyField(Weapon, backref="characters")
    character = ForeignKeyField(Character, backref="weapons")
    # ?CharacterWeaponEquipped


class CharacterWeaponEquipped(BaseModel):
    character_weapon = ForeignKeyField(CharacterWeapon, backref="equipped", unique=True)


class WeaponPropertyDescription(BaseModel):
    name = CharField(unique=True)
    description = TextField()
    # [WeaponProperty}


class WeaponProperty(BaseModel):
    weapon = ForeignKeyField(Weapon, backref="properties")
    property = ForeignKeyField(WeaponPropertyDescription, backref="weapons")
    # ?WeaponPropertyDamage
    # ?WeaponPropertyRange
    # ?WeaponPropertySpecial

    class Meta:
        indexes = ((("weapon", "property"), True),)


class WeaponPropertyDamage(BaseModel):
    property = ForeignKeyField(WeaponProperty, backref="damage", unique=True)
    damage = CharField()


class WeaponPropertyRange(BaseModel):
    property = ForeignKeyField(WeaponProperty, backref="range", unique=True)
    min = IntegerField()
    max = IntegerField()


class WeaponPropertySpecial(BaseModel):
    property = ForeignKeyField(WeaponProperty, backref="special", unique=True)
    description = TextField()


class MagicSchool(BaseModel):
    name = CharField(unique=True)
    description = TextField()


class Component(BaseModel):
    name = CharField(unique=True)
    short = CharField(unique=True, max_length=1)
    description = TextField()


class Spell(BaseModel):
    name = CharField(unique=True)
    description = TextField()
    level = ForeignKeyField(SpellLevel, backref="spells")
    duration = CharField()
    casting_time = CharField()
    range = CharField()
    school = ForeignKeyField(MagicSchool, backref="spells")
    # [SpellComponent]
    # [CClassSpell]
    # ?SpellConcentration
    # ?SpellRitual
    # ?SpellAttackType
    # ?SpellDamageType
    # ?SpellSave
    # ?SpellAoE


class KnownSpell(BaseModel):
    ccclass = ForeignKeyField(CharacterCClass)
    spell = ForeignKeyField(Spell)

    class Meta:
        indexes = ((("ccclass", "spell"), True),)


class PreparedSpell(BaseModel):
    ccclass = ForeignKeyField(CharacterCClass)
    spell = ForeignKeyField(KnownSpell)

    class Meta:
        indexes = ((("ccclass", "spell"), True),)


class SpellShape(BaseModel):
    name = CharField(unique=True)
    description = TextField()


class SpellAoE(BaseModel):
    shape = ForeignKeyField(SpellShape, backref="spells")
    spell = ForeignKeyField(Spell, backref="aoes")
    size = IntegerField()

    class Meta:
        indexes = ((("shape", "spell"), True),)


class SpellDamageType(BaseModel):
    damage_type = ForeignKeyField(DamageType, backref="spells")
    spell = ForeignKeyField(Spell, backref="damage_types")

    class Meta:
        indexes = ((("damage_type", "spell"), True),)


class SpellAttackType(BaseModel):
    attack_type = ForeignKeyField(AttackType, backref="spells")
    spell = ForeignKeyField(Spell, backref="attack_types")

    class Meta:
        indexes = ((("attack_type", "spell"), True),)


class SpellSave(BaseModel):
    save = ForeignKeyField(Ability, backref="save_spells")
    spell = ForeignKeyField(Spell, backref="saves")

    class Meta:
        indexes = ((("save", "spell"), True),)


class SpellRitual(BaseModel):
    spell = ForeignKeyField(Spell, backref="ritual", unique=True)


class SpellConcentration(BaseModel):
    spell = ForeignKeyField(Spell, backref="concentration", unique=True)


class SpellComponent(BaseModel):
    spell = ForeignKeyField(Spell, backref="components")
    component = ForeignKeyField(Component, backref="spells")
    # ?SpellMaterial

    class Meta:
        indexes = ((("component", "spell"), True),)


class SpellMaterial(BaseModel):
    spell_component = ForeignKeyField(SpellComponent, backref="material", unique=True)
    material = TextField()


class CClassSpell(BaseModel):
    spell = ForeignKeyField(Spell, backref="classes")
    cclass = ForeignKeyField(CClass, backref="spells")

    class Meta:
        indexes = ((("cclass", "spell"), True),)


class MagicItem(BaseModel):
    name = CharField(unique=True)
    category = CharField()
    rarity = ForeignKeyField(Rarity, backref="weapons")
    description = TextField()
    # ?Attunement


class Attunement(BaseModel):
    magic_item = ForeignKeyField(MagicItem, backref="magic_items", unique=True)


class Condition(BaseModel):
    name = CharField(unique=True)
    description = TextField()
    # [CharacterCondition]


class CharacterCondition(BaseModel):
    character = ForeignKeyField(Character, backref="conditions")
    condition = ForeignKeyField(Condition, backref="characters")

    class Meta:
        indexes = ((("character", "condition"), True),)


class Vulnerability(BaseModel):
    damage_type = ForeignKeyField(DamageType, backref="vulnerable_characters")
    character = ForeignKeyField(Character, backref="vulnerabilities")

    class Meta:
        indexes = ((("character", "damage_type"), True),)


class Resistance(BaseModel):
    damage_type = ForeignKeyField(DamageType, backref="resistant_characters")
    character = ForeignKeyField(Character, backref="resistances")

    class Meta:
        indexes = ((("character", "damage_type"), True),)


class Immunity(BaseModel):
    damage_type = ForeignKeyField(DamageType, backref="immune_characters")
    character = ForeignKeyField(Character, backref="immunities")

    class Meta:
        indexes = ((("character", "damage_type"), True),)


class Feat(BaseModel):
    name = CharField(unique=True)
    description = TextField()


class FeatPrereq(BaseModel):
    feat = ForeignKeyField(Feat)
    ability = ForeignKeyField(Ability)
    score = IntegerField()

    class Meta:
        indexes = ((("feat", "ability"), True),)


class Feature(BaseModel):
    name = CharField(unique=True)
    description = TextField()


class FeatureLevel(BaseModel):
    level = ForeignKeyField(Level)
    feature = ForeignKeyField(Feature)

    class Meta:
        indexes = ((("level", "feature"), True),)


class CClassFeature(BaseModel):
    cclass = ForeignKeyField(CClass)
    feature = ForeignKeyField(Feature)

    class Meta:
        indexes = ((("cclass", "feature"), True),)


class SubFeature(BaseModel):
    name = CharField(unique=True)
    description = TextField()


class SubFeatureParent(BaseModel):
    subfeature = ForeignKeyField(SubFeature)
    feature = ForeignKeyField(Feature)

    class Meta:
        indexes = ((("subfeature", "feature"), True),)


class SubCClassFeature(BaseModel):
    subcclass = ForeignKeyField(SubCClass)
    feature = ForeignKeyField(Feature)

    class Meta:
        indexes = ((("subcclass", "feature"), True),)


class SubFeatureLevel(BaseModel):
    level = ForeignKeyField(Level)
    feature = ForeignKeyField(SubFeature)

    class Meta:
        indexes = ((("level", "feature"), True),)


class CClassSubFeature(BaseModel):
    cclass = ForeignKeyField(CClass)
    feature = ForeignKeyField(SubFeature)

    class Meta:
        indexes = ((("cclass", "feature"), True),)


class SubCClassSubFeature(BaseModel):
    subcclass = ForeignKeyField(SubCClass)
    feature = ForeignKeyField(SubFeature)

    class Meta:
        indexes = ((("subcclass", "feature"), True),)


class ToolCategory(BaseModel):
    name = CharField(unique=True)


class Tool(BaseModel):
    name = CharField(unique=True)
    category = ForeignKeyField(ToolCategory)
    cost = IntegerField()
    weight = IntegerField()
    description = TextField()


class ToolProficiency(BaseModel):
    tool = ForeignKeyField(Tool, backref="proficient_characters")
    character = ForeignKeyField(Character, backref="tool_proficiencies")

    class Meta:
        indexes = ((("tool", "character"), True),)


# class CClassWeaponProficiency(BaseModel):
#     cclass = ForeignKeyField(CClass)
#     weapon = ForeignKeyField(Weapon)

#     class Meta:
#         indexes = ((("cclass", "weapon"), True),)


# class CClassArmourProficiency(BaseModel):
#     cclass = ForeignKeyField(CClass)
#     armour = ForeignKeyField(Armour)

#     class Meta:
#         indexes = ((("cclass", "armour"), True),)


class CClassSaveProficiency(BaseModel):
    cclass = ForeignKeyField(CClass, backref="save_proficiencies")
    ability = ForeignKeyField(Ability)

    class Meta:
        indexes = ((("cclass", "ability"), True),)


class CClassToolProficiency(BaseModel):
    cclass = ForeignKeyField(CClass, backref="tool_proficiencies")
    tool = ForeignKeyField(Tool)

    class Meta:
        indexes = ((("cclass", "tool"), True),)
