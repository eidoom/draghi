#!/usr/bin/env python3


def proficiency_bonus(level: int) -> int:
    return 2 + (level - 1) // 4 if level else 0
