#!/usr/bin/env python3

import datetime
import urllib.parse

import flask
import flask_login
import peewee

import query
import logic
from database import db
from models import *

# { Flask

app = flask.Flask(__name__, static_url_path="/static")

with open("secret_key", "r") as f:
    app.secret_key = f.read()


@app.after_request
def add_header(response):
    security = {
        "Strict-Transport-Security": "max-age=31536000; includeSubDomains",
        # "Content-Security-Policy": "default-src 'self'",
        "X-Content-Type-Options": "nosniff",
        "X-Frame-Options": "SAMEORIGIN",
    }
    for k, v in security.items():
        response.headers[k] = v
    return response


CTX = {
    "titles": {
        "campaigns": "Campaigns",
        "players": "Players",
        "characters": "Characters",
        "monsters": "Monsters",
        "monster_types": "Monster Types",
        "cclasses": "Classes",
        "subcclasses": "Subclasses",
        "features": "Features",
        "subfeatures": "Subfeatures",
        "races": "Races",
        "subraces": "Subraces",
        "traits": "Traits",
        "feats": "Feats",
        # "equipment": "Equipment",
        "armours": "Armours",
        "armour_categories": "Armour Categories",
        "weapons": "Weapons",
        "attack_types": "Attack Types",
        "damage_types": "Damage Types",
        "tools": "Tools",
        "tool_categories": "Tool Categories",
        "magic_items": "Magic Items",
        "rarities": "Rarities",
        "spells": "Spells",
        "spell_components": "Spell Components",
        "spell_shapes": "Areas of Effect",
        "spell_levels": "Spell Levels",
        "magic_schools": "Magic Schools",
        "abilities": "Abilities",
        "skills": "Skills",
        "alignments": "Alignments",
        "conditions": "Conditions",
        "sizes": "Sizes",
    }
}


# }{ Peewee


@app.before_request
def _db_connect():
    db.connect()


@app.teardown_request
def _db_close(exc):
    if not db.is_closed():
        db.close()


# }{ Flask-Login

login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"


@login_manager.user_loader
def load_user(player_id):
    return Player.get(Player.id == player_id)


def is_safe_url(target):
    ref_url = urllib.parse.urlparse(flask.request.host_url)
    test_url = urllib.parse.urlparse(
        urllib.parse.urljoin(flask.request.host_url, target)
    )
    return test_url.scheme in ("http", "https") and ref_url.netloc == test_url.netloc


@app.route("/login", methods=["GET", "POST"])
def login():
    ctx = {**CTX}

    if flask.request.method == "POST":
        ctx |= flask.request.form

        player = Player.select().where(Player.name == ctx["username"])
        ctx["user_exists"] = player.exists()

        if not ctx["user_exists"]:
            flask.flash(f"I'm afraid I don't recognise player name {ctx['username']}!")
        else:
            player = player.where(Player.password == ctx["password"])
            ctx["pass_passed"] = player.exists()

            if not ctx["pass_passed"]:
                flask.flash(f"That's not the right password!")
            else:
                player = player[0]
                flask_login.login_user(player)
                flask.flash(f"Welcome back {player.name}!")

                redir = flask.request.args.get("next")
                if not is_safe_url(redir):
                    return flask.abort(400)

                # TODO this should redirect to original page even when user wasn't send to login by Flask-Login
                return flask.redirect(redir or flask.url_for("landing_page"))

    return flask.render_template("login.html.jinja2", **ctx)


def check_password(password):
    return len(password) >= 8


@app.route("/signup", methods=["GET", "POST"])
def signup():
    ctx = {**CTX}
    if flask.request.method == "POST":
        ctx |= flask.request.form

        ctx["user_exists"] = Player.select().where(Player.name == ctx["name"]).exists()
        ctx["pass_passed"] = check_password(ctx["password"])

        if ctx["user_exists"]:
            flask.flash(f"Sorry, player name {ctx['name']} is already taken!")
        else:
            if not ctx["pass_passed"]:
                flask.flash(f"Please choose a password with at least 8 characters!")
            else:
                player = Player.create(**flask.request.form)
                flask_login.login_user(player)
                flask.flash(f"Welcome {player.name}!")

                redir = flask.request.args.get("next")
                if not is_safe_url(redir):
                    return flask.abort(400)

                return flask.redirect(redir or flask.url_for("landing_page"))

    return flask.render_template("signup.html.jinja2", **ctx)


@app.get("/player_area")
@flask_login.login_required
def player_area():
    return flask.render_template("player_area.html.jinja2", **CTX)


@app.get("/logout")
@flask_login.login_required
def logout():
    name = flask_login.current_user.name
    flask_login.logout_user()
    flask.flash(f"Goodbye {name}!")
    return flask.redirect(flask.url_for("landing_page"))


@app.post("/change_password")
@flask_login.login_required
def change_password():
    tmpl = """
{% from "macros.html.jinja2" import change_password %}
    {{ change_password(old_password, old_passed, new_passed) }}
    """

    ctx = {"old_password": flask.request.form["old_password"]}
    ctx["old_passed"] = ctx["old_password"] == flask_login.current_user.password

    if ctx["old_passed"]:
        new_password = flask.request.form["new_password"]
        ctx["new_passed"] = check_password(new_password)
        if ctx["new_passed"]:
            flask_login.current_user.password = new_password
            flask_login.current_user.save()

    return flask.render_template_string(tmpl, **ctx)


@app.put("/set_bio/")
@flask_login.login_required
def set_bio():
    flask_login.current_user.bio = flask.request.form.get("bio").strip()
    flask_login.current_user.save()
    return ""


@app.delete("/delete_account")
@flask_login.login_required
def delete_account():
    player = Player.get(Player.id == flask_login.current_user.id)
    flask_login.logout_user()
    flask.flash(f"gg {player.name}, wp")
    # player.delete_instance(recursive=flask.request.form.get("recursive"))
    player.delete_instance()
    return "", {"HX-Redirect": flask.url_for("landing_page")}


# }{ top level pages


@app.get("/")
def landing_page():
    return flask.render_template("index.html.jinja2", **CTX)


@app.post("/search")
def search():
    query = BaseModel.raw(
        """
        SELECT t0.* FROM (
        SELECT t1.id, t1.name, 'quick_character' AS root FROM character AS t1
        UNION
        SELECT t2.id, t2.name, 'cclass' AS root FROM cclass AS t2
        UNION
        SELECT t3.id, t3.name, 'race' AS root FROM race AS t3
        UNION
        SELECT t4.id, t4.name, 'spell' AS root FROM spell AS t4
        UNION
        SELECT t5.id, t5.name, 'magic_item' AS root FROM magicitem AS t5
        UNION
        SELECT t6.id, t6.name, 'weapon' AS root FROM weapon AS t6
        UNION
        SELECT t7.id, t7.name, 'armour' AS root FROM armour AS t7
        UNION
        SELECT t8.id, t8.name, 'subcclass' AS root FROM subcclass AS t8
        UNION
        SELECT t9.id, t9.name, 'subrace' AS root FROM subrace AS t9
        UNION
        SELECT t10.id, t10.name, 'magic_school' AS root FROM magicschool AS t10
        UNION
        SELECT t11.id, t11.name, 'ability' AS root FROM ability AS t11
        UNION
        SELECT t12.id, t12.name, 'skill' AS root FROM skill AS t12
        UNION
        SELECT t13.id, t13.name, 'trait' AS root FROM trait AS t13
        UNION
        SELECT t14.id, t14.name, 'condition' AS root FROM condition AS t14
        UNION
        SELECT t15.id, t15.name, 'damage_type' AS root FROM damagetype AS t15
        UNION
        SELECT t16.id, t16.name, 'spell_shape' AS root FROM spellshape AS t16
        UNION
        SELECT t17.id, t17.name, 'alignment' AS root FROM alignment AS t17
        UNION
        SELECT t18.id, t18.name, 'spell_component' AS root FROM component AS t18
        UNION
        SELECT t19.id, t19.name, 'armour_category' AS root FROM armourcategory AS t19
        UNION
        SELECT t20.id, t20.name, 'spell_level' AS root FROM spelllevel AS t20
        UNION
        SELECT t21.id, t21.name, 'player' AS root FROM player AS t21
        UNION
        SELECT t22.id, t22.name, 'campaign' AS root FROM campaign AS t22
        UNION
        SELECT t23.id, t23.name, 'feature' AS root FROM feature AS t23
        ) AS t0
        WHERE t0.name LIKE ?
        LIMIT 100
        """,
        f"%{flask.request.form['search']}%",
    ).dicts()
    tmpl = """
    {% for row in data %}
    <li>
      <a href="{{ url_for(row.root, id=row.id) }}">{{ row.name }}</a>
    </li>
    {% endfor %}
    """
    data = [*query]
    if data:
        return flask.render_template_string(tmpl, data=data)
    else:
        return "No results found"


@app.get("/settings")
def settings():
    return flask.render_template("settings.html.jinja2", **CTX)


@app.errorhandler(404)
def not_found(*_):
    return (
        flask.render_template(
            "simple.html.jinja2",
            **CTX,
            title="Not Found",
            content="These aren't the droids you're looking for.",
        ),
        404,
    )


@app.errorhandler(401)
def unauthorised():
    return (
        flask.render_template(
            "simple.html.jinja2",
            **CTX,
            title="Unauthorised",
            content="I'm sorry Dave, I'm afraid I can't do that.",
        ),
        401,
    )


# @app.get("/<x>")
# def settings(x):
#     return not_found()


# }{ Character


@app.get("/characters/")
def characters():
    return flask.render_template(
        "characters.html.jinja2",
        **CTX,
        characters=query.characters(),
    )


@app.get("/characters/view/<int:id>")
def quick_character(id):
    ctx = {**CTX}
    try:
        ctx["character"] = (
            Character.select(
                Character.id,
                Character.name,
                Character.hp,
                Character.max_hp,
                Character.tmp_hp,
            )
            .where(Character.id == id)
            .dicts()
        )[0]
    except IndexError:
        return not_found()
    ctx["abilities"] = query.abilities_and_saves(id)
    ctx["players"] = [
        a[0]
        for a in Player.select(Player.id)
        .join(CharacterPlayer)
        .join(Character)
        .where(Character.id == id)
        .tuples()
    ]
    return flask.render_template("quick_character.html.jinja2", **ctx)


@app.get("/characters/edit/<int:id>")
@flask_login.login_required
def character(id):
    ctx = {**CTX, "players": query.character_players(id)}
    if flask_login.current_user.id not in (p["id"] for p in ctx["players"]):
        return unauthorised()
    try:
        ctx["character"] = (
            Character.select(
                Character.id,
                Character.name,
                Character.hp,
                Character.max_hp,
                Character.tmp_hp,
                Character.xp,
                Character.notes,
                Alignment.id,
                Alignment.name,
                Alignment.society,
                Alignment.morality,
                Race.id,
                Race.name,
            )
            .join(Race)
            .join_from(Character, Alignment)
            .where(Character.id == id)
        )[0]
    except IndexError:
        return not_found()

    try:
        ctx["character"].subrace = (
            SubRace.select(SubRace.name, SubRace.id)
            .join(CharacterSubRace)
            .where(CharacterSubRace.character == id)[0]
        )
    except IndexError:
        pass

    ctx["campaigns"] = (
        Campaign.select()
        .join(CampaignCharacter)
        .where(CampaignCharacter.character == id)
    )
    ctx["moralities"] = Morality.select()
    ctx["societies"] = Society.select()
    ctx["abilities"] = query.abilities_and_saves(id)
    ctx["my_classes"] = query.character_classes(id)
    ctx["all_races"] = query.races()
    ctx["subraces"] = (
        SubRace.select(SubRace.id, SubRace.name)
        .join(Race)
        .where(Race.id == ctx["character"].race.id)
    )
    ctx["available_classes"] = query.available_classes(id)
    ctx["levels"] = query.levels()
    ctx["character"].level = query.character_level(id)
    ctx["character"].proficiency_bonus = logic.proficiency_bonus(ctx["character"].level)
    ctx["skills"] = query.skills(id, ctx["character"].proficiency_bonus)
    ctx["passive"] = query.passives(id, ctx["character"].proficiency_bonus)
    highest_spell_level = (
        SpellSlots.select(peewee.fn.MAX(SpellLevel.level))
        .join(CClass)
        .join(CharacterCClass)
        .join(StartingCClass)
        .join_from(SpellSlots, SpellLevel)
        .where(
            (CharacterCClass.character == id)
            & (SpellSlots.cclass_level <= CharacterCClass.level)
        )
    ).scalar()
    ctx["available_spells"] = (
        Spell.select(Spell.name, Spell.id, SpellLevel.name.alias("level"))
        .join(CClassSpell)
        .join(CClass)
        .join(CharacterCClass)
        .join(StartingCClass)
        .join_from(Spell, SpellLevel)
        .where(
            (CharacterCClass.character == id)
            & (SpellLevel.level <= highest_spell_level)
        )
        .order_by(SpellLevel.level, Spell.name)
        .dicts()
    )
    # for Bard, Ranger, Sorcerer, Warlock
    ctx["number_known_spells"] = (
        SpellsKnown.select(SpellsKnown.number)
        .join(CClass)
        .join(CharacterCClass)
        .join(StartingCClass)
        .where(
            (CharacterCClass.character == id)
            & (SpellsKnown.cclass_level == CharacterCClass.level)
        )
        .scalar()
    )
    ctx["known_cantrips"] = (
        CantripsKnown.select(CantripsKnown.number)
        .join(CClass)
        .join(CharacterCClass)
        .join(StartingCClass)
        .where(
            (CharacterCClass.character == id)
            & (CantripsKnown.cclass_level == CharacterCClass.level)
        )
        .scalar()
    )
    ctx["spell_slots"] = {
        level.name: (
            SpellSlot.select(SpellSlot.id, UsedSlot.id.alias("used"))
            .join(CharacterCClass)
            .join(StartingCClass)
            .join_from(SpellSlot, SpellLevel)
            .join(UsedSlot, join_type=peewee.JOIN.LEFT_OUTER, src=SpellSlot)
            .where((CharacterCClass.character == id) & (SpellLevel.id == level.id))
            .dicts()
        )
        for level in SpellLevel.select()
    }
    ctx["spellcasting"] = (
        SpellSlots.select()
        .join(CClass)
        .join(CharacterCClass)
        .where(CharacterCClass.character == id)
        .exists()
    )
    try:
        ctx["spellcasting_ability"] = (
            SpellcastingAbility.select(Ability.name, Ability.id)
            .join(Ability)
            .join_from(SpellcastingAbility, CClass)
            .join(CharacterCClass)
            .join(StartingCClass)
            .where(CharacterCClass.character == id)
            .dicts()
        )[0]
    except IndexError:
        pass
    ctx["known_spells"] = ctx["available_spells"].join_from(Spell, KnownSpell)
    if ctx["spellcasting"] and not ctx["number_known_spells"]:
        mod = (
            Ability.select(query.ABILITY_MOD)
            .join(SpellcastingAbility)
            .join(CClass)
            .join(CharacterCClass)
            .join(StartingCClass)
            .join_from(Ability, CharacterAbility)
            .join(Character)
            .where(Character.id == id)
        ).scalar()
        level = (
            CharacterCClass.select(CharacterCClass.level)
            .join(StartingCClass)
            .where(CharacterCClass.character == id)
        ).scalar()
        ctx["max_prepared_spells"] = mod + level
    ctx["prepared_spells"] = ctx["known_spells"].join_from(KnownSpell, PreparedSpell)
    ctx["inventory_weight"] = query.inventory_weight(id)
    ctx["conditions"] = Condition.select(Condition.name, Condition.id).dicts()
    ctx["my_conditions"] = query.my_conditions(id)
    return flask.render_template("character.html.jinja2", **ctx)


@app.get("/list_characters")
def list_characters():
    tmpl = """
{% from "macros.html.jinja2" import list_characters %}
    {{ list_characters(characters) }}
    """
    return flask.render_template_string(tmpl, characters=query.characters())


@app.get("/section_skills/<int:id>")
def section_skills(id):
    level = query.character_level(id)
    bonus = logic.proficiency_bonus(level)
    ctx = {"skills": query.skills(id, bonus), "id": id}

    tmpl = """
    {% from "character_macros.html.jinja2" import section_skills %}
    {{ section_skills(skills, id) }}
    """

    return flask.render_template_string(tmpl, **ctx)


@app.get("/passive_table/<int:id>")
def passive_table(id):
    level = query.character_level(id)
    bonus = logic.proficiency_bonus(level)
    passives = query.passives(id, bonus)
    tmpl = """
{% from "macros.html.jinja2" import passive_table %}
    {{ passive_table(passive) }}
    """
    return flask.render_template_string(tmpl, passive=passives)


@app.get("/condition_list/<int:id>")
def condition_list(id):
    my_conditions = query.my_conditions(id)
    tmpl = """
{% from "macros.html.jinja2" import condition_list %}
    {{ condition_list(my_conditions) }}
    """
    return flask.render_template_string(tmpl, my_conditions=my_conditions)


@app.get("/character_name/<int:id>")
def character_name(id):
    try:
        return Character.select(Character.name).where(Character.id == id).scalar()
    except IndexError:
        return "", 400


@app.get("/section_saves/<int:id>")
@flask_login.login_required
def section_saves(id):
    ctx = {"abilities": query.abilities_and_saves(id), "id": id}

    tmpl = """
    {% from "character_macros.html.jinja2" import section_saves %}
    {{ section_saves(abilities, id) }}
    """

    return flask.render_template_string(tmpl, **ctx)


@app.route("/characters/add/", methods=["GET", "POST"])
@flask_login.login_required
def add_character():
    if flask.request.method == "POST":
        try:
            abilities = Ability.select(Ability.id).dicts()
            alignment = query.alignment_from_matrix(**flask.request.form)
            with db.atomic():
                character = Character.create(
                    **flask.request.form,
                    alignment=alignment,
                )

                CharacterPlayer.insert(
                    player=flask_login.current_user, character=character
                ).execute()

                CharacterAbility.insert_many(
                    ((character, ability["id"]) for ability in abilities),
                    fields=(CharacterAbility.character, CharacterAbility.ability),
                ).execute()

                cclass = CharacterCClass.create(
                    character=character, cclass=flask.request.form["cclass"]
                )

                StartingCClass.insert(character_cclass=cclass).execute()

                return flask.redirect(flask.url_for("character", id=character.id))

        except peewee.IntegrityError:
            flask.flash(f"There's something not right with that character :/")

    return flask.render_template(
        "add_character.html.jinja2",
        **CTX,
        races=query.races(),
        cclasses=query.cclasses(),
        moralities=Morality.select(),
        societies=Society.select(),
    )


@app.put("/set_abilities/<int:id>")
@flask_login.login_required
def set_abilities(id):
    new = []

    for caid, score in flask.request.form.items():
        ability = CharacterAbility.get(CharacterAbility.id == caid)
        ability.score = score
        new.append(ability)

    with db.atomic():
        CharacterAbility.bulk_update(new, fields=(CharacterAbility.score,))

    ctx = {"abilities": query.character_abilities(id), "id": id}

    tmpl = """
    {% from "character_macros.html.jinja2" import section_abilities %}
    {{ section_abilities(abilities, id) }}
    """

    return flask.render_template_string(tmpl, **ctx), {"HX-Trigger": "updateAbilities"}


@app.put("/set_save_proficiencies/<int:id>")
@flask_login.login_required
def set_save_proficiencies(id):
    caids = [*flask.request.form.keys()]
    to_delete_fk = CharacterAbility.select().where(
        (CharacterAbility.character == id) & ~(CharacterAbility.id << caids)
    )

    with db.atomic():
        SaveProficiency.delete().where(
            SaveProficiency.character_ability << to_delete_fk
        ).execute()

        SaveProficiency.insert_many(
            {"character_ability": caid} for caid in caids
        ).on_conflict_ignore().execute()

    return "", {"HX-Trigger": "updateSaves"}


@app.put("/set_skill_proficiencies/<int:id>")
@flask_login.login_required
def set_skill_proficiencies(id):
    skids = [*flask.request.form.keys()]

    with db.atomic():
        SkillProficiency.delete().where(
            (SkillProficiency.character == id) & ~(SkillProficiency.skill << skids)
        ).execute()

        SkillProficiency.insert_many(
            {"character": id, "skill": skid} for skid in skids
        ).on_conflict_ignore().execute()

    return "", {"HX-Trigger": "updateSkills"}


@app.put("/set_skill_expertise/<int:id>")
@flask_login.login_required
def set_skill_expertise(id):
    spids = [*flask.request.form.keys()]

    with db.atomic():
        SkillExpertise.delete().where(~(SkillExpertise.proficiency << spids)).execute()

        SkillExpertise.insert_many(
            {"proficiency": spid} for spid in spids
        ).on_conflict_ignore().execute()

    return "", {"HX-Trigger": "updateSkills"}


@app.put("/character_conditions/<int:id>")
@flask_login.login_required
def character_conditions(id):
    # TODO improve query?
    coids = [*flask.request.form.keys()]
    with db.atomic():
        CharacterCondition.delete().where(
            (CharacterCondition.character == id) & ~(CharacterCondition.id << coids)
        ).execute()
        for coid in coids:
            CharacterCondition.get_or_create(character=id, condition=coid)
    return "", {"HX-Trigger": "updateConditions"}


@app.put("/set_notes/<int:id>")
@flask_login.login_required
def set_notes(id):
    try:
        c = Character.get(Character.id == id)
    except peewee.DoesNotExist:
        return "", 400
    c.notes = flask.request.form.get("notes").strip()
    c.save()
    return ""


@app.put("/set_character_name")
@flask_login.login_required
def set_character_name():
    return query.set_single_on_character(
        "name", "updateCharacterName", flask.request.form
    )


@app.put("/set_character_alignment/<int:id>")
@flask_login.login_required
def set_character_alignment(id):
    alignment = query.alignment_from_matrix(**flask.request.form)

    n = Character.update({"alignment": alignment}).where(Character.id == id).execute()

    if not n:
        return "", 400

    ctx = {}
    try:
        ctx["character"] = (
            Character.select(
                Character.id,
                Alignment.name,
                Alignment.id,
                Alignment.society,
                Alignment.morality,
            )
            .join(Alignment)
            .where(Character.id == id)
        )[0]
    except IndexError:
        return "", 400

    ctx["moralities"] = Morality.select()
    ctx["societies"] = Society.select()

    tmpl = """
    {% from "character_macros.html.jinja2" import section_alignment %}
    {{ section_alignment(character, moralities, societies) }}
    """

    return flask.render_template_string(tmpl, **ctx)


@app.put("/set_xp/<int:id>")
@flask_login.login_required
def set_xp(id):
    n = Character.update(flask.request.form).where(Character.id == id).execute()

    if not n:
        return "", 400

    ctx = {}
    try:
        ctx["character"] = (
            Character.select(
                Character.id,
                Character.xp,
            ).where(Character.id == id)
        )[0]
    except IndexError:
        return "", 400

    ctx["character"].level = query.character_level(id)
    ctx["character"].proficiency_bonus = logic.proficiency_bonus(ctx["character"].level)

    ctx["levels"] = query.levels()

    tmpl = """
    {% from "character_macros.html.jinja2" import section_xp %}
    {{ section_xp(character, levels) }}
    """

    return flask.render_template_string(tmpl, **ctx), {"HX-Trigger": "updateXP"}


@app.put("/set_hp/<int:id>")
@flask_login.login_required
def set_hp(id):
    n = Character.update(flask.request.form).where(Character.id == id).execute()

    if not n:
        return "", 400

    ctx = {}
    try:
        ctx["character"] = (
            Character.select(
                Character.id,
                Character.hp,
                Character.max_hp,
                Character.tmp_hp,
            )
            .where(Character.id == id)
            .dicts()
        )[0]
    except IndexError:
        return "", 400

    tmpl = """
    {% from "character_macros.html.jinja2" import section_hp %}
    {{ section_hp(character) }}
    """

    return flask.render_template_string(tmpl, **ctx)


@app.put("/set_character_race/<int:id>")
@flask_login.login_required
def set_character_race(id):
    Character.update(race=flask.request.form["race"]).where(
        Character.id == id
    ).execute()

    CharacterSubRace.delete().where(CharacterSubRace.character == id).execute()

    try:
        CharacterSubRace.insert(
            character=id, subrace=flask.request.form["subrace"]
        ).execute()
    except KeyError:
        pass

    ctx = {}
    try:
        ctx["character"] = (
            Character.select(
                Character.id,
                Race.id,
                Race.name,
            )
            .join(Race)
            .where(Character.id == id)
        )[0]
    except IndexError:
        return "", 400

    try:
        ctx["character"].subrace = (
            SubRace.select(SubRace.name, SubRace.id)
            .join(CharacterSubRace)
            .where(CharacterSubRace.character == id)[0]
        )
    except IndexError:
        pass

    ctx["all_races"] = query.races()
    ctx["subraces"] = (
        SubRace.select(SubRace.id, SubRace.name)
        .join(Race)
        .where(Race.id == ctx["character"].race.id)
    )

    tmpl = """
    {% from "character_macros.html.jinja2" import section_race %}
    {{ section_race(character, all_races, subraces) }}
    """

    return flask.render_template_string(tmpl, **ctx)


@app.delete("/delete_character/<int:id>")
@flask_login.login_required
def delete_character(id):
    Character.get(Character.id == id).delete_instance(recursive=True)
    return "", {"HX-Redirect": flask.url_for("characters")}


# }{ Race


@app.get("/races/")
def races():
    return flask.render_template(
        "items.html.jinja2", **CTX, item="race", items=query.races()
    )


@app.get("/races/<int:id>")
def race(id):
    ctx = {**CTX}
    try:
        ctx["race"] = Race.select().join(Size).where(Race.id == id)[0]
    except IndexError:
        return not_found()

    ctx["subraces"] = (
        SubRace.select(SubRace.id, SubRace.name).join(Race).where(Race.id == id)
    )

    ctx["traits"] = (
        RaceTrait.select(Trait.id, Trait.name, Trait.description)
        .join(Race)
        .join_from(RaceTrait, Trait)
        .where(Race.id == id)
        .dicts()
    )

    ctx |= query.prev_next(Race, id)
    ctx["crumb"] = "Races"

    return flask.render_template("race.html.jinja2", **ctx)


# }{ SubRace


@app.get("/subraces/")
def subraces():
    subraces = SubRace.select(SubRace.id, SubRace.name).order_by(SubRace.name)
    return flask.render_template(
        "items.html.jinja2", **CTX, item="subrace", items=subraces
    )


@app.get("/subraces/<int:id>")
def subrace(id):
    ctx = {**CTX}
    try:
        ctx["subrace"] = SubRace.select().join(Race).where(SubRace.id == id)[0]
    except IndexError:
        return not_found()

    ctx["traits"] = (
        SubRaceTrait.select(Trait.id, Trait.name, Trait.description)
        .join(SubRace)
        .join_from(SubRaceTrait, Trait)
        .where(SubRace.id == id)
        .dicts()
    )

    ctx |= query.prev_next(SubRace, id)
    ctx["crumb"] = "Subraces"

    return flask.render_template("subrace.html.jinja2", **ctx)


# }{ Trait


@app.get("/traits/")
def traits():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="trait",
        items=Trait.select(Trait.name, Trait.id).order_by(Trait.name).dicts(),
    )


@app.get("/traits/<int:id>")
def trait(id):
    ctx = {**CTX}
    try:
        ctx["trait"] = Trait.select().where(Trait.id == id)[0]
    except IndexError:
        return not_found()

    ctx |= query.prev_next(Trait, id)
    ctx["crumb"] = "Traits"

    return flask.render_template("trait.html.jinja2", **ctx)


# }{ CClass


@app.get("/classes/")
def cclasses():
    return flask.render_template(
        "items.html.jinja2", **CTX, item="cclass", items=query.cclasses()
    )


@app.get("/classes/<int:id>")
def cclass(id):
    ctx = {**CTX}
    try:
        ctx["cclass"] = (
            CClass.select(CClass.name, CClass.hit_die, CClass.subclass_flavour)
            .where(CClass.id == id)
            .dicts()
        )[0]
    except IndexError:
        return not_found()

    ctx["features"] = (
        CClassFeature.select(
            Feature.name,
            Feature.description,
            Level.level,
        )
        .join(Feature)
        .join(FeatureLevel)
        .join(Level)
        .where(CClassFeature.cclass == id)
        .order_by(Level.level, Feature.name)
        .dicts()
    )

    ctx["subfeatures"] = (
        CClassSubFeature.select(
            SubFeature.name,
            SubFeature.description,
        )
        .join(SubFeature)
        .join(SubFeatureParent)
        .join(Feature)
        .join(FeatureLevel)
        .join(Level)
        .where(CClassSubFeature.cclass == id)
        .order_by(Level.level, Feature.name)
        .dicts()
    )

    try:
        ctx["spellcasting_ability"] = (
            SpellcastingAbility.select(Ability.name, Ability.id)
            .join(Ability)
            .join_from(SpellcastingAbility, CClass)
            .where(CClass.id == id)
            .dicts()
        )[0]

        ctx["spell_levels"] = (
            SpellLevel.select(SpellLevel)
            .join(SpellSlots)
            .where((SpellLevel.level > 0) & (SpellSlots.cclass == id))
            .group_by(SpellLevel.level)
            .dicts()
        )

        ctx["spell_slots"] = {
            level.level: SpellSlots.select(SpellSlots.spell_level, SpellSlots.number)
            .join(SpellLevel)
            .where((SpellSlots.cclass == id) & (SpellSlots.cclass_level == level.level))
            .order_by(SpellLevel.level)
            .dicts()
            for level in Level.select()
        }

        ctx["known_spells"] = (
            SpellsKnown.select(SpellsKnown.cclass_level, SpellsKnown.number)
            .where(SpellsKnown.cclass == id)
            .order_by(SpellsKnown.cclass_level)
        )

        ctx["known_cantrips"] = (
            CantripsKnown.select(CantripsKnown.cclass_level, CantripsKnown.number)
            .where(CantripsKnown.cclass == id)
            .order_by(CantripsKnown.cclass_level)
        )

    except IndexError:
        pass

    raw = (
        Spell.select(Spell.name, Spell.id)
        .join(CClassSpell)
        .join(CClass)
        .where(CClass.id == id)
        .order_by(Spell.name)
        .dicts()
    )
    ctx["spells"] = query.levelled_spells(raw)

    ctx["subcclasses"] = (
        SubCClass.select(SubCClass.name, SubCClass.id)
        .where(SubCClass.cclass == id)
        .dicts()
    )

    ctx["proficiencies"] = {
        "saves": Ability.select(Ability.id, Ability.name)
        .join(CClassSaveProficiency)
        .where(CClassSaveProficiency.cclass == id)
        .dicts(),
        "tools": Tool.select(Tool.id, Tool.name)
        .join(CClassToolProficiency)
        .where(CClassToolProficiency.cclass == id)
        .dicts(),
    }

    ctx |= query.prev_next(CClass, id)
    ctx["crumb"] = "Classes"

    return flask.render_template("cclass.html.jinja2", **ctx)


# }{ CharacterCClass
# NB it would be much simpler to update the whole Classes <section> on HX-Trigger: updateClass
# but I've done triggered events on the small units of the DOM instead, mainly to play with HTMX
# see https://htmx.org/examples/update-other-content/


@app.get("/character_classes/<int:id>")
def character_classes(id):
    my_classes = query.character_classes(id)
    tmpl = """
    {% from "macros.html.jinja2" import list_character_classes %}
    {{ list_character_classes(my_classes) }}
    """
    return flask.render_template_string(tmpl, my_classes=my_classes)


@app.get("/delete_character_class_list/<int:id>")
def delete_character_class_list(id):
    my_classes = query.character_classes(id)
    tmpl = """
    {% from "macros.html.jinja2" import delete_class %}
    {{ delete_class(my_classes) }}
    """
    return flask.render_template_string(tmpl, my_classes=my_classes)


@app.get("/add_character_class_list/<int:id>")
def add_character_class_list(id):
    tmpl = """
    {% from "macros.html.jinja2" import choose_class %}
    {{ choose_class(available_classes) }}
    """
    available_classes = query.available_classes(id)
    return flask.render_template_string(tmpl, available_classes=available_classes)


@app.put("/add_character_class")
def add_character_class():
    try:
        starting = (
            not CharacterCClass.select()
            .where(CharacterCClass.character == flask.request.form["character"])
            .exists()
        )
        with db.atomic():
            cclass = CharacterCClass.create(**flask.request.form)
            if starting:
                StartingCClass.insert(character_cclass=cclass).execute()
    except peewee.IntegrityError:
        return "", 400
    return "", {"HX-Trigger": "updateClass"}


@app.put("/edit_character_class")
def edit_character_class():
    try:
        cclass = CharacterCClass.get(CharacterCClass.id == flask.request.form["ccid"])
        cclass.level = flask.request.form["level"]
        cclass.save()
    except peewee.DoesNotExist:
        return "", 400
    return "", {"HX-Trigger": "updateClass"}


@app.delete("/delete_character_class")
def delete_character_class():
    cclass = CharacterCClass.get(CharacterCClass.id == flask.request.form["ccid"])
    cclass.delete_instance(recursive=True)
    return "", {"HX-Trigger": "updateClass"}


# }{ SubCClass


@app.get("/subclasses/")
def subcclasses():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="subcclass",
        items=SubCClass.select().order_by(SubCClass.name).dicts(),
    )


@app.get("/subclasses/<int:id>")
def subcclass(id):
    ctx = {**CTX}
    try:
        ctx["subcclass"] = (
            SubCClass.select(
                SubCClass.name,
                SubCClass.description,
                CClass.name,
                CClass.id,
                CClass.subclass_flavour,
            )
            .join(CClass)
            .where(SubCClass.id == id)
        )[0]
    except IndexError:
        return not_found()

    ctx["features"] = (
        SubCClassFeature.select(
            Feature.name,
            Feature.description,
            Level.level,
        )
        .join(Feature)
        .join(FeatureLevel)
        .join(Level)
        .where(SubCClassFeature.subcclass == id)
        .order_by(Level.level)
        .dicts()
    )

    ctx["subfeatures"] = (
        SubCClassSubFeature.select(
            SubFeature.name,
            SubFeature.description,
        )
        .join(SubFeature)
        .join(SubFeatureParent)
        .join(Feature)
        .join(FeatureLevel)
        .join(Level)
        .where(SubCClassSubFeature.subcclass == id)
        .order_by(Level.level, Feature.name)
        .dicts()
    )

    ctx |= query.prev_next(SubCClass, id)
    ctx["crumb"] = "Subclasses"

    return flask.render_template("subcclass.html.jinja2", **ctx)


# }{ Equipment


# @app.get("/equipment/")
# def equipment():
#     equipment = []
#     return flask.render_template("equipment.html.jinja2", equipment=equipment)


# }{ Armour


@app.get("/armours/")
def armours():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="armour",
        items=Armour.select(Armour.id, Armour.name).order_by(Armour.name).dicts(),
    )


@app.get("/armours/<int:id>")
def armour(id):
    ctx = {**CTX}
    try:
        ctx["armour"] = (
            Armour.select(
                Armour.name,
                Armour.description,
                Armour.ac,
                Armour.cost,
                Armour.weight,
                Rarity.name,
                Rarity.id,
                ArmourCategory.name,
                ArmourCategory.id,
                peewee.Case(None, ((ArmourStealthDisadvantage.id, True),), False).alias(
                    "stealth_disadvantage"
                ),
            )
            .join(ArmourCategory)
            .join_from(Armour, Rarity)
            .switch(Armour)
            .left_outer_join(ArmourStealthDisadvantage)
            .where(Armour.id == id)[0]
        )
    except IndexError:
        return not_found()

    ctx |= query.prev_next(Armour, id)
    ctx["crumb"] = "Armours"

    return flask.render_template("armour.html.jinja2", **ctx)


# }{ ArmourCategory


@app.get("/armour_categories/")
def armour_categories():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="armour_category",
        items=ArmourCategory.select(ArmourCategory.id, ArmourCategory.name)
        .order_by(ArmourCategory.degree)
        .dicts(),
    )


@app.get("/armour_categories/<int:id>")
def armour_category(id):
    ctx = {**CTX}
    try:
        ctx["category"] = ArmourCategory.select(
            ArmourCategory.name,
            ArmourCategory.description,
        ).where(ArmourCategory.id == id)[0]
    except IndexError:
        return not_found()

    ctx["armours"] = (
        Armour.select(Armour.name, Armour.id)
        .where(Armour.category == id)
        .order_by(Armour.name)
    )

    ctx |= query.prev_next(ArmourCategory, id)
    ctx["crumb"] = "Armour Categories"

    return flask.render_template("armour_category.html.jinja2", **ctx)


# }{ Weapon


@app.get("/weapons/")
def weapons():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="weapon",
        items=Weapon.select(Weapon.id, Weapon.name).order_by(Weapon.name).dicts(),
    )


@app.get("/weapons/<int:id>")
def weapon(id):
    ctx = {**CTX}
    try:
        ctx["weapon"] = (
            Weapon.select(
                Weapon.name,
                Weapon.damage,
                Rarity.name,
                Rarity.id,
                Weapon.level,
                # TODO format coins
                Weapon.cost,
                Weapon.weight,
                AttackType.name,
                AttackType.id,
                DamageType.name,
                DamageType.id,
            )
            .join(Rarity)
            .join_from(Weapon, AttackType)
            .join_from(Weapon, DamageType)
            .where(Weapon.id == id)[0]
        )
    except IndexError:
        return not_found()

    ctx["properties"] = (
        WeaponProperty.select(
            WeaponPropertyDescription.name,
            WeaponPropertyDescription.description,
            peewee.Case(
                None, ((WeaponPropertyDamage.id, WeaponPropertyDamage.damage),), False
            ).alias("damage"),
            peewee.Case(
                None, ((WeaponPropertyRange.id, WeaponPropertyRange.min),), False
            ).alias("min"),
            peewee.Case(
                None, ((WeaponPropertyRange.id, WeaponPropertyRange.max),), False
            ).alias("max"),
        )
        .join(WeaponPropertyDescription)
        .join(
            WeaponPropertyDamage, src=WeaponProperty, join_type=peewee.JOIN.LEFT_OUTER
        )
        .join(WeaponPropertyRange, src=WeaponProperty, join_type=peewee.JOIN.LEFT_OUTER)
        .where(WeaponProperty.weapon == id)
    ).dicts()

    ctx["special"] = (
        WeaponPropertySpecial.select(WeaponPropertySpecial.description)
        .join(WeaponProperty)
        .join(Weapon)
        .where(Weapon.id == id)
        .scalar()
    )

    ctx |= query.prev_next(Weapon, id)
    ctx["crumb"] = "Weapons"

    return flask.render_template("weapon.html.jinja2", **ctx)


# }{ Spell


@app.get("/spells/")
def spells():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="spell",
        items=Spell.select(Spell.id, Spell.name).order_by(Spell.name).dicts(),
    )


@app.get("/spells/<int:id>")
def spell(id):
    # TODO improve querying
    ctx = {**CTX}
    try:
        ctx["spell"] = (
            Spell.select(
                Spell.name,
                Spell.description,
                Spell.casting_time,
                Spell.range,
                Spell.duration,
                SpellLevel.name,
                SpellLevel.id,
                MagicSchool.name,
                MagicSchool.id,
                peewee.Case(None, ((SpellConcentration.id, True),), False).alias(
                    "concentration"
                ),
                peewee.Case(None, ((SpellRitual.id, True),), False).alias("ritual"),
                peewee.Case(None, ((SpellSave.id, Ability.name),), None).alias("save"),
                peewee.Case(None, ((SpellSave.id, Ability.id),), None).alias("save_id"),
                peewee.Case(None, ((SpellAttackType.id, AttackType.name),), None).alias(
                    "attack_type"
                ),
                peewee.Case(None, ((SpellAttackType.id, AttackType.id),), None).alias(
                    "attack_type_id"
                ),
                peewee.Case(None, ((SpellDamageType.id, DamageType.name),), None).alias(
                    "damage_type"
                ),
                peewee.Case(None, ((SpellDamageType.id, DamageType.id),), None).alias(
                    "damage_type_id"
                ),
                peewee.Case(None, ((SpellAoE.id, SpellAoE.size),), None).alias("area"),
                peewee.Case(None, ((SpellAoE.id, SpellShape.name),), None).alias(
                    "shape"
                ),
                peewee.Case(None, ((SpellAoE.id, SpellShape.id),), None).alias(
                    "shape_id"
                ),
            )
            .join(MagicSchool)
            .join_from(Spell, SpellLevel)
            .join(SpellConcentration, src=Spell, join_type=peewee.JOIN.LEFT_OUTER)
            .join(SpellRitual, src=Spell, join_type=peewee.JOIN.LEFT_OUTER)
            .join(SpellSave, src=Spell, join_type=peewee.JOIN.LEFT_OUTER)
            .left_outer_join(Ability)
            .join(SpellAttackType, src=Spell, join_type=peewee.JOIN.LEFT_OUTER)
            .left_outer_join(AttackType)
            .join(SpellDamageType, src=Spell, join_type=peewee.JOIN.LEFT_OUTER)
            .left_outer_join(DamageType)
            .join(SpellAoE, src=Spell, join_type=peewee.JOIN.LEFT_OUTER)
            .left_outer_join(SpellShape)
            .where(Spell.id == id)[0]
        )
    except IndexError:
        return not_found()

    ctx["components"] = (
        SpellComponent.select(Component.short, Component.id)
        .join(Component)
        .where(SpellComponent.spell == id)
    ).dicts()
    ctx["material"] = (
        SpellMaterial.select(SpellMaterial.material)
        .join(SpellComponent)
        .where(SpellComponent.spell == id)
    )
    ctx["cclasses"] = (
        CClassSpell.select(CClass.name, CClass.id)
        .join(CClass)
        .where(CClassSpell.spell == id)
    ).dicts()

    ctx |= query.prev_next(Spell, id)
    ctx["crumb"] = "Spells"

    return flask.render_template("spell.html.jinja2", **ctx)


# }{ MagicSchool


@app.get("/magic_schools/")
def magic_schools():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="magic_school",
        items=MagicSchool.select(MagicSchool.id, MagicSchool.name)
        .order_by(MagicSchool.name)
        .dicts(),
    )


@app.get("/magic_schools/<int:id>")
def magic_school(id):
    ctx = {**CTX}
    try:
        ctx["school"] = MagicSchool.select(
            MagicSchool.name, MagicSchool.description
        ).where(MagicSchool.id == id)[0]
    except IndexError:
        return not_found()

    raw = (
        Spell.select(Spell.name, Spell.id)
        .where(Spell.school == id)
        .order_by(Spell.name)
        .dicts()
    )
    ctx["spells"] = query.levelled_spells(raw)

    ctx |= query.prev_next(MagicSchool, id)
    ctx["crumb"] = "Magic Schools"

    return flask.render_template("school.html.jinja2", **ctx)


# }{ SpellLevel


@app.get("/spell_levels/")
def spell_levels():
    return flask.render_template(
        "spell_levels.html.jinja2",
        **CTX,
        spell_levels=(
            SpellLevel.select(SpellLevel.id, SpellLevel.name, SpellLevel.level)
            .order_by(SpellLevel.level)
            .dicts()
        ),
    )


@app.get("/spell_levels/<int:id>")
def spell_level(id):
    ctx = {**CTX}
    try:
        ctx["spell_level"] = (
            SpellLevel.select(SpellLevel.name, SpellLevel.level)
            .where(SpellLevel.id == id)
            .dicts()
        )[0]
    except IndexError:
        return not_found()

    ctx["spells"] = Spell.select(Spell.name, Spell.id).where(Spell.level == id).dicts()

    ctx |= query.prev_next(SpellLevel, id)
    ctx["crumb"] = "Spell Levels"

    return flask.render_template("spell_level.html.jinja2", **ctx)


# }{ Component


@app.get("/spell_components/")
def spell_components():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="spell_component",
        items=Component.select(Component.id, Component.name)
        .order_by(Component.name)
        .dicts(),
    )


@app.get("/spell_components/<int:id>")
def spell_component(id):
    ctx = {**CTX}
    try:
        ctx["component"] = Component.select(
            Component.name,
            Component.description,
        ).where(Component.id == id)[0]
    except IndexError:
        return not_found()

    raw = (
        SpellComponent.select(Spell.name, Spell.id)
        .join(Spell)
        .where(SpellComponent.component == id)
        .order_by(Spell.name)
        .dicts()
    )
    ctx["spells"] = query.levelled_spells(raw)

    ctx |= query.prev_next(Component, id)
    ctx["crumb"] = "Spell Components"

    return flask.render_template("component.html.jinja2", **ctx)


# }{ MagicItem


@app.get("/magic_items/")
def magic_items():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="magic_item",
        items=MagicItem.select(MagicItem.id, MagicItem.name)
        .order_by(MagicItem.name)
        .dicts(),
    )


@app.get("/magic_items/<int:id>")
def magic_item(id):
    ctx = {**CTX}
    try:
        ctx["magic_item"] = (
            MagicItem.select(
                MagicItem.name,
                MagicItem.category,
                MagicItem.description,
                Rarity.name,
                Rarity.id,
                peewee.Case(None, ((Attunement.id, True),), False).alias("attunement"),
            )
            .join(Rarity)
            .join(Attunement, src=MagicItem, join_type=peewee.JOIN.LEFT_OUTER)
            .where(MagicItem.id == id)[0]
        )
    except IndexError:
        return not_found()

    ctx |= query.prev_next(MagicItem, id)
    ctx["crumb"] = "Magic Items"

    return flask.render_template("magic_item.html.jinja2", **ctx)


# }{ Ability


@app.get("/abilities/")
def abilities():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="ability",
        items=Ability.select(Ability.name, Ability.id).order_by(Ability.id).dicts(),
    )


@app.get("/abilities/<int:id>")
def ability(id):
    ctx = {**CTX}
    try:
        ctx["ability"] = (
            Ability.select(
                Ability.name, Ability.measure, Ability.description, Ability.short
            )
            .where(Ability.id == id)
            .dicts()[0]
        )
    except IndexError:
        return not_found()

    ctx["skills"] = (
        Skill.select(Skill.name, Skill.id).where(Skill.ability == id).dicts()
    )

    ctx |= query.prev_next(Ability, id)
    ctx["crumb"] = "Abilities"

    return flask.render_template("ability.html.jinja2", **ctx)


# }{ Skill


@app.get("/skills/")
def skills():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="skill",
        items=Skill.select(Skill.name, Skill.id).order_by(Skill.name).dicts(),
    )


@app.get("/skills/<int:id>")
def skill(id):
    ctx = {**CTX}
    try:
        ctx["skill"] = (
            Skill.select(Skill.name, Ability.name, Ability.id, Skill.description)
            .join(Ability)
            .where(Skill.id == id)[0]
        )
    except IndexError:
        return not_found()

    ctx |= query.prev_next(Skill, id)
    ctx["crumb"] = "Skills"

    return flask.render_template("skill.html.jinja2", **ctx)


# }{ Condition


@app.get("/conditions/")
def conditions():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="condition",
        items=(
            Condition.select(Condition.name, Condition.id)
            .order_by(Condition.name)
            .dicts()
        ),
    )


@app.get("/conditions/<int:id>")
def condition(id):
    ctx = {**CTX}
    try:
        ctx["condition"] = Condition.select(
            Condition.name, Condition.description
        ).where(Condition.id == id)[0]
    except IndexError:
        return not_found()

    ctx |= query.prev_next(Condition, id)
    ctx["crumb"] = "Conditions"

    return flask.render_template("condition.html.jinja2", **ctx)


# }{ DamageType


@app.get("/damage_types/")
def damage_types():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="damage_type",
        items=(
            DamageType.select(DamageType.name, DamageType.id)
            .order_by(DamageType.name)
            .dicts()
        ),
    )


@app.get("/damage_types/<int:id>")
def damage_type(id):
    ctx = {**CTX}
    try:
        ctx["damage_type"] = DamageType.select(
            DamageType.name, DamageType.description
        ).where(DamageType.id == id)[0]
    except IndexError:
        return not_found()

    ctx |= query.prev_next(DamageType, id)
    ctx["crumb"] = "Damage Types"

    return flask.render_template("damage_type.html.jinja2", **ctx)


# }{ SpellShape


@app.get("/spell_shapes/")
def spell_shapes():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="spell_shape",
        items=(
            SpellShape.select(SpellShape.name, SpellShape.id)
            .order_by(SpellShape.name)
            .dicts()
        ),
    )


@app.get("/spell_shapes/<int:id>")
def spell_shape(id):
    ctx = {**CTX}
    try:
        ctx["spell_shape"] = SpellShape.select(
            SpellShape.name, SpellShape.description
        ).where(SpellShape.id == id)[0]
    except IndexError:
        return not_found()

    raw = (
        SpellAoE.select(Spell.name, Spell.id)
        .join(Spell)
        .join_from(SpellAoE, SpellShape)
        .where(SpellShape.id == id)
        .order_by(Spell.name)
        .dicts()
    )
    ctx["spells"] = query.levelled_spells(raw)

    ctx |= query.prev_next(SpellShape, id)
    ctx["crumb"] = "Areas of Effect"

    return flask.render_template("spell_shape.html.jinja2", **ctx)


# }{ Alignment


@app.get("/alignments/")
def alignments():
    return flask.render_template(
        "alignments.html.jinja2",
        **CTX,
        alignments=[
            Alignment.select(Alignment.name, Alignment.id, Morality.id, Society.id)
            .join(Morality)
            .join_from(Alignment, Society)
            .where(Morality.id == m.id)
            .order_by(Society.id)
            for m in Morality.select(Morality.id)
        ],
    )


@app.get("/alignments/<int:id>")
def alignment(id):
    ctx = {**CTX}
    try:
        ctx["alignment"] = (
            Alignment.select(
                Alignment.name,
                Alignment.description,
                Alignment.short,
                Morality.name,
                Society.name,
            )
            .join(Morality)
            .join_from(Alignment, Society)
            .where(Alignment.id == id)[0]
        )
    except IndexError:
        return not_found()

    ctx |= query.prev_next(Alignment, id)
    ctx["crumb"] = "Alignments"

    return flask.render_template("alignment.html.jinja2", **ctx)


# }{ Player


@app.get("/players/")
def players():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="player",
        items=Player.select(Player.name, Player.id).order_by(Player.name).dicts(),
    )


@app.get("/players/<int:id>")
def player(id):
    ctx = {**CTX}

    try:
        ctx["player"] = Player.select(
            Player.name, Player.join_datetime, Player.bio
        ).where(Player.id == id)[0]
    except IndexError:
        return not_found()

    ctx["member_time"] = int(
        (
            datetime.datetime.now(datetime.timezone.utc)
            - datetime.datetime.fromisoformat(ctx["player"].join_datetime)
        ).total_seconds()
    )

    ctx |= query.prev_next(Player, id)
    ctx["crumb"] = "Players"

    ctx["characters"] = (
        Character.select(Character.name, Character.id)
        .join(CharacterPlayer)
        .where(CharacterPlayer.player == id)
    )

    ctx["campaigns"] = (
        Campaign.select(Campaign.name, Campaign.id)
        .join(CampaignCharacter)
        .join(Character)
        .join(CharacterPlayer)
        .where(CharacterPlayer.player == id)
    )

    return flask.render_template("player.html.jinja2", **ctx)


# }{ Campaign


@app.get("/campaigns/")
def campaigns():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="campaign",
        items=(
            Campaign.select(Campaign.name, Campaign.id).order_by(Campaign.id).dicts()
        ),
    )


@app.get("/campaigns/<int:id>")
def campaign(id):
    ctx = {**CTX}

    try:
        ctx["campaign"] = Campaign.select(
            Campaign.name, Campaign.create_datetime
        ).where(Campaign.id == id)[0]
    except IndexError:
        return not_found()

    ctx |= query.prev_next(Campaign, id)
    ctx["crumb"] = "Campaigns"

    ctx["characters"] = (
        Character.select(Player.name, Player.id, Character.name, Character.id)
        .join(CharacterPlayer)
        .join(Player)
        .join_from(Character, CampaignCharacter)
        .where(CampaignCharacter.campaign == id)
    )

    ctx["game_masters"] = (
        Player.select(Player.name, Player.id)
        .join(CampaignGameMaster)
        .where(CampaignGameMaster.campaign == id)
    )

    ctx["founded"] = datetime.datetime.fromisoformat(
        ctx["campaign"].create_datetime
    ).strftime("%d %b %Y")

    return flask.render_template("campaign.html.jinja2", **ctx)


# }{ AttackType


@app.get("/attack_types/")
def attack_types():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="attack_type",
        items=(
            AttackType.select(AttackType.name, AttackType.id)
            .order_by(AttackType.name)
            .dicts()
        ),
    )


@app.get("/attack_types/<int:id>")
def attack_type(id):
    ctx = {**CTX}
    try:
        ctx["attack_type"] = AttackType.select(AttackType.name).where(
            AttackType.id == id
        )[0]
    except IndexError:
        return not_found()

    ctx |= query.prev_next(AttackType, id)
    ctx["crumb"] = "Attack Types"

    return flask.render_template("attack_type.html.jinja2", **ctx)


# }{ Rarity


@app.get("/rarities/")
def rarities():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="rarity",
        items=(Rarity.select(Rarity.name, Rarity.id).order_by(Rarity.degree).dicts()),
    )


@app.get("/rarities/<int:id>")
def rarity(id):
    ctx = {**CTX}
    try:
        ctx["rarity"] = Rarity.select(Rarity.name).where(Rarity.id == id)[0]
    except IndexError:
        return not_found()

    ctx |= query.prev_next(Rarity, id)
    ctx["crumb"] = CTX["titles"]["rarities"]

    return flask.render_template("rarity.html.jinja2", **ctx)


# }{ Feat


@app.get("/feats/")
def feats():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="feat",
        items=(Feat.select(Feat.name, Feat.id).order_by(Feat.name).dicts()),
    )


@app.get("/feats/<int:id>")
def feat(id):
    ctx = {**CTX}
    try:
        ctx["feat"] = Feat.select(Feat.name, Feat.description).where(Feat.id == id)[0]
    except IndexError:
        return not_found()

    ctx["prereqs"] = (
        FeatPrereq.select(FeatPrereq.score, Ability.name, Ability.id)
        .join(Ability)
        .where(FeatPrereq.feat == id)
        .dicts()
    )

    ctx |= query.prev_next(Feat, id)
    ctx["crumb"] = CTX["titles"]["feats"]

    return flask.render_template("feat.html.jinja2", **ctx)


# }{ Feature


@app.get("/features/")
def features():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="feature",
        items=(Feature.select(Feature.name, Feature.id).order_by(Feature.name).dicts()),
    )


@app.get("/features/<int:id>")
def feature(id):
    ctx = {**CTX}
    try:
        ctx["feature"] = Feature.select(Feature.name, Feature.description).where(
            Feature.id == id
        )[0]
    except IndexError:
        return not_found()

    ctx |= query.prev_next(Feature, id)
    ctx["crumb"] = CTX["titles"]["features"]

    return flask.render_template("feature.html.jinja2", **ctx)


# }{ SubFeature


@app.get("/subfeatures/")
def subfeatures():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="subfeature",
        items=(
            SubFeature.select(SubFeature.name, SubFeature.id)
            .order_by(SubFeature.name)
            .dicts()
        ),
    )


@app.get("/subfeatures/<int:id>")
def subfeature(id):
    ctx = {**CTX}
    try:
        ctx["subfeature"] = SubFeature.select(
            SubFeature.name, SubFeature.description
        ).where(SubFeature.id == id)[0]
    except IndexError:
        return not_found()

    ctx |= query.prev_next(SubFeature, id)
    ctx["crumb"] = CTX["titles"]["subfeatures"]

    return flask.render_template("subfeature.html.jinja2", **ctx)


# }{ ToolCategory


@app.get("/tool_categories/")
def tool_categories():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="tool",
        items=(
            ToolCategory.select(ToolCategory.name, ToolCategory.id)
            .order_by(ToolCategory.name)
            .dicts()
        ),
    )


@app.get("/tool_categories/<int:id>")
def tool_category(id):
    ctx = {**CTX}

    try:
        ctx["category"] = ToolCategory.select(ToolCategory.name).where(
            ToolCategory.id == id
        )[0]
    except IndexError:
        return not_found()

    ctx["tools"] = (
        Tool.select(Tool.name, Tool.id).join(ToolCategory).where(ToolCategory.id == id)
    )

    ctx |= query.prev_next(ToolCategory, id)
    ctx["crumb"] = CTX["titles"]["tool_categories"]

    return flask.render_template("tool_category.html.jinja2", **ctx)


# }{ Tool


@app.get("/tools/")
def tools():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="tool",
        items=(Tool.select(Tool.name, Tool.id).order_by(Tool.name).dicts()),
    )


@app.get("/tools/<int:id>")
def tool(id):
    ctx = {**CTX}
    try:
        ctx["tool"] = Tool.select().where(Tool.id == id)[0]
    except IndexError:
        return not_found()

    ctx |= query.prev_next(Tool, id)
    ctx["crumb"] = CTX["titles"]["tools"]

    return flask.render_template("tool.html.jinja2", **ctx)


# }{ MonsterType


@app.get("/monster_types/")
def monster_types():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="monster_type",
        items=(
            MonsterType.select(MonsterType.name, MonsterType.id)
            .order_by(MonsterType.name)
            .dicts()
        ),
    )


@app.get("/monster_types/<int:id>")
def monster_type(id):
    ctx = {**CTX}
    try:
        ctx["monster_type"] = MonsterType.select().where(MonsterType.id == id)[0]
    except IndexError:
        return not_found()

    ctx |= query.prev_next(MonsterType, id)
    ctx["crumb"] = CTX["titles"]["monster_types"]

    return flask.render_template("monster_type.html.jinja2", **ctx)


# }{ Monster


@app.get("/monsters/")
def monsters():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="monster",
        items=(Monster.select(Monster.name, Monster.id).order_by(Monster.name).dicts()),
    )


@app.get("/monsters/<int:id>")
def monster(id):
    ctx = {**CTX}
    try:
        ctx["monster"] = Monster.select().where(Monster.id == id)[0]
    except IndexError:
        return not_found()

    ctx["abilities"] = MonsterAbility.select().join(Ability).where(MonsterAbility.monster == id)

    ctx |= query.prev_next(Monster, id)
    ctx["crumb"] = CTX["titles"]["monsters"]

    return flask.render_template("monster.html.jinja2", **ctx)


# }{ Size


@app.get("/sizes/")
def sizes():
    return flask.render_template(
        "items.html.jinja2",
        **CTX,
        item="size",
        items=(Size.select(Size.name, Size.id).order_by(Size.id).dicts()),
    )


@app.get("/sizes/<int:id>")
def size(id):
    ctx = {**CTX}
    try:
        ctx["size"] = Size.select().where(Size.id == id)[0]
    except IndexError:
        return not_found()

    ctx |= query.prev_next(Size, id)
    ctx["crumb"] = CTX["titles"]["sizes"]

    return flask.render_template("size.html.jinja2", **ctx)


# } EOF
