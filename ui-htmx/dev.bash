#!/usr/bin/env bash

VENV=.venv
HOST=0.0.0.0
PORT=5000

./setup.bash

source "${VENV}/bin/activate"

files="$(echo templates/*) $(echo static/*)"

flask --app app --debug run --host ${HOST} --port ${PORT} --extra-files "${files// /:}"
