#!/usr/bin/env bash

VENV=.venv
HOST=localhost
PORT=8000

./setup.bash

source "${VENV}/bin/activate"

gunicorn \
	--bind ${HOST}:${PORT} \
	--workers 2 \
	"wsgi:app"
