#!/usr/bin/env python3


from peewee import (
    Case,
    fn,
    JOIN,
)

import logic
from models import *


def characters():
    return Character.select(Character.id, Character.name).order_by(Character.id)


def races():
    return Race.select(Race.id, Race.name).order_by(Race.name)


def cclasses():
    return CClass.select(CClass.id, CClass.name).order_by(CClass.name)


def character_level(chid):
    xp = Character.select(Character.xp).where(Character.id == chid).scalar()
    for step in Level.select(Level.level, Level.threshold).order_by(Level.level):
        if xp < step.threshold:
            return step.level - 1
    return 20


# def sum_cclass_levels(chid):
#     return (
#         CharacterCClass.select(fn.SUM(CharacterCClass.level))
#         .where(CharacterCClass.character == chid)
#         .scalar()
#         or 0
#     )


def character_classes(chid):
    return (
        CharacterCClass.select(
            CharacterCClass.id,
            CharacterCClass.level,
            CClass.id,
            CClass.name,
            StartingCClass.id,
        )
        .join(CClass)
        .join(StartingCClass, src=CharacterCClass, join_type=JOIN.LEFT_OUTER)
        .where(CharacterCClass.character == chid)
        .order_by(CharacterCClass.id)
    )


def available_classes(chid):
    # TODO can i do this with a join?
    chcs = CharacterCClass.select(CharacterCClass.cclass).where(
        CharacterCClass.character == chid
    )
    return (
        CClass.select(CClass.name, CClass.id)
        .where(CClass.id.not_in(chcs))
        .order_by(CClass.name)
    ).dicts()


def inventory_weight(chid):
    return (
        Character.select(
            Case(None, ((Armour.id, fn.SUM(Armour.weight)),), 0)
            + Case(None, ((Weapon.id, fn.SUM(Weapon.weight)),), 0)
        )
        .left_outer_join(CharacterArmour)
        .left_outer_join(Armour)
        .switch(Character)
        .left_outer_join(CharacterWeapon)
        .left_outer_join(Weapon)
        .where(Character.id == chid)
        .scalar()
    )


# note IntegerField / int does integer division (unlike Python syntax where / is always float)
ABILITY_MOD = CharacterAbility.score / 2 - 5
PROF_SAVE = Case(None, ((SaveProficiency.id, True),), False)


def character_abilities(chid):
    return (
        Ability.select(
            Ability.id,
            Ability.name,
            CharacterAbility.id,
            CharacterAbility.score,
            ABILITY_MOD.alias("mod"),
        )
        .join(CharacterAbility)
        .where(CharacterAbility.character == chid)
        .order_by(Ability.id)
    )


def saving_throws(chid):
    level = character_level(chid)
    bonus = logic.proficiency_bonus(level)
    return (
        CharacterAbility.select(
            Ability.name,
            Ability.id,
            PROF_SAVE.alias("proficient"),
            (ABILITY_MOD + Case(None, ((SaveProficiency.id, bonus),), 0)).alias(
                "save_mod"
            ),
        )
        .left_outer_join(SaveProficiency)
        .join_from(CharacterAbility, Ability)
        .where(CharacterAbility.character == chid)
        .order_by(Ability.id)
        .dicts()
    )


def abilities_and_saves(chid):
    # TODO seems suboptimal
    level = character_level(chid)
    bonus = logic.proficiency_bonus(level)
    return (
        Ability.select(
            Ability.name,
            Ability.id,
            Ability.short,
            CharacterAbility.id,
            CharacterAbility.score,
            ABILITY_MOD.alias("mod"),
            PROF_SAVE.alias("proficient"),
            (ABILITY_MOD + Case(None, ((SaveProficiency.id, bonus),), 0)).alias(
                "save_mod"
            ),
        )
        .join(CharacterAbility)
        .left_outer_join(SaveProficiency)
        .where(CharacterAbility.character == chid)
        .order_by(Ability.id)
    )


def set_single_on_character(field, trigger_header, form):
    Character.update({field: form[field]}).where(
        Character.id == form["character"]
    ).execute()
    return "", {"HX-Trigger": trigger_header}


def skills(chid, prof_bonus):
    return (
        Skill.select(
            Skill.name,
            Skill.id,
            Ability.short,
            Ability.id,
            SkillProficiency.id,
            SkillExpertise.id,
            (
                ABILITY_MOD
                + Case(None, ((SkillProficiency.id, prof_bonus),), 0)
                + Case(None, ((SkillExpertise.id, prof_bonus),), 0)
            ).alias("bonus"),
        )
        .left_outer_join(SkillProficiency)
        .left_outer_join(SkillExpertise)
        .join_from(Skill, Ability)
        .join(CharacterAbility)
        .where(CharacterAbility.character == chid)
        .order_by(Skill.name)
    )


def passives(chid, prof_bonus):
    prof_skill = Case(SkillProficiency.character, ((chid, True),), False)
    return (
        Skill.select(
            Skill.name,
            Skill.id,
            Ability.short,
            Ability.id,
            prof_skill.alias("proficient"),
            (10 + ABILITY_MOD + Case(None, ((prof_skill, prof_bonus),), 0)).alias(
                "score"
            ),
        )
        .left_outer_join(SkillProficiency)
        .join_from(Skill, Ability)
        .join(CharacterAbility)
        .where(CharacterAbility.character == chid)
        .where(Skill.name << ("Perception", "Investigation", "Insight"))
    )


def prev_item(table, tid, name):
    prev_item = (
        table.select(table.id, getattr(table, name))
        .order_by(table.id.desc())
        .where(table.id < tid)
        .limit(1)
        .dicts()
    )
    return prev_item[0] if prev_item.exists() else None


def next_item(table, tid, name):
    next_item = (
        table.select(table.id, getattr(table, name))
        .order_by(table.id)
        .where(table.id > tid)
        .limit(1)
        .dicts()
    )
    return next_item[0] if next_item.exists() else None


def prev_next(table, tid, name="name"):
    return {
        "prev_item": prev_item(table, tid, name),
        "next_item": next_item(table, tid, name),
    }


def my_conditions(chid):
    return (
        CharacterCondition.select(Condition.name, Condition.id)
        .join(Condition)
        .where(CharacterCondition.character == chid)
        .dicts()
    )


def alignment_from_matrix(morality, society, **_):
    return (
        Alignment.select(Alignment.id)
        .where(Alignment.morality == morality)
        .where(Alignment.society == society)
        .scalar()
    )


def levelled_spells(spell_query):
    return [
        {
            "level": level.level,
            "name": level.name,
            "spells": spells,
        }
        for level in SpellLevel.select()
        if (spells := spell_query.where(Spell.level == level.id)).exists()
    ]


def character_players(chid):
    return (
        Player.select(Player.id, Player.name)
        .join(CharacterPlayer)
        .join(Character)
        .where(Character.id == chid)
        .dicts()
    )


def levels():
    return {
        l: t
        for l, t in Level.select(Level.level, Level.threshold)
        .order_by(Level.level)
        .tuples()
    }
