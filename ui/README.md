# UI

[Live](https://eidoom.gitlab.io/draghi/)

Built on Svelte. Used [template](https://github.com/sveltejs/template).

## Development

```bash
npm install
npm run dev
```

Navigate to <http://localhost:8080>.

## Production

```bash
npm run build
npm run start
```
or to access from other machines,
```bash
npm run open
```

## TODO

* The new functional code could do with some further testing
* Should do some mobile-first styling
* Make all character functions return character? Then can equate the character variable to tell Svelte it's changed if necessary.
* Probably shouldn't reset all spell slots on level up

## Artwork

All artwork is by Mia West and licenced under [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).
