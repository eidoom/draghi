// Race
//     languages = [];
//     abilities = {};
//     other_ability_increases = 0;
//     ability_increases = {};
//     walking_speed;
//     size;
//     subrace;
//     subraces = [];
//     proficiencies = {
//         weapons: [],
//         armour: [],
//         skills: [],
//         tools: {},
//     };
//     _traits = [];
//     extra_languages = 0;
//     resistances = [];
//     _hit_points = 0;
//     name;
//     #base_other_ability_increases = 0;

// SubRace
//     abilities = {};
//     proficiencies = {
//         weapons: [],
//         armour: [],
//         skills: [],
//         tools: {},
//     };
//     hit_points = 0;
//     walking_speed = 0;
//     traits = [];
//     name;

export default {
    init,
    hit_points,
    has_other_ability_increases,
    has_subraces,
    race_abilities,
    abilities,
    walking_speed,
    traits,
    subrace_abilities,
};

function init(raceIn) {
    const raceOut = structuredClone(raceIn);

    if (raceIn.other_ability_increases) {
        raceOut.base_other_ability_increases = raceIn.other_ability_increases;
        raceOut.ability_increases = {};
    }

    if (!raceIn.proficiencies) {
        raceOut.proficiencies = {};
    }

    return raceOut;
}

// is it better to init these instead?
function hit_points(race) {
    return (
        (race.hit_points || 0) +
        (race.subrace ? race.subrace.hit_points || 0 : 0)
    );
}

function has_other_ability_increases(race) {
    return race.base_other_ability_increases;
}

function has_subraces(race) {
    return race.subraces;
}

function race_abilities(race, ability) {
    let a = race.abilities[ability] || 0;
    if (race.ability_increases) {
        a += race.ability_increases[ability] || 0;
    }
    return a;
}

function abilities(race, ability) {
    let a = race_abilities(race, ability);

    if (race.subrace) {
        a += subrace_abilities(race.subrace, ability);
    }

    return a;
}

function walking_speed(race) {
    let ws = race.walking_speed;
    if (race.subrace) {
        ws += race.subrace.walking_speed;
    }
    return ws;
}

function traits(race) {
    let t = race.traits;
    if (race.subrace) {
        t += race.subrace.traits;
    }
    return t;
}

function subrace_abilities(subrace, ability) {
    return subrace.abilities[ability] || 0;
}
