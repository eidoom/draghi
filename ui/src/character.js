import race from "./race.js";
import xp from "./xp.js";
import { abilities } from "./global.js";

import skills from "./skills.json";

const character = {
  abilities: {
    bases: {},
    current_improvements: {},
    improvements: {},
    available_improvements: 0,
  },
  skills: {
    proficiencies: Object.values(skills)
      .flat()
      .reduce((a, v) => ({ [v]: false, ...a }), {}),
    background_available: 2,
  },
  xp: {
    value: 0,
    level_ups: 0,
  },
  done: {
    name: false,
    cclass: false,
    race: false,
    subrace: false,
    base_scores: false,
    race_scores: false,
    alignment: false,
    skills: false,
  },
  choose: {
    race: "",
    subrace: "",
    cclass: "",
    armour: "",
  },
  weapons: [],
  hit_points: 0,
  base_max_hit_points: 0,
  armour: {},
  alignment: {
    good_evil: "",
    law_chaos: "",
  },
};

function used_improvements(character) {
  return (
    Object.values(character.abilities.current_improvements).reduce(
      (acc, val) => acc + val,
      0
    ) == character.abilities.available_improvements
  );
}

function done_abilities(character) {
  return (
    character.done.race &&
    character.done.base_scores &&
    (has_subraces(character) ? character.done.subrace : true) &&
    (has_other_ability_increases(character) ? character.done.race_scores : true)
  );
}

function done_init(character) {
  return character.done.cclass && done_abilities(character);
}

function reset_improvements(character) {
  for (let ability of abilities) {
    character.abilities.current_improvements[ability] = 0;
  }
  character.abilities.available_improvements = 0;
}

function init_improvements(character) {
  for (let ability of abilities) {
    character.abilities.improvements[ability] = 0;
    character.abilities.current_improvements[ability] = 0;
  }
}

function init_spell_slots(character) {
  if (character.cclass.spell_slots) {
    character.spell_slots = structuredClone(
      character.cclass.spell_slots[xp.level(character.xp.value)]
    );
  }
}

function init_character(character) {
  init_hit_points(character);
  init_improvements(character);
}

function init_hit_points(character) {
  character.hit_points = max_hit_points(character);
}

function max_hit_points(character) {
  return (
    character.base_max_hit_points +
    xp.level(character.xp.value) * (modifier(character, "constitution") || 0) +
    (character.race ? race.hit_points(character.race) : 0)
  );
}

function has_subrace(character) {
  return character.race && character.race.subrace;
}

function has_other_ability_increases(character) {
  return character.race && race.has_other_ability_increases(character.race);
}

function ability_score(character, ability) {
  const score =
    character.abilities.bases[ability] +
    (character.race ? race.abilities(character.race, ability) : 0) +
    character.abilities.improvements[ability];
  return Math.min(20, score);
}

function modifier(character, ability) {
  const score = ability_score(character, ability);

  if (score !== null) {
    return Math.min(10, Math.floor(score / 2) - 5);
  }
}

// function heal(character, n) {
//   character.hit_points =
//     n > 0
//       ? Math.min(character.max_hit_points(), character.hit_points + n)
//       : Math.max(0, character.hit_points + n);
// }

function has_subraces(character) {
  return character.race && race.has_subraces(character.race);
}

function armour_class(character) {
  return (
    (character.armour
      ? character.armour.ac +
        (character.armour.name == "none"
          ? modifier(character, "dexterity")
          : 0) +
        (character.armour.weight == "light"
          ? modifier(character, "dexterity")
          : 0) +
        (character.armour.weight == "medium"
          ? Math.min(2, modifier(character, "dexterity"))
          : 0)
      : 0) + (character.shield ? 2 : 0)
  );
}

export default {
  character,
  done_abilities,
  done_init,
  init_character,
  reset_improvements,
  max_hit_points,
  has_subrace,
  has_other_ability_increases,
  ability_score,
  modifier,
  has_subraces,
  used_improvements,
  init_spell_slots,
  armour_class,
};
