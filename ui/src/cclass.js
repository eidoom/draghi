export default {
    init,
};

function init(cclassIn) {
    const cclassOut = structuredClone(cclassIn);

    cclassOut.ability_improvements = {};

    return cclassOut;
}
