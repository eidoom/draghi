const level_xps = [
  0, 300, 900, 2700, 6500, 14000, 23000, 34000, 48000, 64000, 85000, 100000,
  120000, 140000, 165000, 195000, 225000, 265000, 305000, 355000,
];

function level(xp) {
  for (let i = 0; i < level_xps.length; i++) {
    if (xp < level_xps[i]) {
      return i;
    }
  }
  return 20;
}

function proficiency_bonus(xp) {
  for (let i = 1; i < 5; i++) {
    if (xp < level_xps[4 * i]) {
      return i + 1;
    }
  }
  return 6;
}

export default { level, proficiency_bonus, level_xps };
