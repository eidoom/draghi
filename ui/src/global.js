export { capitalise, titleCase, abilities, isNumber };

const _capitalise = (str) => str.replace(/^\w/, (c) => c.toUpperCase());

const capitalise = (str) => _capitalise(str.toLowerCase());

const titleCase = (str) =>
  _capitalise(
    str
      .toLowerCase()
      .split(" ")
      .map((word) => (["of", "the"].includes(word) ? word : _capitalise(word)))
      .join(" ")
  );

const abilities = [
  "strength",
  "dexterity",
  "constitution",
  "intelligence",
  "wisdom",
  "charisma",
];

const isNumber = (number) => typeof number === "number" && !isNaN(number);
