#!/usr/bin/env python3

import flask


app = flask.Flask(__name__)


@app.route("/")
def index():
    return {
        "title": "draghi",
    }


@app.route("/version")
def version():
    return {
        "major": 0,
        "minor": 0,
        "patch": 0,
    }


@app.route("/abilities")
def abilities():
    return {
        "strength": 15,
        "dexterity": 15,
        "constitution": 14,
        "intelligence": 12,
        "wisdom": 7,
        "charisma": 7,
    }
