#!/usr/bin/env python3

# import math

import matplotlib.pyplot
import numpy
import scipy.optimize

import main


def sigfig(n, m):
    # m = # sig figs
    digits = len(str(int(n)))
    return round(n, -digits + m)


def f(x, a, b, c):
    # return a + b * x + c * x**2
    # return a + b * c**x
    return a + b * x**c


# def f(x, a, b):
#     return a + b * math.exp(x)


if __name__ == "__main__":
    ys = main.LEVEL_XPS
    xs = [*range(1, len(ys) + 1)]

    print(len(ys))
    print(ys)
    print([y2 - y1 for y1, y2 in zip(ys, ys[1:])])

    # fit = numpy.polynomial.polynomial.Polynomial.fit(xs, ys, 2)
    # sim0 = [fit(x) for x in xs]

    coeffs, _ = scipy.optimize.curve_fit(f, xs, ys)
    print(coeffs)
    sim1 = [f(x, *coeffs) for x in xs]
    print(sim1)

    sim1sf = [sigfig(n, 3) for n in sim1]
    print(sim1sf)
    # print(ys)

    # residuals = [s - d for s, d in zip(sim1, ys)]

    matplotlib.pyplot.plot(xs, ys, marker=".", linestyle="")
    # matplotlib.pyplot.plot(xs, sim0, marker="", linestyle="-", label="numpy")
    matplotlib.pyplot.plot(xs, sim1, marker="", linestyle="-", label="scipy")
    # matplotlib.pyplot.legend()

    # matplotlib.pyplot.plot(xs, residuals, marker=".", linestyle="", label="residuals")

    matplotlib.pyplot.show()
