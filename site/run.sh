#!/usr/bin/env bash

HOST=127.0.0.1

usage() {
	echo "Usage: $0 [-o] [-d] [-h]

	-o    open port
	-d    development mode
	-h    show help
	"
	exit 0
}

while getopts "odh" OPT; do
	case "${OPT}" in
		o)
			HOST=0.0.0.0
			;;
		d)
			export FLASK_ENV=development
			;;
		h | *)
			usage
			;;
	esac
done

# export FLASK_APP=app

flask run --host=${HOST}
