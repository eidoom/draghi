#!/usr/bin/env python3

import flask


app = flask.Flask(__name__)


@app.route("/")
def index():
    return flask.render_template("index.html.jinja2", header="draghi")
