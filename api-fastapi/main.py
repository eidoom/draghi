#!/usr/bin/env python3

# from enum import Enum, auto, unique
import json, sqlite3, itertools

from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

DEBUG = True


# @unique
# class StrEnum(str, Enum):
#     def _generate_next_value_(name, _, __, ___):
#         return name


class Character(BaseModel):
    name: str
    race: str
    cclass: str


# class ArmourCategory(StrEnum):
#     heavy = auto()
#     medium = auto()
#     light = auto()


class Armour(BaseModel):
    name: str
    # category: ArmourCategory # have to cast to str on dict()
    category: str
    ac: int
    cost: int
    weight: int
    stealth_disadvantage: bool = False
    description: str


class Weapon(BaseModel):
    name: str
    damage: str
    type: str
    level: str
    range: str
    # properties: list[str] # each property is a bool?
    cost: int
    weight: int


TABLES = {
    "character": tuple(Character.__fields__),
    "armour": tuple(Armour.__fields__),
    "weapon": tuple(Weapon.__fields__),
}


def open_db():
    con = sqlite3.connect("tmp.db")
    cur = con.cursor()
    return con, cur


app = FastAPI()


@app.on_event("startup")
def database_connect():
    con, cur = open_db()

    for table, columns in TABLES.items():
        res = cur.execute("SELECT name FROM sqlite_master WHERE name=?", (table,))
        if res.fetchone() is None:
            cmd = f"CREATE TABLE {table}(" + ",".join(columns) + ")"
            if DEBUG:
                print(cmd)
            cur.execute(cmd)

    with open("../static/armour.json", "r") as f:
        armours = json.load(f)

    for armour_raw in armours:
        armour = Armour.parse_obj(armour_raw)
        res = cur.execute("SELECT name FROM armour WHERE name = ?", (armour.name,))
        if res.fetchone() is None:
            vals = tuple(armour.dict().values())
            # vals = [str(v) if isinstance(v, StrEnum) else v for v in armour.dict().values()]
            cmd = f"INSERT INTO armour VALUES ({','.join('?' for _ in vals)})"
            if DEBUG:
                print(cmd, vals)
            cur.execute(cmd, vals)
            con.commit()

    with open("../static/weapon.json", "r") as f:
        data = json.load(f)

    for weapon_raw in data["weapons"]:
        weapon = Weapon.parse_obj(weapon_raw)
        res = cur.execute("SELECT name FROM weapon WHERE name = ?", (weapon.name,))
        if res.fetchone() is None:
            vals = tuple(weapon.dict().values())
            cmd = f"INSERT INTO weapon VALUES ({','.join('?' for _ in vals)})"
            if DEBUG:
                print(cmd, vals)
            cur.execute(cmd, vals)
            con.commit()

    con.close()


@app.get("/list/{table}")
def list_table(table):
    con, cur = open_db()

    cmd = f"SELECT name FROM {table}"

    if DEBUG:
        print(cmd)

    res = cur.execute(cmd)

    ans = res.fetchall()

    con.close()

    return [*itertools.chain(*ans)]


def select_name(table, name):
    con, cur = open_db()

    cmd = f"SELECT * FROM {table} WHERE name = ?"
    arg = (name,)

    if DEBUG:
        print(cmd, arg)

    res = cur.execute(cmd, arg)

    ans = res.fetchone()

    con.close()

    if ans is None:
        raise HTTPException(status_code=404, detail=f"{table} not found")

    return dict(zip(TABLES[table], ans))


@app.get("/character/{name}")
def read_character(name: str, q: str | None = None):
    data = select_name("character", name)

    return data | {"q": q}


@app.put("/character/{name}")
def update_character(name: str, character: Character):
    if name != character.name:
        raise HTTPException(
            status_code=400, detail="Character name should match endpoint"
        )

    table = "character"

    con, cur = open_db()

    cmd = f"INSERT INTO {table} VALUES (:name, :race, :cclass)"
    arg = character.dict()

    if DEBUG:
        print(cmd, arg)

    cur.execute(cmd, arg)
    con.commit()

    con.close()


@app.get("/weapon/{name}")
def read_weapon(name: str):
    return select_name("weapon", name)


@app.get("/armour/{name}")
def read_armour(name: str):
    return select_name("armour", name)
