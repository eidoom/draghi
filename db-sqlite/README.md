# db-sqlite

`main.py` initialises an SQLite3 database from plain text files (JSON) in `../static/`.

```shell
sqlite3 tmp.db < test.sql
```
