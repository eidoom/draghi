#!/usr/bin/env python3

import enum, json, sqlite3, dataclasses


@enum.unique
class StrEnum(str, enum.Enum):
    def _generate_next_value_(name, _0, _1, _2):
        # allows ArmourCategory("heavy") -> ArmourCategory.heavy
        return name

    def __str__(self):
        # allows str(ArmourCategory.heavy) -> "heavy"
        return self.name


ArmourCategory = StrEnum("ArmourCategory", ("light", "medium", "heavy"))


@dataclasses.dataclass
class Armour:
    name: str
    category: ArmourCategory
    ac: int
    cost: int
    weight: int
    description: str
    stealth_disadvantage: bool = False


@dataclasses.dataclass
class Weapon:
    name: str
    damage: str
    type: str
    level: str
    range: str
    # properties: list[str] # each property is a bool?
    cost: int
    weight: int


@dataclasses.dataclass
class WeaponProperties:
    name: str
    description: str


TABLES = {
    table_name: [f.name for f in dataclasses.fields(class_name)]
    for table_name, class_name in (
        ("armour", Armour),
        ("weapon", Weapon),
        ("weapon_properties", WeaponProperties),
    )
}


def open_db():
    con = sqlite3.connect("tmp.db")
    cur = con.cursor()
    return con, cur


def fill_table(cur, name, table):
    for row in table:
        res = cur.execute(f"SELECT name FROM {name} WHERE name = ?", (row.name,))
        if res.fetchone() is None:
            vals = tuple(dataclasses.asdict(row).values())
            cur.execute(
                f"INSERT INTO {name} VALUES ({','.join('?' for _ in vals)})", vals
            )


if __name__ == "__main__":
    con, cur = open_db()

    for table, columns in TABLES.items():
        res = cur.execute("SELECT name FROM sqlite_master WHERE name=?", (table,))
        if res.fetchone() is None:
            cur.execute(f"CREATE TABLE {table}(" + ",".join(columns) + ")")

    with open("../static/armour.json", "r") as f:
        armours_raw = json.load(f)

    armours = [Armour(**armour_raw) for armour_raw in armours_raw]
    fill_table(cur, "armour", armours)

    with open("../static/weapon.json", "r") as f:
        data = json.load(f)

    weapons_properties = [WeaponProperties(n, d) for n, d in data["properties"].items()]
    fill_table(cur, "weapon_properties", weapons_properties)

    weapons = []
    for weapon_raw in data["weapons"]:
        weapon = Weapon(**{k: v for k, v in weapon_raw.items() if k != "properties"})
        weapons.append(weapon)

    fill_table(cur, "weapon", weapons)

    con.commit()

    con.close()
