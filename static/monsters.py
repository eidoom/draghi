#!/usr/bin/env python3

import re, json

from process import proc


if __name__ == "__main__":
    text = proc("../../5e-database/src/5e-SRD-Monsters.json")

    data = json.loads(text)

    with open("monster.json", "w") as f:
        json.dump(data, f, indent=4)
