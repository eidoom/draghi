#!/usr/bin/env python3

import re, json

from process import proc


def make_new(ff):
    new = {
        k: v
        for k, v in ff.items()
        if k
        not in (
            "level",
            "class",
            "subclass",
            "parent",
            "feature_specific",
        )  # TODO options
    }
    new["levels"] = [ff["level"]]

    if "parent" in ff:
        new["parents"] = [
            f"Fighting Style: {ff['class']}"
            if ff["parent"] == "Fighting Style"
            else ff["parent"]
        ]

    if "subclass" in ff:
        new["subclasses"] = [ff["subclass"]]
    else:
        new["classes"] = [ff["class"]]

    return new


if __name__ == "__main__":
    text = proc("../../5e-database/src/5e-SRD-Features.json")

    text = text.replace('    "prerequisites": [],\n', "")
    text = re.sub(
        r'(\s*"(subclass|class|parent)": )\{\n\s*"name": (".*")\n\s*\}(,?\n)',
        r"\1\3\4",
        text,
        flags=re.MULTILINE,
    )

    data = json.loads(text)

    uniq = {}
    dups = []
    for ff in data:
        nn = ff["name"]
        if nn in uniq.keys():
            if uniq[nn]["description"] == ff["description"]:
                if "classes" in uniq[nn] and ff["class"] not in uniq[nn]["classes"]:
                    uniq[nn]["classes"].append(ff["class"])

                if ff["level"] not in uniq[nn]["levels"]:
                    uniq[nn]["levels"].append(ff["level"])

                if (
                    "parents" in uniq[nn]
                    and ff["parent"] == "Fighting Style"
                    and ff["parent"] not in uniq[nn]["parents"]
                ):
                    uniq[nn]["parents"].append(
                        ff["parent"]
                        if ff["class"] in ff["parent"]
                        else f"{ff['parent']}: {ff['class']}"
                    )
            else:
                dups.append(make_new(ff))
        else:
            uniq[nn] = make_new(ff)

    for nn in set(d["name"] for d in dups):
        dups.append(uniq[nn])
        del uniq[nn]

    ddps = []
    for dup in dups:
        counter = "classes" if "classes" in dup else "subclasses"
        count = len(dup[counter])
        if count == 1:
            cclass = dup[counter][0]
            dup["name"] += f": {cclass}"
        if dup["name"] not in uniq.keys():
            uniq[dup["name"]] = dup
        else:
            ddps.append(dup)

    for nn in set(d["name"] for d in ddps):
        ddps.append(uniq[nn])
        del uniq[nn]

    new = {}
    for d in ddps:
        if "Ability Score Improvement" in d["name"]:
            nn = "Ability Score Improvement (7 tiers)"
        else:
            nn = d["name"]

        if nn in new:
            if d["classes"][0] not in new[nn]["classes"]:
                new[nn]["classes"].extend(d["classes"])
            if d["levels"][0] not in new[nn]["levels"]:
                new[nn]["levels"].extend(d["levels"])
        else:
            d["name"] = nn
            new[nn] = d

    nn = "Ability Score Improvement (5 tiers)"
    uniq[nn] = {
        k: nn if k == "name" else v
        for k, v in uniq["Ability Score Improvement"].items()
    }
    del uniq["Ability Score Improvement"]

    uniq |= new

    outp = sorted(uniq.values(), key=lambda d: d["name"])

    with open("feature.json", "w") as f:
        json.dump(outp, f, indent=4)
