#!/usr/bin/env python3

import argparse, pathlib, re


def proc(file):
    text = pathlib.Path(file).read_text()

    text = re.sub(
        r"^\s*\"(index|url|reference)\": \"[^ ]+\",?\n", "", text, flags=re.MULTILINE
    )

    # ul
    text = re.sub(
        r'(^\s*")([^-].*:)",\n\s*"- (.*)"',
        r'\1<p>\2</p><ul><li>\3</li>"',
        text,
        flags=re.MULTILINE,
    )
    text = re.sub(
        r'(^\s*")- (.*)((",\n\s*"[^-]|"\n\s*\],?\n))',
        r"\1<li>\2</li></ul>\3",
        text,
        flags=re.MULTILINE,
    )
    old = ""
    while old != text:
        old = text
        text = re.sub(
            r'(li>)",\n\s*"- (.*)"', r'\1<li>\2</li>"', text, flags=re.MULTILINE
        )
    text = re.sub(r'(li>)",\n\s*"(<li)', r"\1\2", text, flags=re.MULTILINE)

    # desc start
    text = re.sub(
        r'("desc": )\[\n\s*("<.*)",\n\s*"(.*)"',
        r'\1\2<p>\3</p>"',
        text,
        flags=re.MULTILINE,
    )
    text = re.sub(
        r'("desc": )\[\n\s*"([^<].*)"', r'\1"<p>\2</p>"', text, flags=re.MULTILINE
    )
    text = re.sub(r'("desc": )\[\n\s*"(<.*)"', r'\1"\2"', text, flags=re.MULTILINE)

    text = re.sub(
        r'>",\n\s*"<',
        r"><",
        text,
        flags=re.MULTILINE,
    )

    # p
    old = ""
    while old != text:
        old = text
        text = re.sub(
            r'("desc": "<.*)",\n\s*"\s*([^<].*)"',
            r'\1<p>\2</p>"',
            text,
            flags=re.MULTILINE,
        )

    text = re.sub(
        r'>",\n\s*"<',
        r"><",
        text,
        flags=re.MULTILINE,
    )

    # desc end
    text = re.sub(
        r'"desc(": "<.*")\n\s*\]', r'"description\1', text, flags=re.MULTILINE
    )
    text = re.sub(
        r'"desc": \["([^<].*)"\](,?)',
        r'"description": "<p>\1</p>"\2',
        text,
        flags=re.MULTILINE,
    )

    # punctuation
    text = text.replace("'", "’")  # apostrophe
    text = re.sub(r"\b---\b", "—", text)  # emdash
    text = re.sub(r"\b--\b", "–", text)  # endash
    text = re.sub(r"\\\"(.*?)\\\"", r"“\1”", text)  # quotation marks

    old = ""
    while old != text:
        old = text
        text = re.sub(r'([\d"}\]]),(\n\s*\})', r"\1\2", text)

    replacements = [
        ("armor", "armour"),
        ("honor", "honour"),
        ("valor", "valour"),
        ("vigor", "vigour"),
        ("favor", "favour"),
        ("specializ", "specialis"),
        ("neutraliz", "neutralis"),
        ("agoniz", "agonis"),
        ("vitaliz", "vitalis"),
    ]
    replacements += [(a.title(), b.title()) for a, b in replacements]
    for a, b in replacements:
        text = text.replace(a, b)

    # "# ft." -> #
    text = re.sub(r'"(\d+) ft\."', r'\1', text)

    # text = re.sub(r'\{\n\s*"name": (".+?")\n\s*\}', r'\1', text, re.MULTILINE,re.S)

    return text


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file", type=pathlib.Path)
    args = parser.parse_args()

    print(proc(args.file))
