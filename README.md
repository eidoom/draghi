# [draghi](https://gitlab.com/eidoom/draghi)
[Companion post](https://eidoom.gitlab.io/computing-blog/post/draghi/)
## Reference material
* [Systems Reference Document](http://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf) ([OGL](https://en.wikipedia.org/wiki/Open_Game_License))
* Most data obtained from [`5e-bits/5e-database`](https://github.com/5e-bits/5e-database/tree/main/src)
    * Upstream last merged 2022-11-29 (last upstream commit: 0580dc0)
## Contents
directory|description|progress
---|---|---
[.](.)|some experimentation scripts|
[api-flask](api-flask)|api for modular-ui using Flask|*
[api-fastapi](api-fastapi)|another api using FastAPI|*
[db-sqlite](db-sqlite)|play with sqlite3 db|*
[db-peewee](db-peewee)|play with peewee osm|*
[modular-ui](modular-ui)|ui for api-flask using Svelte|*
[site](site)|self-contained Flask app|*
[static](static)|data files|***
[ui](site)|self-contained Svelte app ([live](https://eidoom.gitlab.io/draghi/))|***
[ui-htmx](ui-htmx)|app using Flask, Peewee, SQLite, and HTMX|***
[phoenix](phoenix)|app using Elixir Phoenix framework|**
